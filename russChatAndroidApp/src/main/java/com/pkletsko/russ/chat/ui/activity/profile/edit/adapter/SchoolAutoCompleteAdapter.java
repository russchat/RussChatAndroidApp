package com.pkletsko.russ.chat.ui.activity.profile.edit.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfoSearchResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pkletsko on 06.05.2015.
 */
public class SchoolAutoCompleteAdapter extends ArrayAdapter<SchoolInfo> implements Filterable {
    private ArrayList<SchoolInfo> mData;
    private Utils utils;
    private GlobalClass globalVariable;
    private Context context;

    public SchoolAutoCompleteAdapter(Context context, int textViewResourceId, GlobalClass gv, Utils ut) {
        super(context, textViewResourceId);
        mData = new ArrayList<SchoolInfo>();
        globalVariable = gv;
        utils = ut;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public SchoolInfo getItem(int index) {
        return mData.get(index);
    }

    private void loadFromServer(CharSequence constraint) {
        if (constraint != null && globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_SCHOOL);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("search", constraint.toString());

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        SchoolInfoSearchResult schoolInfoSearchResult = utils.getSchoolInfoSearchResult(result);
                        if (schoolInfoSearchResult == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            mData.clear();
                            mData.addAll(schoolInfoSearchResult.getSchools());
                            if(mData.size() > 0) {
                                notifyDataSetChanged();
                            } else {
                                notifyDataSetInvalidated();
                            }
                        }
                    }
                }
            }).request(requestParameters, globalVariable);
        }
    }

    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint != null) {
                    loadFromServer(constraint);
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint, FilterResults results) {
            }
        };
        return myFilter;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        TextView originalView = (TextView) super.getView(position, convertView, parent); // Get the original view

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final TextView view = (TextView) inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);

        // Start tweaking
        view.setText(originalView.getText());
        //view.setTextColor(R.color.black);  // also useful if you have a color scheme that makes the text show up white
        view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10); // override the text size
        return view;
    }
}

package com.pkletsko.russ.chat.ui.activity.profile.edit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.common.activity.MainActivity;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivity;
import com.pkletsko.russ.chat.ui.activity.profile.edit.adapter.SchoolAutoCompleteAdapter;
import com.pkletsko.russ.chat.ui.activity.profile.edit.adapter.SchoolClassAutoCompleteAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileEdit;

import java.util.HashMap;
import java.util.Map;

public class UserProfileEditActivity extends BaseActivity {

    // LogCat tag
    private static final String TAG = UserProfileEditActivity.class.getSimpleName();

    private EditText inputFirstName;
    private EditText inputLastName;
    private EditText inputNickName;
    private EditText inputDateOfBirth;
    private EditText inputCountry;
    private EditText inputCity;

    private LinearLayout schoolClassSection;

    private ImageButton newSchoolButton;
    private ImageButton newSchoolClassButton;

    private AutoCompleteTextView schoolAutoComplete;
    private SchoolAutoCompleteAdapter schoolAdapter;

    private AutoCompleteTextView schoolClassAutoComplete;
    private SchoolClassAutoCompleteAdapter schoolClassAdapter;

    private SchoolInfo selectedSchoolInfo;
    private SchoolClassInfo selectedSchoolClassInfo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Edit");

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {

            schoolClassSection = (LinearLayout) findViewById(R.id.schoolClassSection);

            if (globalVariable.getCurrentUser().getSchool() == 0) {
                schoolClassSection.setVisibility(View.GONE);
            }

            schoolAdapter = new SchoolAutoCompleteAdapter(this, android.R.layout.simple_list_item_1, globalVariable, utils);

            schoolAutoComplete = (AutoCompleteTextView) findViewById(R.id.schoolAutoComplete);

            // set adapter for the auto complete fields
            schoolAutoComplete.setAdapter(schoolAdapter);

            // specify the minimum type of characters before drop-down list is shown
            schoolAutoComplete.setThreshold(1);

            schoolAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                    selectedSchoolInfo = (SchoolInfo) parent.getItemAtPosition(position);
                    schoolClassSection.setVisibility(View.VISIBLE);

                    //if select new school than clean school class
                    selectedSchoolClassInfo = null;
                    schoolClassAutoComplete.setText("");
                }
            });

            schoolClassAdapter = new SchoolClassAutoCompleteAdapter(this, android.R.layout.simple_list_item_1, globalVariable, utils);

            schoolClassAutoComplete = (AutoCompleteTextView) findViewById(R.id.schoolClassAutoComplete);

            // set adapter for the auto complete fields
            schoolClassAutoComplete.setAdapter(schoolClassAdapter);

            // specify the minimum type of characters before drop-down list is shown
            schoolClassAutoComplete.setThreshold(1);

            schoolClassAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                    selectedSchoolClassInfo = (SchoolClassInfo) parent.getItemAtPosition(position);
                }
            });


            loadSchoolFromServer();
            loadSchoolClassFromServer();


            inputFirstName = (EditText) findViewById(R.id.firstName);
            inputFirstName.setText(globalVariable.getCurrentUser().getFirstName());

            inputLastName = (EditText) findViewById(R.id.lastName);
            inputLastName.setText(globalVariable.getCurrentUser().getLastName());

            inputNickName = (EditText) findViewById(R.id.nickName);
            inputNickName.setText(globalVariable.getCurrentUser().getNickName());

            inputDateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
            inputDateOfBirth.setText(globalVariable.getCurrentUser().getBirthDate());

            inputCountry = (EditText) findViewById(R.id.country);
            inputCountry.setText(globalVariable.getCurrentUser().getCountry());

            inputCity = (EditText) findViewById(R.id.city);
            inputCity.setText(globalVariable.getCurrentUser().getCity());

            newSchoolButton = (ImageButton) findViewById(R.id.newSchoolButton);
            newSchoolButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Intent intentRedirect = new Intent(UserProfileEditActivity.this, AddSchoolActivity.class);
                    startActivity(intentRedirect);
                }

            });
            newSchoolClassButton = (ImageButton) findViewById(R.id.newSchoolClassButton);
            newSchoolClassButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (selectedSchoolInfo == null) {
                        Toast.makeText(getBaseContext(), "School is not selected", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intentRedirect = new Intent(UserProfileEditActivity.this, AddSchoolClassActivity.class);
                        intentRedirect.putExtra("selectedSchoolInfo", selectedSchoolInfo);
                        startActivity(intentRedirect);
                    }
                }

            });
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.profile_edit;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        globalVariable.setSelectedWallPost(null);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        startService(intentBroadcastService);
        registerReceiver(broadcastReceiver, new IntentFilter(EventHub.BROADCAST_PROFILE_UPDATE));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.comment_edit, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send_comment:
                UserProfileEdit userProfileEdit = new UserProfileEdit();

                if (!inputFirstName.getText().toString().equals(globalVariable.getCurrentUser().getFirstName())) {
                    userProfileEdit.setFirstName(inputFirstName.getText().toString());
                }

                if (!inputLastName.getText().toString().equals(globalVariable.getCurrentUser().getLastName())) {
                    userProfileEdit.setLastName(inputLastName.getText().toString());
                }

                if (!inputNickName.getText().toString().equals(globalVariable.getCurrentUser().getNickName())) {
                    userProfileEdit.setNickName(inputNickName.getText().toString());
                }

                if (!inputDateOfBirth.getText().toString().equals(globalVariable.getCurrentUser().getBirthDate())) {
                    userProfileEdit.setDateOfBirth(inputDateOfBirth.getText().toString());
                }

                if (!inputCountry.getText().toString().equals(globalVariable.getCurrentUser().getCountry())) {
                    userProfileEdit.setCountry(inputCountry.getText().toString());
                }

                if (!inputCity.getText().toString().equals(globalVariable.getCurrentUser().getCity())) {
                    userProfileEdit.setCity(inputCity.getText().toString());
                }

                if (selectedSchoolInfo != null && selectedSchoolInfo.getSchoolId() != globalVariable.getCurrentUser().getSchool()) {
                    userProfileEdit.setSchool(selectedSchoolInfo.getSchoolId());
                }

                if (selectedSchoolClassInfo != null && selectedSchoolClassInfo.getSchoolClassId() != globalVariable.getCurrentUser().getSchoolClass()) {
                    userProfileEdit.setSchoolClass(selectedSchoolClassInfo.getSchoolClassId());
                }

                //validation

                boolean noValidationError = true;

                if (selectedSchoolInfo == null) {
                    //TODO validation error school must be selected
                    Toast.makeText(getBaseContext(), "Select existed School", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                }


                //should be after first time use of edit profile and school selected and school class is not selected
                if (selectedSchoolInfo != null && selectedSchoolClassInfo == null) {
                    //TODO validation error schoolclass must be selected
                    Toast.makeText(getBaseContext(), "Select existed School Class", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                }

                if (selectedSchoolInfo != null && selectedSchoolClassInfo != null && selectedSchoolInfo.getSchoolId() != selectedSchoolClassInfo.getSchool().getSchoolId()) {
                    Toast.makeText(getBaseContext(), "Selected Class doesn't belong to selected School", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                }

                if (noValidationError) {
                    sendMessageToServer(utils.getSendUpdateUserProfileInfoJSON(userProfileEdit));
                    onBackPressed();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public SchoolInfo getSelectedSchoolInfo() {
        return selectedSchoolInfo;
    }

    private void loadSchoolFromServer() {
        if (globalVariable.getCurrentUser().getSchool() > 0 && globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_SCHOOL_INFO);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("schoolId", String.valueOf(globalVariable.getCurrentUser().getSchool()));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        Log.d(TAG, "loadSchoolFromServer result = " + result);
                        selectedSchoolInfo = utils.getSchoolInfo(result);
                        if (selectedSchoolInfo == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    schoolAutoComplete.setText(selectedSchoolInfo.getSchoolName());
                                }
                            });
                        }
                    }
                }
            }).request(requestParameters, globalVariable);
        }
    }

    private void showToast(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }

    /**
     * load received wall posts from server with direction
     * */
    private void loadSchoolClassFromServer() {
        if (globalVariable.getCurrentUser().getSchoolClass() > 0 && globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_SCHOOL_CLASS_INFO);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("schoolClassId", String.valueOf(globalVariable.getCurrentUser().getSchoolClass()));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        Log.d(TAG, "loadSchoolClassFromServer result = " + result);
                        selectedSchoolClassInfo = utils.getSchoolClassInfo(result);

                        if (selectedSchoolClassInfo == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    schoolClassAutoComplete.setText(selectedSchoolClassInfo.getClassName());
                                }
                            });
                        }

                    }
                }
            }).request(requestParameters, globalVariable);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String broadcastedMessage = intent.getStringExtra("message");
            Log.d(TAG, "BroadcastReceiver User Profile Changed: broadcastedMessage = " + broadcastedMessage);

            if (broadcastedMessage != null) {
                Intent i = new Intent(UserProfileEditActivity.this, MainActivity.class);
                startActivity(i);
            }
        }
    };
}

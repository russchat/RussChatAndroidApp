package com.pkletsko.russ.chat.ui.activity.common.search;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.List;

/**
 * Created by pkletsko on 23.03.2015.
 */
public class SearchCursorAdapter extends CursorAdapter {

    private List<UserProfile> items;

    private TextView text;

    public SearchCursorAdapter(Context context, Cursor cursor, List<UserProfile> items) {

        super(context, cursor, false);

        this.items = items;

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        text.setText(items.get(cursor.getPosition()).getFullName());

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.search_result_item, parent, false);

        text = (TextView) view.findViewById(R.id.search_result_user_name);

        return view;

    }

}
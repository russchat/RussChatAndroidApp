package com.pkletsko.russ.chat.websocket.message.json.model.user;

import java.util.ArrayList;
import java.util.List;

public class UserProfile {

    private Long userId  = -1L;

    private Long userMDBId = -1L;

    private String firstName = "UnKnown";

    private String lastName = "UnKnown";

    private String fullName = "UnKnown";

    private String nickName = "";

    private String country = "Norge";

    private String city = "Oslo";

    private int school = 0;

    private int schoolClass = 0;

    private int colour = 0;

    private String birthDate = "";

    private String profileIcon = "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg";

    private Long myWallId  = -1L;

    private int followingWalls = 0;

    private int followers  = 0;

    private int myPosts = 0;

    private Boolean followed = false;

    private List<Long> followedWalls = new ArrayList<>();

    private int version = 0;

    private int loggedInUser = -1;

    private int loginSrc  = -1;

    public List<Long> getFollowedWalls() {
        return followedWalls;
    }

    public void setFollowedWalls(List<Long> followedWalls) {
        this.followedWalls = followedWalls;
    }

    public Boolean isFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public int getFollowingWalls() {
        return followingWalls;
    }

    public void setFollowingWalls(int followingWalls) {
        this.followingWalls = followingWalls;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getMyPosts() {
        return myPosts;
    }

    public void setMyPosts(int myPosts) {
        this.myPosts = myPosts;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getProfileIcon() {
        return profileIcon;
    }

    public void setProfileIcon(String profileIcon) {
        this.profileIcon = profileIcon;
    }

    public Long getUserMDBId() {
        return userMDBId;
    }

    public void setUserMDBId(Long userMDBId) {
        this.userMDBId = userMDBId;
    }

    public Long getMyWallId() {
        return myWallId;
    }

    public void setMyWallId(Long myWallId) {
        this.myWallId = myWallId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getSchool() {
        return school;
    }

    public void setSchool(int school) {
        this.school = school;
    }

    public int getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(int schoolClass) {
        this.schoolClass = schoolClass;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(int loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public int getLoginSrc() {
        return loginSrc;
    }

    public void setLoginSrc(int loginSrc) {
        this.loginSrc = loginSrc;
    }

    @Override
    public String toString() {
        return getFullName();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UserProfile) {
            UserProfile pp = (UserProfile) o;
            return (pp.fullName.equals(this.fullName) && pp.userId == this.userId);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = userId != null ? (int)(userId.longValue()^(userId.longValue()>>>32))  : 0;
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        return result;
    }
}

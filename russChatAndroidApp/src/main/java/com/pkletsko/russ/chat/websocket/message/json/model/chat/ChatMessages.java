package com.pkletsko.russ.chat.websocket.message.json.model.chat;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessages extends BasicTransportJSONObject implements JSONResponse {

    private Set<ChatMessage> chatMessages = new HashSet<>();

    private Set<UserProfile> userProfiles = new HashSet<>();

    public Set<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(Set<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }
}

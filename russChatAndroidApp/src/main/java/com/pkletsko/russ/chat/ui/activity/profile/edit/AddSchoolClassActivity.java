package com.pkletsko.russ.chat.ui.activity.profile.edit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;

public class AddSchoolClassActivity extends BaseActivity {

    // LogCat tag
    private static final String TAG = AddSchoolClassActivity.class.getSimpleName();

    private EditText inputClassName;

    private SchoolInfo selectedSchoolInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Add Class");
        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {

            // 2. get person object from intent
            selectedSchoolInfo = (SchoolInfo) getIntent().getSerializableExtra("selectedSchoolInfo");

            inputClassName = (EditText) findViewById(R.id.schoolClassName);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.school_class_edit;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        startService(intentBroadcastService);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.comment_edit, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send_comment:
                SchoolClassInfo schoolClassInfo = new SchoolClassInfo();

                schoolClassInfo.setClassName(inputClassName.getText().toString());
                schoolClassInfo.setSchool(selectedSchoolInfo);

                boolean noValidationError = true;

                if (schoolClassInfo.getClassName().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Type Class Name", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                } else if (schoolClassInfo.getSchool() == null) {
                    Toast.makeText(getBaseContext(), "School is not selected", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                }

                if (noValidationError) {
                    sendMessageToServer(utils.getSendCreateNewSchoolClassJSON(schoolClassInfo));
                    onBackPressed();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //stopService(intentActiveConversation);
        globalVariable.setSelectedWallPost(null);
        startActivity(new Intent(AddSchoolClassActivity.this, UserProfileEditActivity.class));
        finish();
    }
}

package com.pkletsko.russ.chat.ui.activity.wall.comment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivityTest;
import com.pkletsko.russ.chat.ui.activity.common.view.EndlessRecyclerOnScrollListener;
import com.pkletsko.russ.chat.ui.activity.wall.comment.adapter.CommentAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.CommentHelper.COMMENT_LIST_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.CommentHelper.INIT_VALUE;
import static com.pkletsko.russ.chat.ui.activity.common.helper.CommentHelper.appendComment;
import static com.pkletsko.russ.chat.ui.activity.common.helper.CommentHelper.appendComments;
import static com.pkletsko.russ.chat.ui.activity.common.helper.CommentHelper.getCommentId;
import static com.pkletsko.russ.chat.ui.activity.common.helper.CommentHelper.parseMessage;
import static com.pkletsko.russ.chat.ui.activity.common.helper.LikeHelper.LIKE_LIST_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.DOWN;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.FIRST_ITEM;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.UP;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.notifyDataSetChanged;

public class CommentActivity extends BaseActivityTest {

    private static final String TAG = CommentActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;

    private CommentAdapter commentAdapter;

    private ArrayList<Comment> comments = new ArrayList<>();

    private Long currentWallPostId;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.comment_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Comments");

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {


            activity = this;

            currentWallPostId = globalVariable.getSelectedWallPost().getWallMessageId();

            swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

            mRecyclerView = (RecyclerView) findViewById(R.id.commentsRecyclerView);

            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

            mRecyclerView.setLayoutManager(mLayoutManager);

            commentAdapter = new CommentAdapter(this, comments, globalVariable, mRecyclerView);

            mRecyclerView.setAdapter(commentAdapter);

            mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
                    mLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    loadMoreDown();
                }
            });

            // Setup refresh listener which triggers new data loading
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    // Make sure you call swipeContainer.setRefreshing(false)
                    // once the network request has completed successfully.
                    loadMoreUp();
                }
            });

            // Configure the refreshing colors
            swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

            final EditText inputMsg = (EditText) findViewById(R.id.inputMsg);

            final ImageButton btnSend = (ImageButton) findViewById(R.id.btnSend);
            btnSend.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (globalVariable.isAppRunning()) {
                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendCommentJSON(inputMsg.getText().toString(), currentWallPostId));
                        // Clearing the input filed once message was sent
                        inputMsg.setText("");
                    } else {
                        Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            initializationOfCommentsData();
        }
    }


    protected void loadMoreUp() {
        if (comments.isEmpty()) {
            // 5 Case
            loadFromServer(INIT_VALUE);
        } else {
            // 6 Case
            loadFromServer(getCommentId(FIRST_ITEM, comments), UP);
        }
    }

    protected void loadMoreDown() {
        if (comments.size() > 0) {
            Log.d(TAG, "LoadMore userProfiles size = " + comments.size() + ", totalItemsCount = " + comments.size() + " , wallPostId = " + comments.get(comments.size() - 1).getCommentId());
            // 4 Case
            loadFromServer(comments.get(comments.size() - 1).getCommentId());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        globalVariable.setSelectedWallPost(null);
        //unregisterReceiver(broadcastReceiver);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(intentBroadcastService);
        registerReceiver(broadcastReceiver, new IntentFilter(EventHub.BROADCAST_ACTIVE_COMMENT_UPDATE));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializationOfCommentsData() {
        AsyncTask<Void, Void, Set<Comment>> task = new AsyncTask<Void, Void, Set<Comment>>() {
            @Override
            protected Set<Comment> doInBackground(Void... params) {
                return globalVariable.getCommentMDAO().getCommentsByWallMessageId(currentWallPostId, currentUserId);
            }

            @Override
            protected void onPostExecute(Set<Comment> existedComments) {
                Log.i(TAG, "initializationOfCommentsData onPostExecute: existedComments size = " + existedComments.size());

                if (existedComments.isEmpty()) {
                    loadFromServer(INIT_VALUE);
                } else {
                    //TODO do we need comments in cash
                    globalVariable.getCommentCash().put(currentWallPostId, existedComments);

                    appendComments(comments, existedComments);
                    notifyDataSetChanged(activity, commentAdapter);

                    if (comments.size() < COMMENT_LIST_SIZE_LIMIT) {
                        loadFromServer(comments.get(comments.size() - 1).getCommentId());
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * load received wall posts from server
     */
    protected void loadFromServer(final Long lastLoadedCommentId) {
        loadFromServer(lastLoadedCommentId, DOWN);
    }

    /**
     * load received wall posts from server with direction
     */
    protected void loadFromServer(final Long lastLoadedCommentId, final int direction) {
        if (globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_WALL_POST_COMMENT_URL);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("id", currentWallPostId.toString());
            requestParameters.put("commentId", String.valueOf(lastLoadedCommentId));
            requestParameters.put("direction", String.valueOf(direction));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        Log.d(TAG, "loadFromServer result = " + result);

                        final Comments commentsObj = utils.getComments(result);
                        if (commentsObj == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            final Set<Comment> receivedComments = commentsObj.getComments();

                            final int currentCommentsSize = comments.size();
                            final int totalSize = currentCommentsSize + receivedComments.size();

                            //we need user to show name and icon (we need them temporary)
                            for (UserProfile userProfile : commentsObj.getUserProfiles()) {
                                //TODO check if this is not overwrite more extended userProfile object existed in cash
                                globalVariable.getUsersCash().put(userProfile.getUserId(), userProfile);
                            }

                            for (final Comment comment : receivedComments) {
                                // save comment in cash and mdb
                                saveComment(comment);
                            }

                            Set<Comment> commentCash = globalVariable.getCommentCash().get(currentWallPostId);
                            if (commentCash != null) {
                                globalVariable.getCommentCash().get(currentWallPostId).addAll(comments);
                            } else {
                                globalVariable.getCommentCash().put(currentWallPostId, new HashSet<>(comments));
                            }

                            if (totalSize > COMMENT_LIST_SIZE_LIMIT) {
                                //remove items according to limit

                                final int removeItemsNumber = totalSize - LIKE_LIST_SIZE_LIMIT;
                                final ArrayList<Comment> clearedComments = (ArrayList<Comment>) comments.clone();

                                comments.clear();

                                if (UP == direction) {
                                    final int removeLimit = currentCommentsSize - removeItemsNumber;
                                    final int removeFrom = currentCommentsSize - 1;

                                    for (int i = removeFrom; i >= removeLimit; i--) {
                                        removeComment(clearedComments, i);
                                    }

                                    appendComments(comments, receivedComments, clearedComments);
                                } else {
                                    //DOWN
                                    for (int i = 0; i < removeItemsNumber; i++) {
                                        removeComment(clearedComments);
                                    }
                                    appendComments(comments, clearedComments, receivedComments);
                                }
                            } else {
                                appendComments(comments, receivedComments);
                            }
                            notifyDataSetChanged(activity, commentAdapter);
                        }
                    }
                    swipeContainer.setRefreshing(false);
                }
            }).request(requestParameters, globalVariable);
        } else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    private void removeComment(ArrayList<Comment> clearedComments) {
        removeComment(clearedComments, 0);
    }

    public void removeComment(ArrayList<Comment> clearedComments, final int index) {
        final Long userId = clearedComments.get(index).getCommentId();
        //wallPostService.deleteWallPostFromMDB(userId);
        //removeWallPostFromCash(wallPostId);
        //globalVariable.getFollowedWallPostCash().remove(clearedWallPosts.get(index));
        //clearedWallPosts.remove(index);
    }

    public void saveComment(final Comment comment) {
        //wallPost.setReceived(true);
        // putToCash(wallPost, globalVariable.getWallPostCash(), globalVariable.getFollowedWallPostCash());
        //wallPostService.saveWallPostToMDB(wallPost, true);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Comment comment = parseMessage(intent.getStringExtra("message"), utils);

            if (comment == null) {
                showToast("Something went wrong. Bad response from server.");
            }

            // Checking if this comment related to this wall post . If this comment is not for this wall post, do not add it.
            if (comment != null && comment.getWallMessageId().equals(currentWallPostId)) {
                // Appending the message to chat list
                appendComment(comment, comments);
                notifyDataSetChanged(activity, commentAdapter);
            }
        }
    };

    private void showToast(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }
}

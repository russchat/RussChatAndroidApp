package com.pkletsko.russ.chat.ui.activity.profile.edit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;

public class AddSchoolActivity extends BaseActivity {

    // LogCat tag
    private static final String TAG = AddSchoolActivity.class.getSimpleName();

    private EditText inputSchoolName;
    private EditText inputCountry;
    private EditText inputCity;
    private EditText inputAddress;
    private EditText inputZip;
    private EditText inputPhone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Add School");
        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {

            inputSchoolName = (EditText) findViewById(R.id.schoolName);

            inputAddress = (EditText) findViewById(R.id.address);

            inputZip = (EditText) findViewById(R.id.zip);

            inputPhone = (EditText) findViewById(R.id.phone);

            inputCountry = (EditText) findViewById(R.id.country);
            inputCountry.setText(globalVariable.getCurrentUser().getCountry());

            inputCity = (EditText) findViewById(R.id.city);
            inputCity.setText(globalVariable.getCurrentUser().getCity());
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.school_edit;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.comment_edit, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send_comment:
                SchoolInfo schoolInfo = new SchoolInfo();

                schoolInfo.setSchoolName(inputSchoolName.getText().toString());
                schoolInfo.setAddress(inputAddress.getText().toString());
                schoolInfo.setCountry(inputCountry.getText().toString());
                schoolInfo.setCity(inputCity.getText().toString());
                schoolInfo.setZip(inputZip.getText().toString());
                schoolInfo.setTelephone(inputPhone.getText().toString());

                boolean noValidationError = true;

                if (schoolInfo.getSchoolName().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Type School Name", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                } else if (schoolInfo.getAddress().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Type Address", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                } else if (schoolInfo.getCountry().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Type Country", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                } else if (schoolInfo.getCity().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Type City", Toast.LENGTH_LONG).show();
                    noValidationError = false;
                }

                if (noValidationError) {
                    sendMessageToServer(utils.getSendCreateNewSchoolJSON(schoolInfo));
                    onBackPressed();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //stopService(intentActiveConversation);
        globalVariable.setSelectedWallPost(null);
        startActivity(new Intent(AddSchoolActivity.this, UserProfileEditActivity.class));
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        startService(intentBroadcastService);
    }
}

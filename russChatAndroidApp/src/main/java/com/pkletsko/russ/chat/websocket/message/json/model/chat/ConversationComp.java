package com.pkletsko.russ.chat.websocket.message.json.model.chat;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class ConversationComp implements Comparator<Chat> {
    @Override
    public int compare(Chat e1, Chat e2) {
        final long id1 = e1.getConversationId(), id2 = e2.getConversationId();
        if (id1 == id2)
            return 0;
        return id1 < id2 ? 1 : -1;
    }
}

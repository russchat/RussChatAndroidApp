package com.pkletsko.russ.chat.ui.activity.common.img.full.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.ui.activity.common.img.full.screen.adapter.FullScreenImageAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Image;

import java.util.ArrayList;


public class FullScreenViewActivity extends Activity {

	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
    private GlobalClass globalVariable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);

        globalVariable = (GlobalClass) getApplicationContext();

		viewPager = (ViewPager) findViewById(R.id.pager);

		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);

        ArrayList<String> imagePaths = new ArrayList<String>();

        for(final Image image: globalVariable.getFullScreenViewDataModel().getImages()) {
            imagePaths.add(image.getUrl());
        }


		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, imagePaths, globalVariable);

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(globalVariable.getFullScreenViewDataModel().getPosition());
	}
}

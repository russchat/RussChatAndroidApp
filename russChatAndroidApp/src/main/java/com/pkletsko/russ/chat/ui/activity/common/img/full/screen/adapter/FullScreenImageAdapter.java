package com.pkletsko.russ.chat.ui.activity.common.img.full.screen.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.ui.activity.common.img.full.screen.helper.TouchImageView;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

public class FullScreenImageAdapter extends PagerAdapter {

	private Activity _activity;
	private ArrayList<String> _imagePaths;
	private LayoutInflater inflater;
    private GlobalClass globalVariable;

	// constructor
	public FullScreenImageAdapter(Activity activity,  ArrayList<String> imagePaths, GlobalClass globalVariable) {
		this._activity = activity;
		this._imagePaths = imagePaths;
        this.globalVariable = globalVariable;
	}

	@Override
	public int getCount() {
		return this._imagePaths.size();
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        Button btnClose;
 
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
 
        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
//        imgDisplay.setImageBitmap(bitmap);

        final Transformation transformation = globalVariable.getPicassoHelper().getTransformation(imgDisplay);
        globalVariable.getPicassoHelper().loadImage(_imagePaths.get(position), imgDisplay, transformation);

        //Picasso.with(_activity).load(_imagePaths.get(position)).into(imgDisplay);

        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				_activity.finish();
			}
		}); 

        ((ViewPager) container).addView(viewLayout);
 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
 
    }
}

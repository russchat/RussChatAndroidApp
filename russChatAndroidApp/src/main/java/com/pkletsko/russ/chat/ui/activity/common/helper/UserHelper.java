package com.pkletsko.russ.chat.ui.activity.common.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by pkletsko on 02.11.2015.
 */
public class UserHelper {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int USER_LIST_SIZE_LIMIT = 36;
    public static final Long INIT_VALUE = 0L;
    public static final int FIRST_ITEM = 0;

    public static void notifyDataSetChanged(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public static Long getUserProfileId(final int index, ArrayList<UserProfile> existedUserProfiles) {
        return existedUserProfiles.get(index).getUserId();
    }

     /**
     * Appending message to list view
     */
    public static void appendUserProfiles(final ArrayList<UserProfile> existedUsers, Collection... userCollections) {
        for (Collection users : userCollections) {
            existedUsers.addAll(users);
        }
    }
}

package com.pkletsko.russ.chat.ui.activity.common.activity.base;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.ui.activity.login.SplashActivity;

/**
 * Created by pkletsko on 29.05.2015.
 */
public abstract class BaseProfileActivity extends AppCompatActivity {
    // LogCat tag
    private static final String TAG = BaseProfileActivity.class.getSimpleName();

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected ActionBar actionBar;

    protected UserService userService;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        beforeSetContentView();
        setContentView(getLayoutResourceId());

        this.actionBar = getActionBar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.statusbarred));
        }

//        this.actionBar.setDisplayHomeAsUpEnabled(true); // Enabling Back navigation on Action Bar icon

        this.globalVariable = (GlobalClass) getApplicationContext();

        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {

            this.utils = new Utils(getApplicationContext());

            this.client = globalVariable.getClient();

            this.intentBroadcastService = new Intent(this, BroadcastService.class);

            this.userService = new UserServiceImpl(globalVariable, utils);

            serverCheck();
        }
    }

    public void restartAppAfterSleepMode() {
        //this is special method to prevent app from crash after it was in a sleep mode
        //whole app should be re-init
        if (globalVariable.getCurrentUser() == null) {
            Intent intentRedirect = new Intent(this, SplashActivity.class);
            intentRedirect.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentRedirect);
            finish();
        }
    }

    protected abstract int getLayoutResourceId();

    protected void beforeSetContentView(){
        setTheme(R.style.ChatAppRedTheme); // Set here
    }

    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    //basic functionality is empty, to add you code this method should be override
                    //startService(intentBroadcastService);
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            stopService(intentBroadcastService);
                        }
                    });
                }
            }
        }).isServerAlive(globalVariable);
    }

    /**
     * Method to send message to web socket server
     */
    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }
}

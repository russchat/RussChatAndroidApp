package com.pkletsko.russ.chat.ui.activity.login;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.ui.activity.common.img.RoundedTransformation;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.IOException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class PicassoHelper {

    private Picasso picasso;
    private RoundedTransformation roundedTransformation;

    public PicassoHelper(Context context, final String token) {
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
        OkHttpClient picassoClient = new OkHttpClient();
        picassoClient.setSslSocketFactory(globalVariable.getSslSocketFactory());
        picassoClient.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        picassoClient.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("p4", token)
                        .header("Cache-Control", "max-age=" + (60 * 60 * 24 * 365))
                        .build();
                return chain.proceed(newRequest);
            }
        });
        //picassoClient.setCache(new Cache(new File(Environment.getExternalStorageDirectory() + "/com.pkletsko.russ.chat/cache"), Integer.MAX_VALUE));
        picassoClient.setCache(new Cache(context.getCacheDir(), Integer.MAX_VALUE));


        picasso = new Picasso.Builder(context).downloader(new OkHttpDownloader(picassoClient)).build();

        setRoundedTransformation(new RoundedTransformation(150, 0));
    }

    public RoundedTransformation getRoundedTransformation() {
        return roundedTransformation;
    }

    private void setRoundedTransformation(RoundedTransformation roundedTransformation) {
        this.roundedTransformation = roundedTransformation;
    }

    public void loadImage(final String imageUrl, final ImageView imageView, final Transformation transformation) {
        Log.d("Picasso Helper", "imageUrl = " + imageUrl);
        final Picasso picasso = this.picasso;

        picasso.load(imageUrl)
                .transform(transformation).networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        // Try again online if cache failed
                        picasso.load(imageUrl)
                                .transform(transformation)
                                .into(imageView);
                    }
                });
    }

    public void loadImageWithFit(final String imageUrl, final ImageView imageView, final Transformation transformation) {
        Log.d("Picasso Helper", "imageUrl = " + imageUrl);
        final Picasso picasso = this.picasso;

        picasso.load(imageUrl)
                .transform(transformation).networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        // Try again online if cache failed
                        picasso.load(imageUrl)
                                .transform(transformation)
                                .fit()
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        // Try again online if cache failed
                                        String noImageUrl = "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg";
                                        picasso.load(noImageUrl)
                                                .transform(transformation).networkPolicy(NetworkPolicy.OFFLINE)
                                                .fit()
                                                .into(imageView);
                                    }
                                });
                    }
                });
    }

    public void loadImage(final Uri uri, final ImageView imageView, final Transformation transformation) {
        final Picasso picasso = this.picasso;
    }

    public Transformation getTransformation(final ImageView imageView) {
        return new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = imageView.getWidth();

                if(targetWidth == 0) {
                    targetWidth = 100;
                }

                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };

    }
}

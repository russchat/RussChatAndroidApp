package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class UnloadedAttachmentMDAO {
    // LogCat tag
    private static final String TAG = UnloadedAttachmentMDAO.class.getSimpleName();
    // Database fields
    
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_MDB_ID,
            RussSQLiteHelper.COLUMN_ATTACHMENT_NAME,
            RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_PATH,
            RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public UnloadedAttachmentMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public FileToUpload createFileToUpload(final FileToUpload fileToUpload, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_ATTACHMENT_NAME, fileToUpload.getFileName());
        values.put(RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_PATH, fileToUpload.getFilePath());
        values.put(RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID, fileToUpload.getAttachmentId());
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
        long insertId = database.insert(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT, allColumns, RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        FileToUpload newFileToUpload = cursorToFileToUpload(cursor);
        cursor.close();
        return newFileToUpload;
    }

    public void createFileToUploads(final Set<FileToUpload> fileToUploads, final Long appOwnerId) {
        for (FileToUpload fileToUpload: fileToUploads) {
            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_ATTACHMENT_NAME, fileToUpload.getFileName());
            values.put(RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_PATH, fileToUpload.getFilePath());
            values.put(RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID, fileToUpload.getAttachmentId());
            values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
            database.insert(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT, null, values);
        }
    }

    public void deleteFileToUpload(final FileToUpload fileToUpload, final Long appOwnerId) {
        long id = fileToUpload.getFileToUploadMDBId();
        Log.d(TAG, "deleteFileToUpload: fileToUploadMDBId = " + id + "attachmentId = " + fileToUpload.getAttachmentId() + "file path = " + fileToUpload.getFilePath());
        database.delete(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT, RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_MDB_ID + " = " + id + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public List<FileToUpload> getAllFileToUploads() {
        List<FileToUpload> fileToUploadList = new ArrayList<FileToUpload>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FileToUpload fileToUpload = cursorToFileToUpload(cursor);
            fileToUploadList.add(fileToUpload);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return fileToUploadList;
    }

    public Set<FileToUpload> getFileToUploadsByAttachmentId(final Long attachmentId, final Long appOwnerId) {
        Set<FileToUpload> fileToUploadList = new HashSet<FileToUpload>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT, allColumns, RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID + " = " + attachmentId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FileToUpload fileToUpload = cursorToFileToUpload(cursor);
            fileToUploadList.add(fileToUpload);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return fileToUploadList;
    }

    private FileToUpload cursorToFileToUpload(Cursor cursor) {
        FileToUpload fileToUpload = new FileToUpload();
        fileToUpload.setFileToUploadMDBId(cursor.getLong(0));
        fileToUpload.setFileName(cursor.getString(1));
        fileToUpload.setFilePath(cursor.getString(2));
        fileToUpload.setAttachmentId(cursor.getLong(3));
        return fileToUpload;
    }
}

package com.pkletsko.russ.chat.ui.activity.common.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by pkletsko on 02.11.2015.
 */
public class WallHelper {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int WALL_POSTS_SIZE_LIMIT = 36;
    public static final Long INIT_VALUE = 0L;
    public static final int FIRST_ITEM = 0;

    public static void addTempUser(final Long userId, final GlobalClass globalVariable){
        if (globalVariable.getUsersCash().get(userId) == null) {
            //TODO temp solution
            UserProfile userProfile = new UserProfile();
            userProfile.setUserId(userId);
            userProfile.setFirstName("UnKnown");
            userProfile.setFullName("UnKnown");
            userProfile.setProfileIcon("https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg");
            globalVariable.addUserProfileToCash(userProfile);
        }
    }

    public static void notifyItemInserted(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyItemInserted(0);
                }
            }
        });
    }

    public static void notifyWallChanged(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public static void notifyWallItemChanged(final Activity activity, final RecyclerView.Adapter adapter, final int position) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyItemChanged(position);
                }
            }
        });
    }

    public static WallPost parseMessage(final String msg, final Utils utils) {
        return utils.getWallMessageUI(msg);
//        try {
//            JSONObject jObj = new JSONObject(msg);
//
//            MessageObjectType objectType = MessageObjectType.findByCode(jObj.getInt("objectType"));
//
//            if (objectType != null && objectType.equals(MOT_NEW_WALL_MESSAGE)) {
//                // Appending the message to chat list
//                return utils.getWallMessageUI(msg);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    /**
     * Appending message to list view
     */
    public static void appendWallPosts(final ArrayList<WallPost> existedWallPosts, Collection... wallPostCollections) {
        for (Collection wallPosts : wallPostCollections) {
            existedWallPosts.addAll(wallPosts);
        }
    }

    public static void appendWallPost(final WallPost newWallPost, final ArrayList<WallPost> existedWallPosts) {
        existedWallPosts.add(0, newWallPost);
    }

    public static Long getWallPostId(final int index, ArrayList<WallPost> existedWallPosts) {
        return existedWallPosts.get(index).getWallMessageId();
    }

    @SuppressWarnings("unchecked")
    public static void putToCash(final WallPost wallPost, Object... cashes) {
        for (Object cash : cashes) {
            if (cash instanceof Map<?,?>){
                ((Map<Long, WallPost>) cash).put(wallPost.getWallMessageId(), wallPost);
            } else if (cash instanceof Set<?>) {
                ((Set<WallPost>) cash).add(wallPost);
            }
        }
    }
}

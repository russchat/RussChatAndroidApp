package com.pkletsko.russ.chat.ui.activity.wall.fragment.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.storage.service.WallPostService;
import com.pkletsko.russ.chat.storage.service.WallPostServiceImpl;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.adapter.WallPostImageAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Image;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.ArrayList;
import java.util.Set;

public abstract class BaseWallPostDetailsFragment extends Fragment {

    private static final String TAG = BaseWallPostDetailsFragment.class.getSimpleName();

    protected Activity activity;

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected UserService userService;

    protected WallPostService wallPostService;

    //protected SwipeRefreshLayout swipeContainer;
    protected View mainView;
    protected RecyclerView recyclerView;
    protected LinearLayoutManager mLayoutManager;

    protected ArrayList<Image> wallPostImages = new ArrayList<>();
    protected WallPostImageAdapter wallPostImageAdapter;

    /**
     * Initialization of common wall data.
     * Next list of cases will be covered here:
     * 1 Case : when a user log in (first time/N time) and there is no wall posts stored yet in MDB, so cash is empty.
     *          - in this case request with default parameter 0L will be send to server to get first (N) wall posts if exist.
     * 2 Case : when a user log in (second time/N time) and there is already wall posts stored in MDB, so cash is not empty.
     *          - in this case just load wall posts fro cash to UI
     * 3 Case : in case number of cashed wall posts less than LIMIT ,
     *          a new request with last loaded wall post id will be send to server to get additional wall posts.
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();

        globalVariable = (GlobalClass) activity.getApplicationContext();

        utils = new Utils(activity.getApplicationContext());

        client = globalVariable.getClient();

        userService = new UserServiceImpl(globalVariable, utils);

        wallPostService = new WallPostServiceImpl(globalVariable, userService, activity.getApplicationContext());

        intentBroadcastService = new Intent(activity, BroadcastService.class);

//        final Set<WallPost> cashedWallPosts = getCash();
//
//        if (cashedWallPosts.isEmpty()) {
//            // 1 Case
//            loadFromServer(INIT_VALUE);
//        } else {
//            // 2 Case
//            appendWallPosts(wallPostImages, cashedWallPosts);
//            notifyWallChanged(getActivity(), wallPostImageAdapter);
//
//            // 3 Case
//            if (cashedWallPosts.size() < WALL_POSTS_SIZE_LIMIT) {
//                loadFromServer(getWallPostId(wallPostImages.size() - 1, wallPostImages));
//            }
//        }
    }

    protected abstract Set<WallPost> getCash();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //swipeContainer = (SwipeRefreshLayout) inflater.inflate(R.layout.wall_post_details, container, false);

        mainView = (View) inflater.inflate(R.layout.wall_post_details, container, false);
        recyclerView = (RecyclerView) mainView.findViewById(R.id.recyclerView);

        mLayoutManager = new LinearLayoutManager(activity);

        recyclerView.setLayoutManager(mLayoutManager);

        final Set<FileToUpload> fileToUploadSet = globalVariable.getWallPostLocalImages().get(globalVariable.getSelectedWallPost().getWallMessageId());
        if (fileToUploadSet == null) {
            for (String fileName : globalVariable.getSelectedWallPost().getAttachments()) {
                final Image image = new Image();
                final String cleanedFileName = fileName.substring(0, fileName.indexOf("."));
                final String imageUrl = globalVariable.getFullServicePath() + globalVariable.CONFIG_LOAD_IMAGE_URL + "?p0=" + globalVariable.getCurrentUser().getUserId() + "&p1=" + globalVariable.getSelectedWallPost().getFromUserId() + "&p2=" + globalVariable.getSelectedWallPost().getWallMessageId() + "&p3=" + cleanedFileName;
                Log.d(TAG, "imageUrl = " + imageUrl);
                image.setUrl(imageUrl);
                wallPostImages.add(image);
            }
        } else {
            for (final FileToUpload fileToUpload : fileToUploadSet) {
                final Image image = new Image();
                image.setFileToUpload(fileToUpload);
                image.setUrl(fileToUpload.getFilePath());
                wallPostImages.add(image);
            }
        }

        wallPostImageAdapter = new WallPostImageAdapter(getActivity(), wallPostImages, globalVariable);

        recyclerView.setAdapter(wallPostImageAdapter);

//        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
//                mLayoutManager) {
//            @Override
//            public void onLoadMore(int current_page) {
//                loadMoreDown();
//            }
//        });
//
//        // Setup refresh listener which triggers new data loading
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                //loadMoreUp();
//                if (swipeContainer != null) {
//                swipeContainer.setRefreshing(false);
//            }
//
//            }
//        });

//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);
//
//        serverCheck();

        fragmentInitializationBody(inflater, container, savedInstanceState);

        return mainView;
    }

    /**
     * Re Initialization of common wall data. Happens each time we come back from other Activity
     * Next list of cases will be covered here:
     * 5 Case : when a user scroll up and there is no wall posts loaded yet.
     *          - in this case request with default parameter 0L will be send to server to get first (N) wall posts if exist.
     * 6 Case : when a user scroll up and there is already some wall posts loaded.
     *          a new request with last loaded wall post id will be send to server to get additional wall posts.
     * */
//    protected void loadMoreUp() {
//        if (wallPostImages.isEmpty()) {
//            // 5 Case
//            loadFromServer(INIT_VALUE);
//        } else {
//            // 6 Case
//            loadFromServer(getWallPostId(FIRST_ITEM, wallPostImages), UP);
//        }
//    }

    /**
     * Re Initialization of common wall data. Happens each time we come back from other Activity
     * Next list of cases will be covered here:
     * 4 Case : when a user scroll down.
     *          - in this case request with parameter (last wall post id from UI list) will be send to server to get additional (N) wall posts if exist.
     * */
//    protected void loadMoreDown() {
//        if (wallPostImages.size() > 0) {
//            Log.d(TAG, "LoadMore wallPostImages size = " + wallPostImages.size() + ", totalItemsCount = " + wallPostImages.size() + " , wallPostId = " + wallPostImages.get(wallPostImages.size() - 1).getWallMessageId());
//            // 4 Case
//            loadFromServer(getWallPostId(wallPostImages.size() - 1, wallPostImages));
//        }
//    }

    protected void fragmentInitializationBody(LayoutInflater inflater, ViewGroup container,
                                              Bundle savedInstanceState) {
        //nothing by default
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().startService(intentBroadcastService);
//        getActivity().registerReceiver(broadcastWallPostReceiver, new IntentFilter(EventHub.BROADCAST_ACTIVE_WALL_UPDATE));
//        getActivity().registerReceiver(broadcastMyWallPostReceiver, new IntentFilter(EventHub.BROADCAST_ACTIVE_MY_WALL_UPDATE));
//        getActivity().registerReceiver(broadcastNotifyDataSetChanged, new IntentFilter(EventHub.BROADCAST_NOTIFY_DATA_SET_CHANGED));
    }

    @Override
    public void onPause() {
//        getActivity().unregisterReceiver(broadcastWallPostReceiver);
//        getActivity().unregisterReceiver(broadcastMyWallPostReceiver);
//        getActivity().unregisterReceiver(broadcastNotifyDataSetChanged);
        super.onPause();
    }

//    protected BroadcastReceiver broadcastWallPostReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            final String broadcastedMessage = intent.getStringExtra("message");
//
//            Log.d(TAG, "BroadcastReceiver of Wall: broadcastedMessage = " + broadcastedMessage);
//
//            broadcastWallPostReceiverHandler(broadcastedMessage);
//            notifyWallChanged(getActivity(), wallPostImageAdapter);
//        }
//    };
//
//    protected BroadcastReceiver broadcastMyWallPostReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            final String broadcastedMessage = intent.getStringExtra("message");
//
//            Log.d(TAG, "BroadcastReceiver of Wall: broadcastedMessage = " + broadcastedMessage);
//
//            broadcastMyWallPostReceiverHandler(broadcastedMessage);
//            notifyWallChanged(getActivity(), wallPostImageAdapter);
//        }
//    };
//
//    protected void broadcastMyWallPostReceiverHandler(String broadcastedMessage) {}
//    protected void broadcastWallPostReceiverHandler(String broadcastedMessage) {}
//
//    protected BroadcastReceiver broadcastNotifyDataSetChanged = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            long wallPostId = intent.getLongExtra("wallPostId", -1L);
//
//            for (int i = 0; i < wallPostImages.size(); i++) {
//                if (wallPostImages.get(i).getWallMessageId() == wallPostId) {
//                    // TODO find what item have to be updated
//                    WallHelper.notifyWallItemChanged(getActivity(), wallPostImageAdapter, i);
//                }
//            }
//
//            //notifyWallChanged(getActivity(), wallPostImageAdapter);
//        }
//    };

    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    //basic functionality is empty, to add you code this method should be override
                } else {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(activity.getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            activity.stopService(intentBroadcastService);
                        }
                    });
                }
            }
        }).isServerAlive(globalVariable);
    }

//    /**
//     * Method to send message to web socket server
//     */
//    protected void sendMessageToServer(String message) {
//        if (client != null && client.isConnected()) {
//            client.send(message);
//        }
//    }
//
//    protected Map<String, String> getRequestParameters() {
//        final Map<String,String> requestParameters = new HashMap<>();
//        requestParameters.put(RequestUtil.REQUEST_URL_KEY, GlobalClass.CONFIG_GET_USER_WALL_POSTS_URL);
//        requestParameters.put("id", globalVariable.getCurrentUser().getUserId().toString());
//        return requestParameters;
//    }
//
//    /**
//     * load received wall posts from server
//     * */
//    protected void loadFromServer(final Long lastLoadedWallPostId) {
//        loadFromServer(lastLoadedWallPostId, DOWN);
//    }
//
//    /**
//     * load received wall posts from server with direction
//     * */
//    protected void loadFromServer(final Long lastLoadedWallPostId, final int direction) {
//        if (globalVariable.isAppRunning()) {
//            final Map<String, String> requestParameters = getRequestParameters();
//            requestParameters.put("token", globalVariable.getServerToken());
//            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
//            requestParameters.put("postId", String.valueOf(lastLoadedWallPostId));
//            requestParameters.put("direction", String.valueOf(direction));
//
//            (new RequestUtil() {
//                @Override
//                public void onPostRequestResponse(String result) {
//                    if (result != null && !result.isEmpty()) {
//                        final Set<WallPost> receivedWallPosts = utils.getUserWallPostsResponse(result).getWallPosts();
//                        final int currentWallSize = wallPostImages.size();
//                        final int totalSize = currentWallSize + receivedWallPosts.size();
//
//                        for (final WallPost wallPost : receivedWallPosts) {
//                            // in case information about the user is not loaded yet
//                            addTempUser(wallPost.getFromUserId(), globalVariable);
//
//                            // save wall post in cash and mdb
//                            saveWallPost(wallPost);
//                        }
//
//                        if (totalSize > WALL_POSTS_SIZE_LIMIT) {
//                            //remove items according to limit
//
//                            final int removeItemsNumber = totalSize - WALL_POSTS_SIZE_LIMIT;
//                            final ArrayList<WallPost> clearedWallPosts = (ArrayList<WallPost>) wallPostImages.clone();
//
//                            wallPostImages.clear();
//
//                            if (UP == direction) {
//                                final int removeLimit = currentWallSize - removeItemsNumber;
//                                final int removeFrom = currentWallSize - 1;
//
//                                for (int i = removeFrom; i >= removeLimit; i--) {
//                                    removeWallPost(clearedWallPosts, i);
//                                }
//
//                                appendWallPosts(wallPostImages, receivedWallPosts, clearedWallPosts);
//                            } else {
//                                //DOWN
//                                for (int i = 0; i < removeItemsNumber; i++) {
//                                    removeWallPost(clearedWallPosts);
//                                }
//                                appendWallPosts(wallPostImages, clearedWallPosts, receivedWallPosts);
//                            }
//                        } else {
//                            appendWallPosts(wallPostImages, receivedWallPosts);
//                        }
//                        notifyWallChanged(activity, wallPostImageAdapter);
//                    }
//                    swipeContainer.setRefreshing(false);
//                }
//            }).request(requestParameters);
//        } else {
//            if (swipeContainer != null) {
//                swipeContainer.setRefreshing(false);
//            }
//        }
//    }
//
//    protected void removeWallPostFromCash(final Long wallPostId) {
//        globalVariable.getWallPostCash().remove(wallPostId);
//    }
//
//    private void removeWallPost(ArrayList<WallPost> clearedWallPosts){
//        removeWallPost(clearedWallPosts, 0);
//    }
//
//    protected abstract void removeWallPost(ArrayList<WallPost> clearedWallPosts, final int index);
//
//    protected abstract  void saveWallPost(final WallPost wallPost);
}

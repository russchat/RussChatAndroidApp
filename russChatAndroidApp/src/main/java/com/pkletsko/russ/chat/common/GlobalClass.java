package com.pkletsko.russ.chat.common;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.storage.dao.AttachmentMDAO;
import com.pkletsko.russ.chat.storage.dao.CommentMDAO;
import com.pkletsko.russ.chat.storage.dao.ConversationMDAO;
import com.pkletsko.russ.chat.storage.dao.IPAddressMDAO;
import com.pkletsko.russ.chat.storage.dao.LikeMDAO;
import com.pkletsko.russ.chat.storage.dao.MessageMDAO;
import com.pkletsko.russ.chat.storage.dao.UnloadedAttachmentMDAO;
import com.pkletsko.russ.chat.storage.dao.UnloadedAttachmentWallPostLinkMDAO;
import com.pkletsko.russ.chat.storage.dao.UserMDAO;
import com.pkletsko.russ.chat.storage.dao.WallMessageMDAO;
import com.pkletsko.russ.chat.ui.activity.common.img.model.FullScreenViewDataModel;
import com.pkletsko.russ.chat.ui.activity.common.tab.TabEnum;
import com.pkletsko.russ.chat.ui.activity.login.PicassoHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ConversationComp;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPostComp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.net.ssl.SSLSocketFactory;

import okhttp3.OkHttpClient;


public class GlobalClass extends Application {

    private static final String TAG = GlobalClass.class.getSimpleName();

      private String ip = "10.0.0.58:8443";
    //private String ip = "172.21.0.149:8443";
    //private String ip = "139.162.130.224";
//    private String ip = "10.0.0.58:8080";
//    private String ip = "10.2.1.28:8080";
    //private String ip = "139.162.130.224:80";
//    private String ip = "192.168.181.96:8080";
//    private String ip = "10.0.2.2:8080"; // virtual device
//    private String ip = "10.0.0.46:8117";
    //private String ip = "85.165.113.248:8333";
    //private String ip = "172.21.0.145:8080";


    //POST

    /*CONFIG_CHECK_SERVER  - ping server if it is alive*/
    public static String CONFIG_CHECK_SERVER = "/check/checkserver";

    /*CONFIG_GET_FOLLOWERS_URL  - get followers (NOT endless list YET)*/
    public static String CONFIG_GET_FOLLOWERS_URL = "/user/followers";

    /*CONFIG_GET_FOLLOWING_URL  - get following users (NOT endless list YET)*/
    public static String CONFIG_GET_FOLLOWING_URL = "/user/following";

    /*CONFIG_GET_USER_WALL_POSTS_URL  - get my wall posts / history wall posts (endless list)*/
    public static String CONFIG_GET_USER_WALL_POSTS_URL = "/wall/posts";

    /*CONFIG_GET_USER_RECEIVED_WALL_POSTS_URL  - get wall posts which were received by this user (endless list)*/
    public static String CONFIG_GET_USER_RECEIVED_WALL_POSTS_URL = "/wall/receivedposts";

    /*CONFIG_GET_USER_RECEIVED_WALL_POSTS_URL  - get likes related to selected wall post (endless list)*/
    public static String CONFIG_GET_USERS_LIKED_URL = "/wall/likes";

    /*CONFIG_GET_WALL_POST_COMMENT_URL  - get comments related to selected wall post (endless list)*/
    public static String CONFIG_GET_WALL_POST_COMMENT_URL = "/wall/comments";

    /*CONFIG_UPLOAD_URL  - upload photo to server*/
    public static String CONFIG_UPLOAD_URL = "/wall/upload";

    //GET
    public static String CONFIG_LOAD_IMAGE_URL = "/wall/image";

    /*CONFIG_GET_USER_CONVERSATIONS_URL  - get my conversations (endless list)*/
    public static String CONFIG_GET_USER_CONVERSATIONS_URL = "/chat/conversations";

    /*CONFIG_GET_CONVERSATIONS_MESSAGES_URL  - get conversation messages (endless list)*/
    public static String CONFIG_GET_CONVERSATION_MESSAGES_URL = "/chat/chatmessages";

    /*CONFIG_GET_SCHOOL  - search school by input for autoCompleteAdapter*/
    public static String CONFIG_GET_SCHOOL = "/school/school";

    /*CONFIG_GET_SCHOOL_CLASS  - search school class by input for autoCompleteAdapter*/
    public static String CONFIG_GET_SCHOOL_CLASS = "/school/schoolclass";

    /*CONFIG_GET_SCHOOL_INFO  - get information about selected school*/
    public static String CONFIG_GET_SCHOOL_INFO = "/school/schoolinfo";

    /*CONFIG_GET_SCHOOL_CLASS_INFO  - get information about selected school class*/
    public static String CONFIG_GET_SCHOOL_CLASS_INFO = "/school/schoolclassinfo";

    public static final String URL_WEBSOCKET = "/" + RequestUtil.SERVICE_NAME + "/camel/chat?token=";

    public String getFullServicePath () {
        return RequestUtil.HTTPS + getIp() + RequestUtil.SERVICE_PATH;
    }

    /*
     *  Mobile Database Access Object
     */
    private ConversationMDAO conversationMDAO;
    private MessageMDAO messageMDAO;
    private WallMessageMDAO wallMessageMDAO;
    private UserMDAO userMDAO;
    private CommentMDAO commentMDAO;
    private LikeMDAO likeMDAO;
    private AttachmentMDAO attachmentMDAO;
    private IPAddressMDAO ipAddressMDAO;
    private UnloadedAttachmentWallPostLinkMDAO unloadedAttachmentWallPostLinkMDAO;
    private UnloadedAttachmentMDAO unloadedAttachmentMDAO;

    /*
     *  Common
     */

    private boolean isAppRunning = false;

    private boolean googleLogOut;

    private String token;
    private LoginSourceType loginSourceType;
    private String serverToken;

    private WebSocketClient client;

    private FullScreenViewDataModel fullScreenViewDataModel;

    private Set<UserProfile> searchResult = new HashSet<UserProfile>();
    private boolean search;
    private String searchQuery = "";

    private TabEnum activeTab;
    private Long openProfile;
    private int openProfileCurrentTab = 0;

    private UserProfile currentUser;
    private Map<Long, UserProfile> usersCash = new HashMap<Long, UserProfile>();

    private Set<UserProfile> usersStoreQueue = new HashSet<>();

    private PicassoHelper picassoHelper;

    private OkHttpClient okHttpClient;

    private SSLSocketFactory sslSocketFactory;

    public SSLSocketFactory getSslSocketFactory() {
        return sslSocketFactory;
    }

    public void setSslSocketFactory(SSLSocketFactory sslSocketFactory) {
        this.sslSocketFactory = sslSocketFactory;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public void setOkHttpClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    /*
             *  Chat
             */
    private Chat activeConversation;
    private Set<Chat> conversationCash = new TreeSet<>(new ConversationComp());
    private Map<Long, Chat> chatCash = new HashMap<>();
    private Map<Long, Integer> unreadMessagesCash = new HashMap<Long, Integer>();
    private Map<Long, Set<ChatMessage>> messageCash = new HashMap<>();

    /*
     *  Wall
     */
    private WallPost selectedWallPost;

    private Map<Long, Set<Comment>> commentCash = new HashMap<Long, Set<Comment>>();
    private Map<Long, Set<Like>> likeCash = new HashMap<Long, Set<Like>>();

    private Map<Long, WallPost> wallPostCash = new HashMap<Long, WallPost>();
    private Set<WallPost> myWallPostCash = new TreeSet<WallPost>(new WallPostComp());
    private Set<WallPost> followedWallPostCash = new TreeSet<WallPost>(new WallPostComp());

    private Map<Long, Set<FileToUpload>> queueToUploadFiles = new HashMap<Long, Set<FileToUpload>>();
    private Map<Long, Set<FileToUpload>> wallPostLocalImages = new HashMap<Long, Set<FileToUpload>>();
    private Map<Long, Long> attachmentOfWallPostQueueCash = new HashMap<Long, Long>();

    private Map<Long, FileToUpload> currentUploadQueue = new HashMap<>();

    public Map<Long, FileToUpload> getCurrentUploadQueue() {
        return currentUploadQueue;
    }

    public void setCurrentUploadQueue(Map<Long, FileToUpload> currentUploadQueue) {
        this.currentUploadQueue = currentUploadQueue;
    }

    /*
         *  Add and Remove
         */
    public void addUserProfileToCash(final UserProfile userProfile) {
        usersCash.put(userProfile.getUserId(), userProfile);
    }

    public void addLikesToCash(final Long wallPostId, final Set<Like> likes) {
        if (likeCash.get(wallPostId) == null) {
            likeCash.put(wallPostId, likes);
        } else {
            likeCash.get(wallPostId).addAll(likes);
        }
    }

    public void removeLikesFromCash(final Long wallPostId, final Set<Like> likes) {
        if (likeCash.get(wallPostId) != null) {
            likeCash.get(wallPostId).removeAll(likes);
        }
    }

    public Set<Chat> addToConversationCash(final Chat chat) {
        this.conversationCash.add(chat);
        return this.conversationCash;
    }

    /*
     *  Getter and Setter
     */

    /*
     *  Mobile Database Access Object
     */
    public ConversationMDAO getConversationMDAO() {
        return conversationMDAO;
    }

    public void setConversationMDAO(ConversationMDAO conversationMDAO) {
        this.conversationMDAO = conversationMDAO;
    }

    public MessageMDAO getMessageMDAO() {
        return messageMDAO;
    }

    public void setMessageMDAO(MessageMDAO messageMDAO) {
        this.messageMDAO = messageMDAO;
    }

    public WallMessageMDAO getWallMessageMDAO() {
        return wallMessageMDAO;
    }

    public void setWallMessageMDAO(WallMessageMDAO wallMessageMDAO) {
        this.wallMessageMDAO = wallMessageMDAO;
    }

    public UserMDAO getUserMDAO() {
        return userMDAO;
    }

    public void setUserMDAO(UserMDAO userMDAO) {
        this.userMDAO = userMDAO;
    }

    public CommentMDAO getCommentMDAO() {
        return commentMDAO;
    }

    public void setCommentMDAO(CommentMDAO commentMDAO) {
        this.commentMDAO = commentMDAO;
    }

    public LikeMDAO getLikeMDAO() {
        return likeMDAO;
    }

    public void setLikeMDAO(LikeMDAO likeMDAO) {
        this.likeMDAO = likeMDAO;
    }

    public AttachmentMDAO getAttachmentMDAO() {
        return attachmentMDAO;
    }

    public void setAttachmentMDAO(AttachmentMDAO attachmentMDAO) {
        this.attachmentMDAO = attachmentMDAO;
    }

    public IPAddressMDAO getIpAddressMDAO() {
        return ipAddressMDAO;
    }

    public void setIpAddressMDAO(IPAddressMDAO ipAddressMDAO) {
        this.ipAddressMDAO = ipAddressMDAO;
    }

    public UnloadedAttachmentWallPostLinkMDAO getUnloadedAttachmentWallPostLinkMDAO() {
        return unloadedAttachmentWallPostLinkMDAO;
    }

    public void setUnloadedAttachmentWallPostLinkMDAO(UnloadedAttachmentWallPostLinkMDAO unloadedAttachmentWallPostLinkMDAO) {
        this.unloadedAttachmentWallPostLinkMDAO = unloadedAttachmentWallPostLinkMDAO;
    }

    public UnloadedAttachmentMDAO getUnloadedAttachmentMDAO() {
        return unloadedAttachmentMDAO;
    }

    public void setUnloadedAttachmentMDAO(UnloadedAttachmentMDAO unloadedAttachmentMDAO) {
        this.unloadedAttachmentMDAO = unloadedAttachmentMDAO;
    }

    /*
         *  Common
         */

    public boolean isAppRunning() {
        return isAppRunning;
    }

    public void setIsAppRunning(boolean isAppRunning) {
        Log.d(TAG, "setIsAppRunning() = " + isAppRunning);
        this.isAppRunning = isAppRunning;
    }

    public boolean isGoogleLogOut() {
        return googleLogOut;
    }

    public void setGoogleLogOut(boolean googleLogOut) {
        this.googleLogOut = googleLogOut;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginSourceType getLoginSourceType() {
        return loginSourceType;
    }

    public void setLoginSourceType(LoginSourceType loginSourceType) {
        this.loginSourceType = loginSourceType;
    }

    public String getServerToken() {
        return serverToken;
    }

    public void setServerToken(String serverToken) {
        this.serverToken = serverToken;
    }

    public WebSocketClient getClient() {
        return client;
    }

    public void setClient(WebSocketClient client) {
        this.client = client;
    }

    public FullScreenViewDataModel getFullScreenViewDataModel() {
        return fullScreenViewDataModel;
    }

    public void setFullScreenViewDataModel(FullScreenViewDataModel fullScreenViewDataModel) {
        this.fullScreenViewDataModel = fullScreenViewDataModel;
    }

    public Set<UserProfile> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(Set<UserProfile> searchResult) {
        this.searchResult = searchResult;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public TabEnum getActiveTab() {
        return activeTab;
    }

    public void setActiveTab(TabEnum activeTab) {
        this.activeTab = activeTab;
    }

    public Long getOpenProfile() {
        return openProfile;
    }

    public void setOpenProfile(Long openProfile) {
        this.openProfile = openProfile;
    }

    public int getOpenProfileCurrentTab() {
        return openProfileCurrentTab;
    }

    public void setOpenProfileCurrentTab(int openProfileCurrentTab) {
        this.openProfileCurrentTab = openProfileCurrentTab;
    }

    public UserProfile getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserProfile currentUser) {
        this.currentUser = currentUser;
    }

    public Map<Long, UserProfile> getUsersCash() {
        return usersCash;
    }

    public Set<UserProfile> getUsersStoreQueue() {
        return usersStoreQueue;
    }

    public void addUserStoreQueue(UserProfile userProfile) {
        usersStoreQueue.add(userProfile);
    }

    public PicassoHelper getPicassoHelper() {
        return picassoHelper;
    }

    public void setPicassoHelper(PicassoHelper picassoHelper) {
        this.picassoHelper = picassoHelper;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    /*
                                     *  Chat
                                     */
    public Chat getActiveConversation() {
        return activeConversation;
    }

    public void setActiveConversation(Chat activeConversation) {
        this.activeConversation = activeConversation;
    }

    public Set<Chat> getConversationCash() {
        return conversationCash;
    }

    public void setConversationCash(Set<Chat> conversationCash) {
        this.conversationCash = conversationCash;
    }

    public Map<Long, Chat> getChatCash() {
        return chatCash;
    }

    public void setChatCash(Map<Long, Chat> chatCash) {
        this.chatCash = chatCash;
    }

    public Map<Long, Integer> getUnreadMessagesCash() {
        return unreadMessagesCash;
    }

    public void setUnreadMessagesCash(Map<Long, Integer> unreadMessagesCash) {
        this.unreadMessagesCash = unreadMessagesCash;
    }

    public Map<Long, Set<ChatMessage>> getMessageCash() {
        return messageCash;
    }

    public void setMessageCash(Map<Long, Set<ChatMessage>> messageCash) {
        this.messageCash = messageCash;
    }

    /*
     *  Wall
     */

    public WallPost getSelectedWallPost() {
        return selectedWallPost;
    }

    public void setSelectedWallPost(WallPost selectedWallPost) {
        this.selectedWallPost = selectedWallPost;
    }

    public Map<Long, Set<Comment>> getCommentCash() {
        return commentCash;
    }

    public void setCommentCash(Map<Long, Set<Comment>> commentCash) {
        this.commentCash = commentCash;
    }

    public Map<Long, Set<Like>> getLikeCash() {
        return likeCash;
    }

    public void setLikeCash(Map<Long, Set<Like>> likeCash) {
        this.likeCash = likeCash;
    }

    public Map<Long, WallPost> getWallPostCash() {
        return wallPostCash;
    }

    public void setWallPostCash(Map<Long, WallPost> wallPostCash) {
        this.wallPostCash = wallPostCash;
    }

    public Set<WallPost> getMyWallPostCash() {
        return myWallPostCash;
    }

    public void setMyWallPostCash(Set<WallPost> myWallPostCash) {
        this.myWallPostCash = myWallPostCash;
    }

    public Set<WallPost> getFollowedWallPostCash() {
        return followedWallPostCash;
    }

    public void setFollowedWallPostCash(Set<WallPost> followedWallPostCash) {
        this.followedWallPostCash = followedWallPostCash;
    }

    public Map<Long, Set<FileToUpload>> getQueueToUploadFiles() {
        return queueToUploadFiles;
    }

    public void setQueueToUploadFiles(Map<Long, Set<FileToUpload>> queueToUploadFiles) {
        this.queueToUploadFiles = queueToUploadFiles;
    }

    public Map<Long, Set<FileToUpload>> getWallPostLocalImages() {
        return wallPostLocalImages;
    }

    public void setWallPostLocalImages(Map<Long, Set<FileToUpload>> wallPostLocalImages) {
        this.wallPostLocalImages = wallPostLocalImages;
    }

    public Map<Long, Long> getAttachmentOfWallPostQueueCash() {
        return attachmentOfWallPostQueueCash;
    }

    public void setAttachmentOfWallPostQueueCash(Map<Long, Long> attachmentOfWallPostQueueCash) {
        this.attachmentOfWallPostQueueCash = attachmentOfWallPostQueueCash;
    }

    /**
     * Method to send message to web socket server
     */
    public void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }

    public void notifycation(String notificationTitle, String notificationMessage) {
        NotificationCompat.Builder b = new NotificationCompat.Builder(this);

        b.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL);

        b.setContentTitle(notificationTitle)
                .setContentText(notificationMessage)
                .setSmallIcon(R.drawable.ic_action_email)
                .setTicker("New Message");


        NotificationManager mgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mgr.notify(999, b.build());
    }


}

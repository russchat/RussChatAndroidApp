package com.pkletsko.russ.chat.common.event.handler.user;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.LoginSourceType;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.ui.activity.login.PicassoHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserLogin;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfiles;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SearchResult;

/**
 * Created by pkletsko on 26.05.2015.
 */
public class UserEventHandler {

    private static final String TAG = UserEventHandler.class.getSimpleName();

    private UserService userService;

    private GlobalClass globalVariable;

    private Utils utils;

    private Context context;

    private Intent intentProfileEdited;
    private Intent intentChatUserSearchResult;
    private Intent intentGetUserProfile;
    private Intent intentSearchResult;

    public UserEventHandler(final GlobalClass globalVariable, final Utils utils, final Context context) {
        this.userService = new UserServiceImpl(globalVariable, utils);
        this.globalVariable = globalVariable;
        this.utils = utils;
        this.context = context;

        this.intentProfileEdited = new Intent(EventHub.BROADCAST_PROFILE_UPDATE);
        this.intentSearchResult = new Intent(EventHub.BROADCAST_SEARCH_RESULT);
        this.intentGetUserProfile = new Intent(EventHub.BROADCAST_GET_USER_PROFILE);
        this.intentChatUserSearchResult = new Intent(EventHub.BROADCAST_CHAT_USER_SEARCH_RESULT);
    }

    public void searchEventHandler(final String msg) {
        if (globalVariable.getCurrentUser() != null) {
            // async get data from database and after set data to global variable and than call redirect to login activity
            SearchResult searchResult = utils.getSearchResult(msg);
            if (searchResult == null) {
                showToast("Something went wrong. Bad response from server.");
            } else {
                globalVariable.setSearchResult(searchResult.getUserProfiles());
                context.sendBroadcast(intentSearchResult);
            }
        }
    }

    public void chatUserSearchEventHandler(final String msg) {
        if (globalVariable.getCurrentUser() != null) {
            intentChatUserSearchResult.putExtra("message", msg);
            context.sendBroadcast(intentChatUserSearchResult);
        }
    }

    public void loginStatusEventHandler(final String msg) {
        Log.d(TAG, "loginStatusEventHandler = " + msg);
        UserLogin userLogin = utils.getUserLoginResponse(msg);
        if (userLogin == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            if (userLogin.getLoginStatus().equals("OK")) {
                globalVariable.setCurrentUser(userLogin.getUserProfile());
                globalVariable.setServerToken(userLogin.getToken());
                globalVariable.setPicassoHelper(new PicassoHelper(context, userLogin.getToken()));
                final UserProfile currentUser = userLogin.getUserProfile();
                currentUser.setLoggedInUser(1);

                //save user login provider
                if (globalVariable.getLoginSourceType().equals(LoginSourceType.FACEBOOK)) {
                    currentUser.setLoginSrc(1);
                } else if (globalVariable.getLoginSourceType().equals(LoginSourceType.GOOGLE)) {
                    currentUser.setLoginSrc(2);
                }

                // if it is a new user it will be stored to mbd if existed and version is different , so updated in mdb.
                // latest version of this will be put cash
                userService.storeUserToMDB(currentUser);

                Log.d(TAG, " Request unreceived information from server");
                // Sending message to web socket server
                globalVariable.sendMessageToServer(utils.getUnReceivedInformationRequestJSON());
            }
        }
    }

    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    public void updateUserProfileInfoEventHandler(final String msg) {
        Log.d(TAG, "Update user Profile information");
        UserProfiles userProfiles = utils.getUserProfiles(msg);
        if (userProfiles == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            UserProfile userProfile = userProfiles.getUserProfiles().iterator().next();

            // update session user profile
            globalVariable.setCurrentUser(userProfile);
            globalVariable.getUsersCash().put(userProfile.getUserId(), userProfile);

            // update database
            userService.saveOrUpdateUserMDB(userProfile);

            intentProfileEdited.putExtra("message", msg);
            context.sendBroadcast(intentProfileEdited);
        }
    }

    public void userProfileEventHandler(final String msg) {
        Log.d(TAG, "Update user Profile information");
        UserProfileResponse userProfileResponse = utils.getUserProfileResponse(msg);
        if (userProfileResponse == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            UserProfile userProfile = userProfileResponse.getUserProfile();

            // update session user profile
            if (userProfile.getUserId() == globalVariable.getCurrentUser().getUserId()) {
                userProfile.setLoggedInUser(globalVariable.getCurrentUser().getLoggedInUser());
                userProfile.setLoginSrc(globalVariable.getCurrentUser().getLoginSrc());

                globalVariable.setCurrentUser(userProfile);
            }

            globalVariable.getUsersCash().put(userProfile.getUserId(), userProfile);

            // update database
            userService.storeUserToMDB(userProfile);

            context.sendBroadcast(intentGetUserProfile);
        }
    }
}

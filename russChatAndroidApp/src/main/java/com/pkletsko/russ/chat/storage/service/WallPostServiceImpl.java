package com.pkletsko.russ.chat.storage.service;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Attachment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedComments;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedLikes;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedWallPosts;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnloadedAttachmentWallPostLink;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Uploaded;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pkletsko on 25.05.2015.
 */
public class WallPostServiceImpl implements WallPostService {

    private static final String TAG = WallPostServiceImpl.class.getSimpleName();

    private GlobalClass globalVariable;

    private UserService userService;

    protected Utils utils;

    private Intent intentLikeChanged;
    private Intent intentCommentChanged;
    private Intent intentGoogleLogin;
    private Intent intentComment;

    private Intent intentUploadProgressBar;
    private Intent intentUploadCompleted;

    private Context context;

    public WallPostServiceImpl(GlobalClass globalClass, UserService userService, Context context) {
        this.globalVariable = globalClass;
        this.userService = userService;
        this.context = context;
        this.utils = new Utils(context);

        intentLikeChanged  = new Intent(EventHub.BROADCAST_NOTIFY_LIKE_CHANGED);
        intentCommentChanged  = new Intent(EventHub.BROADCAST_NOTIFY_COMMENT_CHANGED);
        intentGoogleLogin = new Intent(EventHub.BROADCAST_LOGIN);
        intentComment = new Intent(EventHub.BROADCAST_ACTIVE_COMMENT_UPDATE);

        intentUploadProgressBar = new Intent(EventHub.BROADCAST_NOTIFY_UPLOAD_PROGRESS_BAR_CHANGED);
        intentUploadCompleted = new Intent(EventHub.BROADCAST_NOTIFY_UPLOAD_COMPLETED);
    }

    public void putToWallPostCash(final WallPost wallPost) {
        globalVariable.getWallPostCash().put(wallPost.getWallMessageId(), wallPost);
    }

    public void saveWallPostToMDB(final WallPost wallMessage, final boolean checkUser) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {

                if (checkUser) {
                    //check if this user in db
                    //if not load profile and add it and to cash
                    userService.userChecking(wallMessage.getFromUserId());
                }

                final WallPost wallPost = globalVariable.getWallMessageMDAO().createWallPost(wallMessage, globalVariable.getCurrentUser().getUserId());

                if (wallPost != null) {
                    if (wallMessage.isImageAttached()) {
                        for (String attachmentName : wallMessage.getAttachments()) {
                            Attachment attachment = new Attachment();
                            attachment.setAttachmnetName(attachmentName);
                            attachment.setWallMessageId(wallMessage.getWallMessageId());

                            globalVariable.getAttachmentMDAO().createAttachment(attachment, globalVariable.getCurrentUser().getUserId());
                        }

                        wallPost.setAttachments(wallMessage.getAttachments());
                    }

                    putToWallPostCash(wallPost);
                }
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void saveWallPostsToMDB(final Set<WallPost> wallPosts, final boolean received) {
        final boolean checkUser = false;
        saveWallPostsToMDB(wallPosts, received, checkUser);
    }

    public void saveWallPostsToMDB(final Set<WallPost> wallPosts, final boolean received, final boolean checkUser) {
        boolean firstTimeFlag = true;

        for (WallPost wallPost : wallPosts) {
            if (firstTimeFlag) {
                //check if this user in db
                //if not load profile and add it and to cash
                userService.userChecking(wallPost.getFromUserId());
                firstTimeFlag = false;
            }
            wallPost.setReceived(received);
            //save wall post and don't check post owner
            saveWallPostToMDB(wallPost, checkUser);
        }
    }

    public void saveCommentToMDB(final Comment comment) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getCommentMDAO().createComment(comment, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void saveLikeToMDB(final Like like) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getLikeMDAO().createLike(like, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void deleteLikeFromMDB(final Like like) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getLikeMDAO().deleteLike(like, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void deleteUnloadAttachmentFromMDB(final FileToUpload fileToUpload) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getUnloadedAttachmentMDAO().deleteFileToUpload(fileToUpload, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void deleteUnloadAttachmentWallPostLinkFromMDB(final Long attachmentId) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getUnloadedAttachmentWallPostLinkMDAO().deleteUnloadedAttachmentWallPostLink(attachmentId, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void deleteWallPostFromMDB(final Long wallPostId) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getWallMessageMDAO().deleteWallPost(wallPostId, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void saveUnReceivedWallPostsToMDB(final UnReceivedWallPosts unReceivedWallPosts) {
        globalVariable.getWallMessageMDAO().createWallPosts(unReceivedWallPosts.getWallMessages(), globalVariable.getCurrentUser().getUserId());
        //TODO put to cash
    }

    public void storeUnReceivedLikesToMDB(final UnReceivedLikes unReceivedLikes) {
        globalVariable.getLikeMDAO().createLikes(unReceivedLikes.getLikes(), globalVariable.getCurrentUser().getUserId());
    }

    public void removeUnReceivedLikeToMDB(final Like like) {
        globalVariable.getLikeMDAO().deleteLikeById(like.getLikeId(), globalVariable.getCurrentUser().getUserId());
    }

    public void saveUnloadedAttachmentWallPostLinkToMDB(final UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getUnloadedAttachmentWallPostLinkMDAO().createUnloadedAttachmentWallPostLink(unloadedAttachmentWallPostLink, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void initializationOfWall(final UnReceivedWallPosts unReceivedWallPosts) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;

                saveUnReceivedWallPostsToMDB(unReceivedWallPosts);

//                //preparing main wall messages
//                if (globalVariable.getCurrentUser().getFollowedWalls().size() > 0) {
//                    Set<WallPost> wallMessages = globalVariable.getWallMessageMDAO().getFollowedWallMessageUIs(globalVariable.getCurrentUser().getFollowedWalls());
//                    for (WallPost wallPost : wallMessages) {
//                        if (wallPost.isImageAttached()) {
//                            wallPost.setAttachments(globalVariable.getAttachmentMDAO().getAttachmentNamesByWallMessageId(wallPost.getWallMessageId()));
//                        }
//
//                        globalVariable.getWallPostCash().put(wallPost.getWallMessageId(), wallPost);
//                    }
//
//                    // there are all messages from database (including unread messages)
//                    globalVariable.getFollowedWallPostCash().addAll(wallMessages);
//                }
//
//                Set<WallPost> myWallMessages = globalVariable.getWallMessageMDAO().getWallMessageUIsByWallId(globalVariable.getCurrentUser().getMyWallId());
//
//                for (WallPost wallPost : myWallMessages) {
//                    if (wallPost.isImageAttached()) {
//                        wallPost.setAttachments(globalVariable.getAttachmentMDAO().getAttachmentNamesByWallMessageId(wallPost.getWallMessageId()));
//                    }
//                    globalVariable.getWallPostCash().put(wallPost.getWallMessageId(), wallPost);
//                }
//
//                globalVariable.getMyWallPostCash().addAll(myWallMessages);

                //if files were not uploaded from this messages , need relaunch upload process
                for (WallPost wallPost : unReceivedWallPosts.getWallMessages()) {
                    //check if this user in db
                    //if not load profile and add it and to cash
                    userService.userChecking(wallPost.getFromUserId());

                    // wall Post with attachment
                    if (wallPost.isImageAttached() && globalVariable.getCurrentUser().getMyWallId() == wallPost.getWallId()) {
                        Set<FileToUpload> attachments = globalVariable.getQueueToUploadFiles().get(wallPost.getAttachmentId());
                        if (attachments != null) {

                            globalVariable.getAttachmentOfWallPostQueueCash().put(wallPost.getAttachmentId(), wallPost.getWallMessageId());

                            //save it to database in case error in appliation and upload will be unsuccessful
                            UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink = new UnloadedAttachmentWallPostLink();
                            unloadedAttachmentWallPostLink.setAttachmentId(wallPost.getAttachmentId());
                            unloadedAttachmentWallPostLink.setWallPostId(wallPost.getWallMessageId());

                            saveUnloadedAttachmentWallPostLinkToMDB(unloadedAttachmentWallPostLink);

                            for (FileToUpload file : attachments) {
                                uploadFileToServer(file);
                            }
                        }
                    }
                }

                // in case loading was interrupted this function relaunch uploading again
                List<UnloadedAttachmentWallPostLink> unloadedAttachmentWallPostLinks = globalVariable.getUnloadedAttachmentWallPostLinkMDAO().getAllUnloadedAttachmentWallPostLinks(globalVariable.getCurrentUser().getUserId());

                if (unloadedAttachmentWallPostLinks != null && !unloadedAttachmentWallPostLinks.isEmpty()) {

                    for (UnloadedAttachmentWallPostLink link : unloadedAttachmentWallPostLinks) {
                        globalVariable.getAttachmentOfWallPostQueueCash().put(link.getAttachmentId(), link.getWallPostId());
                        Set<FileToUpload> fileToUploadSet = globalVariable.getUnloadedAttachmentMDAO().getFileToUploadsByAttachmentId(link.getAttachmentId(), globalVariable.getCurrentUser().getUserId());
                        globalVariable.getQueueToUploadFiles().put(link.getAttachmentId(), fileToUploadSet);
                    }

                    //TODO if some not uploaded files are still in queue , start upload process
                    if (globalVariable.getQueueToUploadFiles().size() > 0) {
                        Log.i(TAG, "initializationOfWall: some files were not uploaded successfully. Upload Procedure will be relaunch right now. ");
                        for (Long attachmentId : globalVariable.getQueueToUploadFiles().keySet()) {
                            Set<FileToUpload> fileToUploadSet = globalVariable.getQueueToUploadFiles().get(attachmentId);
                            for (FileToUpload file : fileToUploadSet) {
                                uploadFileToServer(file);
                            }
                        }
                    }
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.i(TAG, "result:" + result);
            }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }
    public void uploadFileToServer(final FileToUpload fileToUpload) {
        if (globalVariable.isAppRunning()) {
            final Long wallPostId = globalVariable.getAttachmentOfWallPostQueueCash().get(fileToUpload.getAttachmentId());
            Log.i(TAG, "uploadFileToServer: for WallPostId = " + wallPostId + ", attachmentId = " + fileToUpload.getAttachmentId() + ", fileName = " + fileToUpload.getFileName() + ", path = " + fileToUpload.getFilePath());

            globalVariable.getCurrentUploadQueue().put(fileToUpload.getAttachmentId(), fileToUpload);

            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_UPLOAD_URL);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("fromUserId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("id", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("wallPostId", wallPostId.toString());
            requestParameters.put(RequestUtil.REQUEST_ATTACHMENT_ID_KEY, fileToUpload.getAttachmentId().toString());
            requestParameters.put(RequestUtil.REQUEST_FILE_NAME_KEY, fileToUpload.getFileName());
            requestParameters.put(RequestUtil.REQUEST_FILE_PATH_KEY, fileToUpload.getFilePath());

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        Log.d(TAG, "uploadFileToServer result = " + result);
                        uploadCompleted(result);
                    }
                }
            }).request(requestParameters, globalVariable);
        }
    }

    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    private void uploadCompleted(String result) {
        Uploaded uploaded = null;
        if (result != null) {
            uploaded = utils.getUploadedResult(result);
        }

        if (uploaded == null) {
            showToast("Something went wrong. Bad response from server.");
        }

        if (uploaded != null && (uploaded.getStatus().equals("Ok") || uploaded.getStatus().equals("Completed"))) {
            final FileToUpload fileToUpload = globalVariable.getCurrentUploadQueue().get(uploaded.getAttachmentId());
            final Long wallPostId = globalVariable.getAttachmentOfWallPostQueueCash().get(fileToUpload.getAttachmentId());
            final File uploadFile = new File(fileToUpload.getFilePath());

            Log.i(TAG, "uploadFileToServer: File was upload successfully. for WallPostId = " + wallPostId + ", attachmentId = " + fileToUpload.getAttachmentId() + ", fileName = " + fileToUpload.getFileName() + ", path = " + fileToUpload.getFilePath());

            //TODO send event to change progress bar number
            intentUploadProgressBar.putExtra("wallPostId", wallPostId);
            context.sendBroadcast(intentUploadProgressBar);

            //clear cash after file was upload successfully (each file for the wall post)
            globalVariable.getQueueToUploadFiles().get(fileToUpload.getAttachmentId()).remove(fileToUpload);
            //remove uploaded file if it was ok from database
            deleteUnloadAttachmentFromMDB(fileToUpload);

            //clear cash after all files were uploaded successfully (whole wall post record)
            if (globalVariable.getQueueToUploadFiles().get(fileToUpload.getAttachmentId()).size() == 0) {
                globalVariable.getQueueToUploadFiles().remove(fileToUpload.getAttachmentId());

                //remove connection between attachment and wall post
                globalVariable.getAttachmentOfWallPostQueueCash().remove(fileToUpload.getAttachmentId());
                //remove connection data if it was ok from database
                deleteUnloadAttachmentWallPostLinkFromMDB(fileToUpload.getAttachmentId());
            }

            //remove file if it is from RussChat folder
            if (fileToUpload.getFilePath().contains("RussChat")) {
                Log.i(TAG, "uploadFileToServer: Remove file from RussChat folder. Path = " + fileToUpload.getFilePath());
                try {
                    deleteFile(uploadFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (uploaded.getStatus().equals("Completed")) {
                // send event to server to init broadcast to all followers
                globalVariable.sendMessageToServer(utils.getSendCompletedWallPostRequestJSON(wallPostId));
                //TODO send event to change wall post row
                intentUploadCompleted.putExtra("wallPostId", wallPostId);
                context.sendBroadcast(intentUploadCompleted);
            }
        }
    }

    private void deleteFile(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                deleteFile(c);
            }
        } else if (!f.delete()) {
            new FileNotFoundException("Failed to delete file: " + f);
        }
    }

    public void updateWallPostLikeNumberMDB(final Like like, final boolean likeFlag, final boolean currentUserLike) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                Long wallPostId = like.getWallMessageId();
                Long currentUserId = globalVariable.getCurrentUser().getUserId();
                WallPost wallPost = globalVariable.getWallMessageMDAO().getWallPostByWallId(wallPostId, currentUserId);

                Log.d(TAG, "updateWallPostLikeNumberMDB -> wallPost : " + wallPost);

                if (wallPost != null) {
                    int likeNumber = wallPost.getLikeNumber();
                    Log.d(TAG, "current likes number : " + likeNumber);

                    if (likeFlag) {
                        likeNumber++;
                    } else {
                        likeNumber--;
                    }

                    Log.d(TAG, "likes number after modification: " + likeNumber);

                    wallPost.setLikeNumber(likeNumber);

                    if (currentUserLike) {
                        Log.d(TAG, "liked by me!");
                        if (wallPost.isLiked()) {
                            wallPost.setLiked(false);
                        } else {
                            wallPost.setLiked(true);
                        }
                    }
                    Log.d(TAG, "liked by me flag: " + wallPost.isLiked());

                    WallPost updatedWallPost = globalVariable.getWallMessageMDAO().updateWallPost(wallPost);

                    if (updatedWallPost.isImageAttached()) {
                        updatedWallPost.setAttachments(globalVariable.getAttachmentMDAO().getAttachmentNamesByWallMessageId(wallPostId, currentUserId));
                    }

                    putToWallPostCash(updatedWallPost);

                    if (!currentUserLike) {
                        //4 - send request to update my wall activity to apply changes
                        intentLikeChanged.putExtra("wallPostId", updatedWallPost.getWallMessageId());
                        intentLikeChanged.putExtra("likesNumber", wallPost.getLikeNumber());
                        intentLikeChanged.putExtra("likeFlag", wallPost.isLiked());
                        context.sendBroadcast(intentLikeChanged);
                    }

                    return "OK";
                }
                return "Fail";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void updateWallPostCommentNumberMDB(final Comment comment, final String msg) {
        Log.i(TAG, "updateWallPostCommentNumberMDB: JSON = " + msg);
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                WallPost wallPost = globalVariable.getWallMessageMDAO().getWallPostByWallId(comment.getWallMessageId(), globalVariable.getCurrentUser().getUserId());
                if (wallPost != null) {
                    int commentNumber = wallPost.getCommentNumber() + 1;

                    wallPost.setCommentNumber(commentNumber);

                    WallPost updatedWallPost = globalVariable.getWallMessageMDAO().updateWallPost(wallPost);

                    putToWallPostCash(updatedWallPost);

                    //4 - send request to update my wall activity to apply changes
                    intentCommentChanged.putExtra("wallPostId", updatedWallPost.getWallMessageId());
                    intentCommentChanged.putExtra("commentsNumber", commentNumber);
                    context.sendBroadcast(intentCommentChanged);

                    intentComment.putExtra("message", msg);
                    context.sendBroadcast(intentComment);

                    return "OK";
                }
                return "Fail";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void initializationOfComments(final UnReceivedComments unReceivedComments) {
        Log.i(TAG, "initializationOfComments");
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;

                for (Comment comment : unReceivedComments.getComments()) {
                    //1 - save comment to mdb
                    saveCommentToMDB(comment);

                    //check if this user in db
                    //if not load profile and add it and to cash
                    userService.userChecking(comment.getFromUserId());

                    updateWallPostCommentNumberMDB(comment, null);

                    //2 - put comment to cash
                    if (globalVariable.getCommentCash().get(comment.getWallMessageId()) != null) {
                        globalVariable.getCommentCash().get(comment.getWallMessageId()).add(comment);
                    } else {
                        Set<Comment> comments = new HashSet<Comment>();
                        comments.add(comment);
                        globalVariable.getCommentCash().put(comment.getWallMessageId(), comments);
                    }
                }

                Log.i(TAG, "redirect to login/Splash activity");
                //redirect to login redirect activity
                context.sendBroadcast(intentGoogleLogin);

                return result;
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void initializationOfLikes(final UnReceivedLikes unReceivedLikes) {
        Log.i(TAG, "initializationOfLikes");
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;

                for (Like like : unReceivedLikes.getLikes()) {

                    //check if this user in db
                    //if not load profile and add it and to cash
                    userService.userChecking(like.getFromUserId());

                    if (like.getDisabled()) {

                        removeUnReceivedLikeToMDB(like);

                        boolean himself = false;
                        if (like.getFromUserId() == globalVariable.getCurrentUser().getUserId()) {
                            himself = true;
                        }

                        //2 - update wall post counter
                        updateWallPostLikeNumberMDB(like, false, himself);

                        //3 - remove like from cash
                        globalVariable.removeLikesFromCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));
                        //TODO why it is like this
//                        if (globalVariable.getLikeCash().get(like.getWallMessageId()) != null) {
//                            globalVariable.getLikeCash().get(like.getWallMessageId()).remove(like);
//                        } else {
//                            globalVariable.addLikesToCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));
//                        }
                    } else {

                        globalVariable.getLikeMDAO().createLike(like, globalVariable.getCurrentUser().getUserId());

                        boolean himself = false;
                        if (like.getFromUserId() == globalVariable.getCurrentUser().getUserId()) {
                            himself = true;
                        }

                        //2 - update wall post counter
                        updateWallPostLikeNumberMDB(like, true, himself);

                        //3 - update like cash
                        globalVariable.addLikesToCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));
                    }
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.i(TAG, "result:" + result);
            }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    //TODO this method is dangerous
//    private String getActiveActivity() {
//
//        String result = null;
//        // Using ACTIVITY_SERVICE with getSystemService(String)
//        // to retrieve a ActivityManager for interacting with the global system state.
//
//        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//
//        // Return a list of the tasks that are currently running,
//        // with the most recent being first and older ones after in order.
//        // Taken 1 inside getRunningTasks method means want to take only
//        // top activity from stack and forgot the olders.
//
//        List<ActivityManager.RunningTaskInfo> alltasks = am.getRunningTasks(1);
//
//        for (ActivityManager.RunningTaskInfo aTask : alltasks) {
//            result = aTask.topActivity.getClassName();
//            Log.d(TAG, "getActiveActivity name = " + aTask.topActivity.getClassName());
//        }
//        return result;
//    }
}

package com.pkletsko.russ.chat.websocket.message.json.model.wall;

/**
 * Created by pkletsko on 24.04.2015.
 */
public class UnloadedAttachmentWallPostLink {
    private long unloadedAttachmentWallPostLinkMDBId;
    private Long attachmentId;
    private Long wallPostId;

    public long getUnloadedAttachmentWallPostLinkMDBId() {
        return unloadedAttachmentWallPostLinkMDBId;
    }

    public void setUnloadedAttachmentWallPostLinkMDBId(long unloadedAttachmentWallPostLinkMDBId) {
        this.unloadedAttachmentWallPostLinkMDBId = unloadedAttachmentWallPostLinkMDBId;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Long getWallPostId() {
        return wallPostId;
    }

    public void setWallPostId(Long wallPostId) {
        this.wallPostId = wallPostId;
    }
}

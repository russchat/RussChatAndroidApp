package com.pkletsko.russ.chat.ui.activity.conversation.create;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.ui.activity.chat.ChatActivity;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivity;
import com.pkletsko.russ.chat.ui.activity.common.tokenautocomplete.FilteredArrayAdapter;
import com.pkletsko.russ.chat.ui.activity.common.tokenautocomplete.TokenCompleteTextView;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SearchResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CreateNewConversationMultiActivity extends BaseActivity implements TokenCompleteTextView.TokenListener {

    // LogCat tag
    private static final String TAG = CreateNewConversationMultiActivity.class.getSimpleName();
    
    private ContactsCompletionView multiAutoComplete;
    private UserProfile[] people = new UserProfile[0];
    private ArrayAdapter<UserProfile> adapter;
    private Button button;
    private List<Long> userIdList = new ArrayList<Long>();

    private boolean validateNewConversation(List<UserProfile> userProfiles) {
        boolean result = true;

        if (userProfiles.isEmpty()) {
            showToast("You should choose some people.");
            return false;
        }

        if (userProfiles.size() == 1 && userProfiles.get(0).getUserId() == globalVariable.getCurrentUser().getUserId()) {
            showToast("You cannot create conversation with yourself.");
            return false;
        }

        return result;
    }

    private void showToast(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {


            button = (Button) findViewById(R.id.btnSentCreateNewConversation);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newConversationName = "";
                    if (validateNewConversation(multiAutoComplete.getObjects())) {
                        for (final UserProfile userProfile : multiAutoComplete.getObjects()) {
                            userIdList.add(userProfile.getUserId());
                            //TODO how to create right connversation name
                            newConversationName += userProfile.getFirstName() + " ";
                        }
                        if (newConversationName.length() > 35) {
                            newConversationName = newConversationName.substring(0, 35) + "...";
                        }
                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendCreateNewConversationJSON(userIdList, newConversationName));
                    }
                }
            });

            adapter = new FilteredArrayAdapter<UserProfile>(this, R.layout.chat_person_search_layout, people) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    if (convertView == null) {

                        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                        convertView = l.inflate(R.layout.chat_person_search_layout, parent, false);
                    }

                    UserProfile p = getItem(position);
                    ((TextView) convertView.findViewById(R.id.name)).setText(p.getFullName());
                    ((TextView) convertView.findViewById(R.id.email)).setText(p.getNickName());

                    return convertView;
                }

                @Override
                protected boolean keepObject(UserProfile person, String mask) {
                    mask = mask.toLowerCase();
                    return person.getFullName().toLowerCase().startsWith(mask) || person.getNickName().toLowerCase().startsWith(mask);
                }
            };

            multiAutoComplete = (ContactsCompletionView) findViewById(R.id.searchView);
            multiAutoComplete.setAdapter(adapter);
            multiAutoComplete.setTokenListener(this);
            multiAutoComplete.setClient(globalVariable.getClient());
            multiAutoComplete.setUtils(utils);
            multiAutoComplete.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.chat_person_search_activity_main;
    }

    private void updateTokenConfirmation() {
        StringBuilder sb = new StringBuilder("New Conversation with:\n");
        for (Object token: multiAutoComplete.getObjects()) {
            sb.append(token.toString());
            sb.append("\n");
        }

        ((TextView)findViewById(R.id.tokens)).setText(sb);
    }


    @Override
    public void onTokenAdded(Object token) {
        updateTokenConfirmation();
    }

    @Override
    public void onTokenRemoved(Object token) {
        updateTokenConfirmation();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        registerReceiver(broadcastChatUserSearchResult, new IntentFilter(EventHub.BROADCAST_CHAT_USER_SEARCH_RESULT));
        registerReceiver(broadcastReceiver, new IntentFilter(EventHub.BROADCAST_CREATE_NEW_CONVERSATION));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastChatUserSearchResult);
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent i = new Intent(CreateNewConversationMultiActivity.this, ChatActivity.class);
            startActivity(i);
        }
    };

    private BroadcastReceiver broadcastChatUserSearchResult = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String broadcastedMessage = intent.getStringExtra("message");
            Log.d(TAG, "BroadcastReceiver of Chat User Result: broadcastedMessage = " + broadcastedMessage);
            parseMessage(broadcastedMessage);
        }
    };

    /**
     * Parsing the JSON message received from server The intent of message will
     * be identified by JSON node 'flag'. flag = self, message belongs to the
     * person. flag = new, a new person joined the conversation. flag = message,
     * a new message received from server. flag = exit, somebody left the
     * conversation.
     * */
    private void parseMessage(final String msg) {
        Log.d(TAG, "parseMessage of Chat User Search: msg = " + msg);
        SearchResult searchResult = utils.getSearchResult(msg);
        if (searchResult == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            appendMessage(searchResult.getUserProfiles());
        }

//        try {
//            JSONObject jObj = new JSONObject(msg);
//
//            MessageObjectType objectType = MessageObjectType.findByCode(jObj.getInt("objectType"));
//
//            if (objectType != null && objectType.equals(MOT_CHAT_USER_SEARCH)) {
//                // Appending the message to chat list
//                appendMessage(utils.getSearchResult(msg).getUserProfiles());
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    private void appendMessage(final Set<UserProfile> m) {
        Log.d(TAG, "appendMessage of User Search: WallPost = " + m.size());

        people =  m.toArray(new UserProfile[m.size()]);

        adapter = new FilteredArrayAdapter<UserProfile>(this, R.layout.chat_person_search_layout, people) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {

                    LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = l.inflate(R.layout.chat_person_search_layout, parent, false);
                }

                UserProfile p = getItem(position);
                ((TextView)convertView.findViewById(R.id.name)).setText(p.getFullName());
                ((TextView)convertView.findViewById(R.id.email)).setText(p.getNickName());

                return convertView;
            }

            @Override
            protected boolean keepObject(UserProfile person, String mask) {
                mask = mask.toLowerCase();
                return person.getFullName().toLowerCase().startsWith(mask) || person.getNickName().toLowerCase().startsWith(mask);
            }
        };

        multiAutoComplete.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

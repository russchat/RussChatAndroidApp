package com.pkletsko.russ.chat.ui.activity.conversation.fragment.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.ChatService;
import com.pkletsko.russ.chat.storage.service.ChatServiceImpl;
import com.pkletsko.russ.chat.ui.activity.common.view.EndlessRecyclerOnScrollListener;
import com.pkletsko.russ.chat.ui.activity.conversation.fragment.adapter.ConversationAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ConversationComp;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Conversations;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.NewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.CONVERSATIONS_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.DOWN;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.FIRST_ITEM;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.INIT_VALUE;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.UP;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.appendConversation;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.appendConversations;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.getConversationId;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.notifyDataSetChanged;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ConversationHelper.parseMessage;


public abstract class BaseConversationFragment extends Fragment {

    private static final String TAG = BaseConversationFragment.class.getSimpleName();

    protected Activity activity;

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected SwipeRefreshLayout swipeContainer;
    protected RecyclerView recyclerView;
    protected LinearLayoutManager mLayoutManager;

    protected ArrayList<Chat> conversations = new ArrayList<>();
    protected ConversationAdapter conversationAdapter;

    protected ChatService chatService;

    /**
     * Initialization of common wall data.
     * Next list of cases will be covered here:
     * 1 Case : when a user log in (first time/N time) and there is no wall posts stored yet in MDB, so cash is empty.
     * - in this case request with default parameter 0L will be send to server to get first (N) wall posts if exist.
     * 2 Case : when a user log in (second time/N time) and there is already wall posts stored in MDB, so cash is not empty.
     * - in this case just load wall posts fro cash to UI
     * 3 Case : in case number of cashed wall posts less than LIMIT ,
     * a new request with last loaded wall post id will be send to server to get additional wall posts.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();

        globalVariable = (GlobalClass) activity.getApplicationContext();

        utils = new Utils(activity.getApplicationContext());

        client = globalVariable.getClient();

        intentBroadcastService = new Intent(activity, BroadcastService.class);

        chatService = new ChatServiceImpl(globalVariable);

        final Set<Chat> cashedChats = getCash();

        if (cashedChats.isEmpty()) {
            // 1 Case
            loadFromServer(INIT_VALUE);
        } else {
            // 2 Case
            appendConversations(conversations, cashedChats);
            notifyDataSetChanged(getActivity(), conversationAdapter);

            // 3 Case
            if (cashedChats.size() < CONVERSATIONS_SIZE_LIMIT) {
                loadFromServer(getConversationId(conversations.size() - 1, conversations));
            }
        }
    }

    protected abstract Set<Chat> getCash();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        swipeContainer = (SwipeRefreshLayout) inflater.inflate(R.layout.conversation_main, container, false);

        recyclerView = (RecyclerView) swipeContainer.findViewById(R.id.recyclerView);

        mLayoutManager = new LinearLayoutManager(activity);

        recyclerView.setLayoutManager(mLayoutManager);

        conversationAdapter = new ConversationAdapter(activity, conversations, globalVariable, recyclerView);

        recyclerView.setAdapter(conversationAdapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
                mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadMoreDown();
            }
        });

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                loadMoreUp();
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        serverCheck();

        fragmentInitializationBody(inflater, container, savedInstanceState);

        return swipeContainer;
    }

    /**
     * Re Initialization of common wall data. Happens each time we come back from other Activity
     * Next list of cases will be covered here:
     * 5 Case : when a user scroll up and there is no wall posts loaded yet.
     * - in this case request with default parameter 0L will be send to server to get first (N) wall posts if exist.
     * 6 Case : when a user scroll up and there is already some wall posts loaded.
     * a new request with last loaded wall post id will be send to server to get additional wall posts.
     */
    protected void loadMoreUp() {
        if (conversations.isEmpty()) {
            // 5 Case
            loadFromServer(INIT_VALUE);
        } else {
            // 6 Case
            loadFromServer(getConversationId(FIRST_ITEM, conversations), UP);
        }
    }

    /**
     * Re Initialization of common wall data. Happens each time we come back from other Activity
     * Next list of cases will be covered here:
     * 4 Case : when a user scroll down.
     * - in this case request with parameter (last wall post id from UI list) will be send to server to get additional (N) wall posts if exist.
     */
    protected void loadMoreDown() {
        if (conversations.size() > 0) {
            Log.d(TAG, "LoadMore conversations size = " + conversations.size() + ", totalItemsCount = " + conversations.size() + " , wallPostId = " + conversations.get(conversations.size() - 1).getConversationId());
            // 4 Case
            loadFromServer(getConversationId(conversations.size() - 1, conversations));
        }
    }

    protected void fragmentInitializationBody(LayoutInflater inflater, ViewGroup container,
                                              Bundle savedInstanceState) {
        //nothing by default
    }

    @Override
    public void onResume() {
        super.onResume();
        //globalVariable.setActiveTab(TabEnum.TAB_CONVERSATION);
        activity.startService(intentBroadcastService);
        activity.registerReceiver(broadcastReceiver, new IntentFilter(EventHub.BROADCAST_CONVERSATION_LIST_UPDATE));
    }

    @Override
    public void onPause() {
        activity.unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    private void showToast(String error) {
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
    }

    protected BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String broadcastedMessage = intent.getStringExtra("message");

            Log.d(TAG, "BroadcastReceiver of Wall: broadcastedMessage = " + broadcastedMessage);

            if (broadcastedMessage != null) {
                NewChat newChat = parseMessage(broadcastedMessage, utils);
                if (newChat == null) {
                    showToast("Something went wrong. Bad response from server.");
                } else {
                    appendConversation(newChat.getChat(), conversations);
                    notifyDataSetChanged(activity, conversationAdapter);
                    globalVariable.notifycation("RussChat", "New Conversation");
                }
            }

        }
    };

    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    //basic functionality is empty, to add you code this method should be override
                } else {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(activity.getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            activity.stopService(intentBroadcastService);
                        }
                    });
                }
            }
        }).isServerAlive(globalVariable);
    }

    /**
     * Method to send message to web socket server
     */
    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }

    protected Map<String, String> getRequestParameters() {
        final Map<String, String> requestParameters = new HashMap<>();
        requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + GlobalClass.CONFIG_GET_USER_CONVERSATIONS_URL);
        requestParameters.put("token", globalVariable.getServerToken());
        requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
        return requestParameters;
    }

    /**
     * load received wall posts from server
     */
    protected void loadFromServer(final Long lastLoadedConversationId) {
        loadFromServer(lastLoadedConversationId, DOWN);
    }

    /**
     * load received wall posts from server with direction
     */
    protected void loadFromServer(final Long lastLoadedConversationId, final int direction) {
        if (globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = getRequestParameters();
            requestParameters.put("conversationId", String.valueOf(lastLoadedConversationId));
            requestParameters.put("direction", String.valueOf(direction));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        final Conversations newConversations = utils.getConversations(result);
                        if (newConversations == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            final Set<Chat> receivedConversations = new TreeSet<>(new ConversationComp());
                            for (Chat chat : newConversations.getConversations()) {
                                // set correct conversation name
                                if (chat.getUsers().size() == 2) {
                                    for (UserProfile userProfile : chat.getUsers()) {
                                        if (userProfile.getUserId() != globalVariable.getCurrentUser().getUserId()) {
                                            chat.setConversationName(userProfile.getFullName());
                                            chat.setConversationIcon(userProfile.getProfileIcon());
                                        }
                                    }
                                } else {
                                    chat.setConversationIcon("https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg");
                                    //TODO for group chat
                                }
                                receivedConversations.add(chat);
                            }

                            final int currentConversationSize = conversations.size();
                            final int totalSize = currentConversationSize + receivedConversations.size();

                            for (final Chat conversation : receivedConversations) {
                                // save wall post in cash and mdb
                                saveConversation(conversation);
                            }

                            //TODO if long list of users this operation can be not effective
                            for (final UserProfile userProfile : newConversations.getUserProfiles()) {
                                globalVariable.addUserProfileToCash(userProfile);
                            }

                            if (totalSize > CONVERSATIONS_SIZE_LIMIT) {
                                //remove items according to limit

                                final int removeItemsNumber = totalSize - CONVERSATIONS_SIZE_LIMIT;
                                final ArrayList<Chat> clearedConversations = (ArrayList<Chat>) conversations.clone();

                                conversations.clear();

                                if (UP == direction) {
                                    final int removeLimit = currentConversationSize - removeItemsNumber;
                                    final int removeFrom = currentConversationSize - 1;

                                    for (int i = removeFrom; i >= removeLimit; i--) {
                                        removeConversation(clearedConversations, i);
                                    }

                                    appendConversations(conversations, receivedConversations, clearedConversations);
                                } else {
                                    //DOWN
                                    for (int i = 0; i < removeItemsNumber; i++) {
                                        removeConversation(clearedConversations);
                                    }
                                    appendConversations(conversations, clearedConversations, receivedConversations);
                                }
                            } else {
                                appendConversations(conversations, receivedConversations);
                            }
                            notifyDataSetChanged(activity, conversationAdapter);
                        }
                    }
                    swipeContainer.setRefreshing(false);
                }
            }).request(requestParameters, globalVariable);
        } else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    protected void removeConversationFromCash(final Long conversationId) {
        globalVariable.getConversationCash().remove(conversationId);
    }

    private void removeConversation(ArrayList<Chat> clearedConversations) {
        removeConversation(clearedConversations, 0);
    }

    protected abstract void removeConversation(ArrayList<Chat> clearedConversations, final int index);

    protected abstract void saveConversation(final Chat conversation);
}

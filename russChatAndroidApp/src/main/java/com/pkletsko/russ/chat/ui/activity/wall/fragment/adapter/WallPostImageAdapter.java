package com.pkletsko.russ.chat.ui.activity.wall.fragment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.ui.activity.common.img.full.screen.FullScreenViewActivity;
import com.pkletsko.russ.chat.ui.activity.common.img.model.FullScreenViewDataModel;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Image;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;


public class WallPostImageAdapter extends RecyclerView.Adapter<WallPostImageAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<Image> wallPostImages;
    private GlobalClass globalVariable;
    private AssetManager assetManager;
    private Utils utils;

    // LogCat tag
    private static final String TAG = WallPostImageAdapter.class.getSimpleName();

    public WallPostImageAdapter(Activity activity, ArrayList<Image> wallPostImages, GlobalClass globalVariable) {
        Log.d(TAG, "initialization of WallMessagesListAdapter");

        this.assetManager = activity.getAssets();
        this.activity = activity;
        this.wallPostImages = wallPostImages;
        this.globalVariable = globalVariable;
        this.utils = new Utils(activity.getApplicationContext());
    }

    @Override
    public WallPostImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View vi = LayoutInflater.from(context).inflate(R.layout.wall_post_image_item, parent, false);
        //UI initialization

        ViewHolder viewHolder = new ViewHolder(vi);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        viewHolder.image = wallPostImages.get(position);

        final FullScreenViewDataModel fullScreenViewDataModel = new FullScreenViewDataModel();
        fullScreenViewDataModel.setImages(wallPostImages);
        fullScreenViewDataModel.setPosition(position);
//
//        //TODO debug code (in case we have attachment but server token is not received) - could be offline situation
//        if (viewHolder.wallPost.isImageAttached() && globalVariable.getServerToken() == null) {
//            Log.e(TAG, "In case we have attachment but server token is not received) - could be offline situation WallPostId = " + viewHolder.wallPost.getWallMessageId());
//        }
//
//        // by default it is gone
//        viewHolder.wallPostImage.setVisibility(View.GONE);
//        viewHolder.wallPostImageNumber.setVisibility(View.GONE);
//        if (viewHolder.wallPost.isImageAttached() && globalVariable.getServerToken() != null) {
//
//            viewHolder.wallPostImage.setVisibility(View.VISIBLE);
//
//            final ArrayList<Image> listImages = new ArrayList<Image>();
//
//            final Set<FileToUpload> fileToUploadSet = globalVariable.getWallPostLocalImages().get(viewHolder.wallPost.getWallMessageId());
//            if (fileToUploadSet == null) {
//                for (String fileName : viewHolder.wallPost.getAttachments()) {
//                    final Image image = new Image();
//                    final String cleanedFileName = fileName.substring(0, fileName.indexOf("."));
//                    final String imageUrl = globalVariable.CONFIG_LOAD_IMAGE_URL + "?p0=" + globalVariable.getCurrentUser().getUserId() + "&p1=" + viewHolder.wallPost.getFromUserId() + "&p2=" + viewHolder.wallPost.getWallMessageId() + "&p3=" + cleanedFileName + "&p4=" + globalVariable.getServerToken();
//                    Log.d(TAG, "imageUrl = " + imageUrl);
//                    image.setUrl(imageUrl);
//                    listImages.add(image);
//                }
//            } else {
//                for (final FileToUpload fileToUpload : fileToUploadSet) {
//                    final Image image = new Image();
//                    image.setFileToUpload(fileToUpload);
//                    image.setUrl(fileToUpload.getFilePath());
//                    listImages.add(image);
//                }
//            }
        final Transformation transformation = globalVariable.getPicassoHelper().getTransformation(viewHolder.wallPostImage);
        globalVariable.getPicassoHelper().loadImage(viewHolder.image.getUrl(), viewHolder.wallPostImage, transformation);

        viewHolder.wallPostImage.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                globalVariable.setFullScreenViewDataModel(fullScreenViewDataModel);

                Intent intentRedirect = new Intent(activity.getApplicationContext(), FullScreenViewActivity.class);
                activity.startActivity(intentRedirect);

                return true;    // <- set to true
            }
        });


//        picasso.load(viewHolder.image.getUrl())
//                .transform(transformation)
//                .into(viewHolder.wallPostImage);

//            if(listImages.size() > 2) {
//                viewHolder.wallPostImageNumber.setVisibility(View.VISIBLE);
//                int numberOfImagesLeft = listImages.size() - 1;
//                viewHolder.wallPostImageNumber.setText("+ " +  numberOfImagesLeft);
//            }
//
//            viewHolder.wallPostImage.setOnClickListener(new View.OnClickListener() {
//                public void onClick(final View v) {
//                        globalVariable.setSelectedWallPost(viewHolder.wallPost);
//                        Intent intentRedirect = new Intent(activity.getApplicationContext(), WallPostActivity.class);
//                        activity.startActivity(intentRedirect);
//                }
//            });
//        }

//        //link current wallPost with ui objects
//        viewHolder.likeButton.setTag(viewHolder.wallPost);
//        viewHolder.commentButton.setTag(viewHolder.wallPost);
//        //viewHolder.setTag(viewHolder.wallPost);
//
//        //font initialization
//        Typeface userNameFont = Typeface.createFromAsset(assetManager, "Calibri Bold.ttf");
//        Typeface postTextFont = Typeface.createFromAsset(assetManager, "Gidole-Regular.otf");
//
//        viewHolder.wallPostOwnerNameTextView.setTypeface(userNameFont);
//        viewHolder.wallPostTextView.setTypeface(postTextFont);
//
//        final int likesNumber = viewHolder.wallPost.getLikeNumber();
//        final int commentsNumber = viewHolder.wallPost.getCommentNumber();
//
//
//        final Date createdDate = viewHolder.wallPost.getCreated();
//        //2 December 12:00 PM
//
//        if (createdDate != null) {
//            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm");
//            String dateToStr = format.format(createdDate);
//            viewHolder.wallPostTime.setText(dateToStr);
//        }
//
//        //set values to template
//        viewHolder.likeNumberTextView.setText(String.valueOf(likesNumber));
//        viewHolder.likeNumberTextView.setMovementMethod(LinkMovementMethod.getInstance());
//
//        viewHolder.commentNumberTextView.setText(String.valueOf(commentsNumber));
//
//        //get latest version of user (owner of this post)
//        final UserProfile userProfile = globalVariable.getUsersCash().get(viewHolder.wallPost.getFromUserId());
//
//        Log.d(TAG, "Get User id:[" + viewHolder.wallPost.getFromUserId() + "] from Cash = " + userProfile);
//
//        viewHolder.wallPostOwnerNameTextView.setText(userProfile.getFullName() + " " + viewHolder.wallPost.getWallMessageId());
//
//        viewHolder.wallPostTextView.setText(viewHolder.wallPost.getMessageText());
//
//        //load user profile icon
//        Picasso.with(activity)
//                .load(userProfile.getProfileIcon())
//                .transform(new RoundedTransformation(100, 0))
//                .fit()
//                .into(viewHolder.wallPostOwnerProfileIcon);
//
//        //show right icon if the wall post was liked by current user
//        if (viewHolder.wallPost.isLiked()) {
//            viewHolder.likeButton.setImageResource(R.drawable.like);
//        } else {
//            viewHolder.likeButton.setImageResource(R.drawable.dislike);
//        }
//
//        viewHolder.likeNumberTextView.setOnClickListener(new View.OnClickListener() {
//            public void onClick(final View v) {
//                if (likesNumber > 0) {
//                    globalVariable.setSelectedWallPost(viewHolder.wallPost);
//
//                    Intent intentRedirect = new Intent(activity.getApplicationContext(), LikesActivity.class);
//                    activity.startActivity(intentRedirect);
//                }
//            }
//        });
//
//        //button listeners
//        //like button handler
//        viewHolder.likeButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                if (globalVariable.isAppRunning()) {
//                    WallPost currentWallPost = (WallPost) arg0.getTag();
//                    if (currentWallPost.isLiked()) {
//                        viewHolder.likeButton.setImageResource(R.drawable.dislike);
//
//                        // Sending message to web socket server
//                        sendMessageToServer(utils.getSendDislikeJSON(globalVariable.getCurrentUser().getUserId(), currentWallPost.getWallMessageId()));
//
//                        //((WallPost)arg0.getTag()).setLiked(false);
//                    } else {
//                        viewHolder.likeButton.setImageResource(R.drawable.like);
//
//                        // Sending message to web socket server
//                        sendMessageToServer(utils.getSendLikeJSON(globalVariable.getCurrentUser().getUserId(), currentWallPost.getWallMessageId()));
//
//                        //((WallPost)arg0.getTag()).setLiked(true);
//                    }
//                } else {
//                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        //comment button handler
//        viewHolder.commentNumberTextView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                if (globalVariable.isAppRunning() || (!globalVariable.isAppRunning() && commentsNumber > 0)) {
//                    globalVariable.setSelectedWallPost(viewHolder.wallPost);
//
//                    Intent i = new Intent(activity.getApplicationContext(), CommentActivity.class);
//                    activity.startActivity(i);
//                } else {
//                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//        });
//
//        //comment button handler
//        viewHolder.commentButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                if (globalVariable.isAppRunning() || (!globalVariable.isAppRunning() && commentsNumber > 0)) {
//                    WallPost currentWallPost = (WallPost) arg0.getTag();
//                    globalVariable.setSelectedWallPost(currentWallPost);
//
//                    Intent i = new Intent(activity.getApplicationContext(), CommentActivity.class);
//                    activity.startActivity(i);
//                } else {
//                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//        });
//
//        viewHolder.wallPostOwnerProfileIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (globalVariable.isAppRunning()) {
//                    globalVariable.setOpenProfile(userProfile.getUserId());
//                    Intent i = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
//                    activity.startActivity(i);
//                } else {
//                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return wallPostImages.size();
    }

//    private void sendMessageToServer(String message) {
//        if (globalVariable.getClient() != null && globalVariable.getClient().isConnected()) {
//            globalVariable.getClient().send(message);
//        }
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

//        public TextView wallPostOwnerNameTextView;
//        public TextView likeNumberTextView;
//        public TextView commentNumberTextView;
//        public TextView wallPostTextView;
//        public TextView wallPostTime;
//        public ImageView wallPostOwnerProfileIcon;
//        //public HorizontalListView listViewImages;
//        public TextView wallPostImageNumber;
        public ImageView wallPostImage;

//        public ImageButton likeButton;
//        public ImageButton commentButton;
//
//        public WallPost wallPost;
        public Image image;

        public ViewHolder(View vi) {
            super(vi);

//            wallPostOwnerNameTextView = (TextView) vi.findViewById(R.id.wallPostOwnerName);
//            likeNumberTextView = (TextView) vi.findViewById(R.id.likeNumber);
//            commentNumberTextView = (TextView) vi.findViewById(R.id.commentNumber);
//            wallPostTextView = (TextView) vi.findViewById(R.id.wallPost);
//            wallPostTime = (TextView) vi.findViewById(R.id.wallPostTime);
//            wallPostOwnerProfileIcon = (ImageView) vi.findViewById(R.id.wallPostOwnerProfileIcon);
//
//            wallPostImageNumber = (TextView) vi.findViewById(R.id.wallPostImageNumber);
            wallPostImage = (ImageView) vi.findViewById(R.id.wallPostImage);

            //listViewImages = (HorizontalListView) vi.findViewById(R.id.list_view_attached_images);
//            likeButton = (ImageButton) vi.findViewById(R.id.likeButton);
//            commentButton = (ImageButton) vi.findViewById(R.id.commentButton);
        }
    }
}
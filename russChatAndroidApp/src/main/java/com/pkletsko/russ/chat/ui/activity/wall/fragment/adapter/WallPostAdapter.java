package com.pkletsko.russ.chat.ui.activity.wall.fragment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.ui.activity.profile.activity.UserProfileActivity;
import com.pkletsko.russ.chat.ui.activity.wall.comment.CommentActivity;
import com.pkletsko.russ.chat.ui.activity.wall.like.LikesActivity;
import com.pkletsko.russ.chat.ui.activity.wall.post.WallPostActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class WallPostAdapter extends RecyclerView.Adapter<WallPostAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<WallPost> wallMessages;
    private GlobalClass globalVariable;
    private AssetManager assetManager;
    private Utils utils;
    private List<ViewHolder> viewHolderList = new ArrayList<>();
    private Intent intentCommentActivity;
    private Intent intentLikesActivity;
    private Intent intentUserProfileActivity;
    private Intent intentWallPostActivity;
    private Typeface userNameFont;
    private Typeface postTextFont;
    private SimpleDateFormat format;

    // LogCat tag
    private static final String TAG = WallPostAdapter.class.getSimpleName();

    public WallPostAdapter(Activity activity, ArrayList<WallPost> wallMessages, GlobalClass globalVariable) {
        Log.d(TAG, "initialization of WallMessagesListAdapter");

        this.assetManager = activity.getAssets();
        this.activity = activity;
        this.wallMessages = wallMessages;
        this.globalVariable = globalVariable;
        this.utils = new Utils(activity.getApplicationContext());
        this.intentCommentActivity = new Intent(activity.getApplicationContext(), CommentActivity.class);
        this.intentLikesActivity = new Intent(activity.getApplicationContext(), LikesActivity.class);
        this.intentUserProfileActivity = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
        this.intentWallPostActivity = new Intent(activity.getApplicationContext(), WallPostActivity.class);
        this.userNameFont = Typeface.createFromAsset(assetManager, "Calibri Bold.ttf");
        this.postTextFont = Typeface.createFromAsset(assetManager, "Gidole-Regular.otf");
        this.format = new SimpleDateFormat("dd MMM yyyy hh:mm");
    }

    @Override
    public WallPostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View vi = LayoutInflater.from(context).inflate(R.layout.wall_item, parent, false);
        //UI initialization

        ViewHolder viewHolder = new ViewHolder(vi);
        viewHolderList.add(viewHolder);
        return viewHolder;
    }

    private WallPost getWallPost(int position) {
        //get item from list of items (could be old version of wall post), so need to get version from cash
        final WallPost wallPostTmp = wallMessages.get(position);

        //Log.d(TAG, "getView of WallMessagesListAdapter : WallPost id = " + wallPostTmp.getWallMessageId() + ", msg " + wallPostTmp.getMessageText());

        // get latest version of wall post from cash (could be with new likes or comments)
        return globalVariable.getWallPostCash().get(wallPostTmp.getWallMessageId());
    }

    private WallPost getWallPostById(Long wallPostId) {
        return globalVariable.getWallPostCash().get(wallPostId);
    }

    public ViewHolder getViewHolder(int position) {
        if (viewHolderList.size() > position) {
            return viewHolderList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        //Log.d(TAG, "begin!!!!!!!!!!!!!!");
        //if (viewHolder.wallPost == null) {
//            AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
//                @Override
//                protected String doInBackground(Void... params) {
//                    String accessToken = null;
//                    activity.runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
        viewHolder.wallPost = getWallPost(position);

//                            //TODO debug code (in case we have attachment but server token is not received) - could be offline situation
//                            if (viewHolder.wallPost.isImageAttached() && globalVariable.getServerToken() == null) {
//                                Log.e(TAG, "In case we have attachment but server token is not received) - could be offline situation WallPostId = " + viewHolder.wallPost.getWallMessageId());
//                            }

        // by default it is gone
        viewHolder.wallPostImage.setVisibility(View.GONE);
        viewHolder.wallPostImageNumber.setVisibility(View.GONE);
        viewHolder.progressBar.setVisibility(View.GONE);
        viewHolder.progressStatusTextView.setVisibility(View.GONE);
        //Log.d(TAG, "before image" + viewHolder.wallPost.getWallMessageId());
        if (viewHolder.wallPost.isImageAttached()) {
            viewHolder.wallPostImage.setVisibility(View.VISIBLE);

            final Set<FileToUpload> fileToUploadSet = globalVariable.getWallPostLocalImages().get(viewHolder.wallPost.getWallMessageId());
            int attachmentsSize;
            final Transformation transformation = globalVariable.getPicassoHelper().getTransformation(viewHolder.wallPostImage);


            if (fileToUploadSet == null) {
                attachmentsSize = viewHolder.wallPost.getAttachments().size();

                if (attachmentsSize > 0) {
                    String fileName = viewHolder.wallPost.getAttachments().iterator().next();
                    final String cleanedFileName = fileName.substring(0, fileName.indexOf("."));
                    final String imageUrl = globalVariable.getFullServicePath() + globalVariable.CONFIG_LOAD_IMAGE_URL + "?p0=" + globalVariable.getCurrentUser().getUserId() + "&p1=" + viewHolder.wallPost.getFromUserId() + "&p2=" + viewHolder.wallPost.getWallMessageId() + "&p3=" + cleanedFileName;

                    globalVariable.getPicassoHelper().loadImage(imageUrl, viewHolder.wallPostImage, transformation);
                }
            } else {
                attachmentsSize = fileToUploadSet.size();
                if (attachmentsSize > 0) {
                    Uri uri = Uri.fromFile(new File(fileToUploadSet.iterator().next().getFilePath()));
                    Picasso.with(activity)
                            .load(uri)
                            .transform(transformation).resize(600, 400) // resizes the image to these dimensions (in pixel)
                            .centerCrop()
                            .into(viewHolder.wallPostImage);
                    viewHolder.progressBar.setVisibility(View.VISIBLE);
                    viewHolder.progressStatusTextView.setVisibility(View.VISIBLE);
                    viewHolder.progressBar.setMax(attachmentsSize);
                    viewHolder.progressBar.setProgress(0);
                    //viewHolder.progressStatusTextView.setText("0" + "/" + attachmentsSize);

                    new Thread(new Runnable() {
                        public void run() {
                            while (viewHolder.progressStatus < viewHolder.progressBar.getMax()) {

                                // Update the progress bar
                                viewHolder.handler.post(new Runnable() {
                                    public void run() {
                                        viewHolder.progressBar.setProgress(viewHolder.progressStatus);
                                        viewHolder.progressStatusTextView.setText(viewHolder.progressStatus + "/" + viewHolder.progressBar.getMax());
                                    }
                                });
                            }
                        }
                    }).start();


                }
            }

            if (attachmentsSize > 1) {
                viewHolder.wallPostImageNumber.setVisibility(View.VISIBLE);
                int numberOfImagesLeft = attachmentsSize - 1;
                viewHolder.wallPostImageNumber.setText("+ " + numberOfImagesLeft);
            }

            viewHolder.wallPostImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(final View v) {
                    globalVariable.setSelectedWallPost(viewHolder.wallPost);
                    activity.startActivity(intentWallPostActivity);
                }
            });
        }
        // Log.d(TAG, "after image" + viewHolder.wallPost.getWallMessageId());
        //link current wallPost with ui objects
        viewHolder.likeButton.setTag(viewHolder.wallPost);
        viewHolder.commentButton.setTag(viewHolder.wallPost);
        //viewHolder.setTag(viewHolder.wallPost);
        viewHolder.likeNumberTextView.setTag(viewHolder.wallPost);
        viewHolder.commentNumberTextView.setTag(viewHolder.wallPost);

        //font initialization


        viewHolder.wallPostOwnerNameTextView.setTypeface(userNameFont);
        viewHolder.wallPostTextView.setTypeface(postTextFont);

        final int likesNumber = viewHolder.wallPost.getLikeNumber();
        final int commentsNumber = viewHolder.wallPost.getCommentNumber();


        final Date createdDate = viewHolder.wallPost.getCreated();
        //2 December 12:00 PM

        if (createdDate != null) {
            String dateToStr = format.format(createdDate);
            viewHolder.wallPostTime.setText(dateToStr);
        }

        //set values to template
        viewHolder.likeNumberTextView.setText(String.valueOf(likesNumber));
        viewHolder.likeNumberTextView.setMovementMethod(LinkMovementMethod.getInstance());

        viewHolder.commentNumberTextView.setText(String.valueOf(commentsNumber));

        //get latest version of user (owner of this post)
        final UserProfile userProfile = globalVariable.getUsersCash().get(viewHolder.wallPost.getFromUserId());

        // Log.d(TAG, "Get User id:[" + viewHolder.wallPost.getFromUserId() + "] from Cash = " + userProfile);

        //viewHolder.wallPostOwnerNameTextView.setText(userProfile.getFullName() + " " + viewHolder.wallPost.getWallMessageId());
        viewHolder.wallPostOwnerNameTextView.setText(userProfile.getFullName());

        viewHolder.wallPostTextView.setText(viewHolder.wallPost.getMessageText());

        //Log.d(TAG, "before icon" + viewHolder.wallPost.getWallMessageId());
        final Transformation transformation = globalVariable.getPicassoHelper().getTransformation(viewHolder.wallPostImage);
        globalVariable.getPicassoHelper().loadImageWithFit(userProfile.getProfileIcon(), viewHolder.wallPostOwnerProfileIcon, globalVariable.getPicassoHelper().getRoundedTransformation());
        //Log.d(TAG, "after icon" + viewHolder.wallPost.getWallMessageId());
        //show right icon if the wall post was liked by current user
        if (viewHolder.wallPost.isLiked()) {
            viewHolder.likeButton.setImageResource(R.drawable.like);
        } else {
            viewHolder.likeButton.setImageResource(R.drawable.dislike);
        }

        viewHolder.likeNumberTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                WallPost currentWallPost = getLatestCopyOfWallPost(v);
                if (currentWallPost != null) {
                    viewHolder.likeNumberTextView.setTag(currentWallPost);
                    viewHolder.wallPost = currentWallPost;
                    if (currentWallPost.getLikeNumber() > 0) {
                        globalVariable.setSelectedWallPost(viewHolder.wallPost);
                        activity.startActivity(intentLikesActivity);
                    }
                }
            }
        });

        //button listeners
        //like button handler
        viewHolder.likeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (globalVariable.isAppRunning()) {
                    WallPost currentWallPost = ((WallPost) arg0.getTag());
                    if (currentWallPost.isLiked()) {
                        viewHolder.likeButton.setImageResource(R.drawable.dislike);
                        int likes = ((WallPost) arg0.getTag()).getLikeNumber();

                        ((WallPost) arg0.getTag()).setLikeNumber(likes - 1);

                        viewHolder.likeNumberTextView.setText(String.valueOf(likes - 1));
                        ((WallPost) arg0.getTag()).setLiked(false);

                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendDislikeJSON(globalVariable.getCurrentUser().getUserId(), currentWallPost.getWallMessageId()));
                    } else {
                        viewHolder.likeButton.setImageResource(R.drawable.like);

                        int likes = ((WallPost) arg0.getTag()).getLikeNumber();

                        ((WallPost) arg0.getTag()).setLikeNumber(likes + 1);

                        viewHolder.likeNumberTextView.setText(String.valueOf(likes + 1));
                        ((WallPost) arg0.getTag()).setLiked(true);

                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendLikeJSON(globalVariable.getCurrentUser().getUserId(), currentWallPost.getWallMessageId()));

                    }
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //comment button handler
        viewHolder.commentNumberTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                WallPost currentWallPost = getLatestCopyOfWallPost(arg0);
                if (currentWallPost != null) {
                    viewHolder.commentNumberTextView.setTag(currentWallPost);
                    viewHolder.wallPost = currentWallPost;
                    if (globalVariable.isAppRunning() || (!globalVariable.isAppRunning() && currentWallPost.getCommentNumber() > 0)) {
                        globalVariable.setSelectedWallPost(viewHolder.wallPost);
                        activity.startActivity(intentCommentActivity);
                    } else {
                        Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //comment button handler
        viewHolder.commentButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (globalVariable.isAppRunning() || (!globalVariable.isAppRunning() && commentsNumber > 0)) {
                    WallPost currentWallPost = (WallPost) arg0.getTag();
                    globalVariable.setSelectedWallPost(currentWallPost);
                    activity.startActivity(intentCommentActivity);
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }

        });

        viewHolder.wallPostOwnerProfileIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalVariable.isAppRunning()) {
                    globalVariable.setOpenProfile(userProfile.getUserId());
                    activity.startActivity(intentUserProfileActivity);
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //          }
        //        });

//                    return accessToken;
//                }
//
//                @Override
//                protected void onPostExecute(String accessToken) {
//                }
//            };
//
//            task.execute();
        //}
        // Log.d(TAG, "end!!!!!!!!!!!!!!");
    }

    private WallPost getLatestCopyOfWallPost(final View v) {
        WallPost currentWallPost = (WallPost) v.getTag();
        if (currentWallPost != null) {
            return getWallPostById(currentWallPost.getWallMessageId());
        }
        return null;
    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return wallMessages.size();
    }

    private void sendMessageToServer(String message) {
        if (globalVariable.getClient() != null && globalVariable.getClient().isConnected()) {
            globalVariable.getClient().send(message);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView wallPostOwnerNameTextView;
        public TextView likeNumberTextView;
        public TextView commentNumberTextView;
        public TextView wallPostTextView;
        public TextView wallPostTime;
        public ImageView wallPostOwnerProfileIcon;
        //public HorizontalListView listViewImages;
        public TextView wallPostImageNumber;
        public ImageView wallPostImage;

        public ImageButton likeButton;
        public ImageButton commentButton;

        public ProgressBar progressBar;
        public int progressStatus = 0;
        public TextView progressStatusTextView;
        public Handler handler = new Handler();

        public WallPost wallPost;

        public ViewHolder(View vi) {
            super(vi);

            wallPostOwnerNameTextView = (TextView) vi.findViewById(R.id.wallPostOwnerName);
            likeNumberTextView = (TextView) vi.findViewById(R.id.likeNumber);
            commentNumberTextView = (TextView) vi.findViewById(R.id.commentNumber);
            wallPostTextView = (TextView) vi.findViewById(R.id.wallPost);
            wallPostTime = (TextView) vi.findViewById(R.id.wallPostTime);
            wallPostOwnerProfileIcon = (ImageView) vi.findViewById(R.id.wallPostOwnerProfileIcon);

            wallPostImageNumber = (TextView) vi.findViewById(R.id.wallPostImageNumber);
            wallPostImage = (ImageView) vi.findViewById(R.id.wallPostImage);

            //listViewImages = (HorizontalListView) vi.findViewById(R.id.list_view_attached_images);
            likeButton = (ImageButton) vi.findViewById(R.id.likeButton);
            commentButton = (ImageButton) vi.findViewById(R.id.commentButton);

            progressBar = (ProgressBar) vi.findViewById(R.id.progressBar1);
            progressStatusTextView = (TextView) vi.findViewById(R.id.progressStatusTextView);
        }
    }
}
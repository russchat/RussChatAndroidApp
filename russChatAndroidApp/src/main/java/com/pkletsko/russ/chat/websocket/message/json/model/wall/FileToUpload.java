package com.pkletsko.russ.chat.websocket.message.json.model.wall;

/**
 * Created by pkletsko on 24.04.2015.
 */
public class FileToUpload {
    private long fileToUploadMDBId;
    private String fileName;
    private String filePath;
    private Long attachmentId;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public long getFileToUploadMDBId() {
        return fileToUploadMDBId;
    }

    public void setFileToUploadMDBId(long fileToUploadMDBId) {
        this.fileToUploadMDBId = fileToUploadMDBId;
    }
}

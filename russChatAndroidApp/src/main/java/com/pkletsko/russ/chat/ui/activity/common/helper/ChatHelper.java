package com.pkletsko.russ.chat.ui.activity.common.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by pkletsko on 02.11.2015.
 */
public class ChatHelper {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int CHAT_MESSAGE_SIZE_LIMIT = 36;
    public static final Long INIT_VALUE = 0L;
    public static final int FIRST_ITEM = 0;

    public static void notifyDataSetChanged(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public static Long getChatMessageId(final int index, ArrayList<ChatMessage> existedChatMessages) {
        return existedChatMessages.get(index).getMessageId();
    }

     /**
     * Appending message to list view
     */
    public static void appendChatMessages(final ArrayList<ChatMessage> existedChatMessages, Collection... chatMessageCollections) {
        for (Collection chatMessages : chatMessageCollections) {
            existedChatMessages.addAll(chatMessages);
        }
    }

    public static void appendChatMessage(final ChatMessage chatMessage, final ArrayList<ChatMessage> existedChatMessage) {
        existedChatMessage.add(chatMessage);
    }

    public static ChatMessage parseMessage(final String msg, final Utils utils) {
       return utils.getChatMessage(msg);
//        try {
//            JSONObject jObj = new JSONObject(msg);
//
//            MessageObjectType objectType = MessageObjectType.findByCode(jObj.getInt("objectType"));
//
//            if (objectType != null) {
//                if (objectType.equals(MOT_NEW_MESSAGE)) {
//                    // Appending the message to chat list
//                    return utils.getChatMessage(msg);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return null;
    }
}

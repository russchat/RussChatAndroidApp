package com.pkletsko.russ.chat.ui.activity.common.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by pkletsko on 02.11.2015.
 */
public class CommentHelper {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int COMMENT_LIST_SIZE_LIMIT = 36;
    public static final Long INIT_VALUE = 0L;
    public static final int FIRST_ITEM = 0;

    public static void notifyDataSetChanged(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public static Long getCommentId(final int index, ArrayList<Comment> existedComments) {
        return existedComments.get(index).getCommentId();
    }

     /**
     * Appending message to list view
     */
    public static void appendComments(final ArrayList<Comment> existedComments, Collection... likeCollections) {
        for (Collection likes : likeCollections) {
            existedComments.addAll(likes);
        }
    }

    public static void appendComment(final Comment comment, final ArrayList<Comment> existedComment) {
        existedComment.add(0, comment);
    }

    public static Comment parseMessage(final String msg, final Utils utils) {
        return utils.getComment(msg);
//
//        try {
//            JSONObject jObj = new JSONObject(msg);
//            MessageObjectType objectType = MessageObjectType.findByCode(jObj.getInt("objectType"));
//
//            if (objectType != null && objectType.equals(MOT_COMMENT)) {
//                // Appending the message to chat list
//                return utils.getComment(msg);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return null;
    }
}

package com.pkletsko.russ.chat.ui.activity.wall.like;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivityTest;
import com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper;
import com.pkletsko.russ.chat.ui.activity.common.view.EndlessRecyclerOnScrollListener;
import com.pkletsko.russ.chat.ui.activity.wall.like.adapter.LikeAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Likes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.LikeHelper.INIT_VALUE;
import static com.pkletsko.russ.chat.ui.activity.common.helper.LikeHelper.LIKE_LIST_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.LikeHelper.appendLikes;
import static com.pkletsko.russ.chat.ui.activity.common.helper.LikeHelper.getLikeId;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.DOWN;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.FIRST_ITEM;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.UP;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.notifyDataSetChanged;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.notifyWallChanged;

public class LikesActivity extends BaseActivityTest {

    private static final String TAG = LikesActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private LikeAdapter likeAdapter;

    private ArrayList<Like> likes = new ArrayList<>();

    @Override
    protected int getLayoutResourceId() {
        return R.layout.likes_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Likes");

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {
            activity = this;

            swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

            mRecyclerView = (RecyclerView) findViewById(R.id.likesRecyclerView);

            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

            mRecyclerView.setLayoutManager(mLayoutManager);

            likeAdapter = new LikeAdapter(this, likes, globalVariable, mRecyclerView);

            mRecyclerView.setAdapter(likeAdapter);

            mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
                    mLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    loadMoreDown();
                }
            });

            // Setup refresh listener which triggers new data loading
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    // Make sure you call swipeContainer.setRefreshing(false)
                    // once the network request has completed successfully.
                    loadMoreUp();
                }
            });

            // Configure the refreshing colors
            swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

            initializationOfLikeCash();
        }
    }

    protected void loadMoreUp() {
        if (likes.isEmpty()) {
            // 5 Case
            loadFromServer(WallHelper.INIT_VALUE);
        } else {
            // 6 Case
            loadFromServer(getLikeId(FIRST_ITEM, likes), UP);
        }
    }

    protected void loadMoreDown() {
        if (likes.size() > 0) {
            Log.d(TAG, "LoadMore userProfiles size = " + likes.size() + ", totalItemsCount = " + likes.size() + " , wallPostId = " + likes.get(likes.size() - 1).getLikeId());
            // 4 Case
            loadFromServer(likes.get(likes.size() - 1).getLikeId());
        }
    }

    /**
     * load received wall posts from server
     * */
    protected void loadFromServer(final Long lastLoadedLikeId) {
        loadFromServer(lastLoadedLikeId, DOWN);
    }

    /**
     * load received wall posts from server with direction
     * */
    protected void loadFromServer(final Long lastLoadedLikeId, final int direction) {
        if (globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_USERS_LIKED_URL);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("id", globalVariable.getSelectedWallPost().getWallMessageId().toString());
            requestParameters.put("likeId", String.valueOf(lastLoadedLikeId));
            requestParameters.put("direction", String.valueOf(direction));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        Log.d(TAG, "loadFromServer result = " + result);

                        final Likes likesObj = utils.getLikes(result);
                        if (likesObj == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            final Set<Like> receivedLikes = likesObj.getLikes();

                            final int currentLikesSize = likes.size();
                            final int totalSize = currentLikesSize + receivedLikes.size();

                            //we need user to show name and icon (we need them temporary)
                            for (UserProfile userProfile : likesObj.getUserProfiles()) {
                                //TODO check if this is not overwrite more extended userProfile object existed in cash
                                globalVariable.getUsersCash().put(userProfile.getUserId(), userProfile);
                            }

                            for (final Like like : receivedLikes) {
                                // save like in cash and mdb
                                saveLike(like);
                            }

                            globalVariable.addLikesToCash(globalVariable.getSelectedWallPost().getWallMessageId(), receivedLikes);

                            if (totalSize > LIKE_LIST_SIZE_LIMIT) {
                                //remove items according to limit

                                final int removeItemsNumber = totalSize - LIKE_LIST_SIZE_LIMIT;
                                final ArrayList<Like> clearedLikes = (ArrayList<Like>) likes.clone();

                                likes.clear();

                                if (UP == direction) {
                                    final int removeLimit = currentLikesSize - removeItemsNumber;
                                    final int removeFrom = currentLikesSize - 1;

                                    for (int i = removeFrom; i >= removeLimit; i--) {
                                        removeLike(clearedLikes, i);
                                    }

                                    appendLikes(likes, receivedLikes, clearedLikes);
                                } else {
                                    //DOWN
                                    for (int i = 0; i < removeItemsNumber; i++) {
                                        removeLike(clearedLikes);
                                    }
                                    appendLikes(likes, clearedLikes, receivedLikes);
                                }
                            } else {
                                appendLikes(likes, receivedLikes);
                            }
                            notifyDataSetChanged(activity, likeAdapter);
                        }
                    }
                    swipeContainer.setRefreshing(false);
                }
            }).request(requestParameters, globalVariable);
        } else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    private void showToast(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }

    private void removeLike(ArrayList<Like> clearedLikes){
        removeLike(clearedLikes, 0);
    }

    public void removeLike(ArrayList<Like> clearedLikes, final int index){
        final Long userId = clearedLikes.get(index).getLikeId();
        //wallPostService.deleteWallPostFromMDB(userId);
        //removeWallPostFromCash(wallPostId);
        //globalVariable.getFollowedWallPostCash().remove(clearedWallPosts.get(index));
        //clearedWallPosts.remove(index);
    }

    public void saveLike(final Like like) {
        //wallPost.setReceived(true);
        // putToCash(wallPost, globalVariable.getWallPostCash(), globalVariable.getFollowedWallPostCash());
        //wallPostService.saveWallPostToMDB(wallPost, true);
    }

    private void initializationOfLikeCash() {
        AsyncTask<Void, Void, Set<Like>> task = new AsyncTask<Void, Void, Set<Like>>() {
            @Override
            protected Set<Like> doInBackground(Void... params) {
                return globalVariable.getLikeMDAO().getLikesByWallMessageId(globalVariable.getSelectedWallPost().getWallMessageId(), globalVariable.getCurrentUser().getUserId());
            }
            @Override
            protected void onPostExecute(Set<Like> receivedLikes) {
                Log.i(TAG, "initializationOfLikeCash onPostExecute:" + receivedLikes);

                if (receivedLikes == null || receivedLikes.isEmpty()) {
                    loadFromServer(INIT_VALUE);
                } else {
                    globalVariable.addLikesToCash(globalVariable.getSelectedWallPost().getWallMessageId(), receivedLikes);

                    appendLikes(likes, receivedLikes);
                    notifyWallChanged(activity, likeAdapter);

                    if (likes.size() < LIKE_LIST_SIZE_LIMIT) {
                        loadFromServer(likes.get(likes.size() - 1).getLikeId());
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(intentBroadcastService);
    }
}

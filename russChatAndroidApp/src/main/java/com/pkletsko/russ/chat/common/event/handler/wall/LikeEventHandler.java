package com.pkletsko.russ.chat.common.event.handler.wall;

import android.content.Context;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.storage.service.WallPostService;
import com.pkletsko.russ.chat.storage.service.WallPostServiceImpl;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedLikes;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by pkletsko on 26.05.2015.
 */
public class LikeEventHandler {

    private static final String TAG = LikeEventHandler.class.getSimpleName();

    private UserService userService;

    private WallPostService wallPostService;

    private GlobalClass globalVariable;

    private Utils utils;

    private Context context;

    public LikeEventHandler(final GlobalClass globalVariable, final Utils utils, final Context context) {
        this.globalVariable = globalVariable;
        this.utils = utils;
        this.context = context;

        userService = new UserServiceImpl(globalVariable, utils);

        wallPostService = new WallPostServiceImpl(globalVariable, userService ,context);
    }

    public void unreceivedWallPostsEventHandler(final String msg) {
        if (globalVariable.getCurrentUser() != null) {
            // async get data from database and after set data to global variable and than call redirect to login activity
            UnReceivedLikes unReceivedLikes = utils.getUnReceivedLikes(msg);
            if (unReceivedLikes == null) {
                showToast("Something went wrong. Bad response from server.");
            } else {
                wallPostService.initializationOfLikes(unReceivedLikes);
            }
        }
    }

    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    public void likeEventHandler(final String msg) {
        Like like = utils.getLike(msg);
        if (like == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {

            //1 - save like to mdb
            wallPostService.saveLikeToMDB(like);

            boolean himself = false;
            if (like.getFromUserId() == globalVariable.getCurrentUser().getUserId()) {
                himself = true;
            }

            //2 - update wall post counter
            wallPostService.updateWallPostLikeNumberMDB(like, true, himself);

            //3 - update like cash
            globalVariable.addLikesToCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));

            //check if this user in db
            //if not load profile and add it and to cash
            userService.userChecking(like.getFromUserId());
        }
    }

    public void dislikeEventHandler(final String msg) {
        Like like = utils.getLike(msg);
        if (like == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            //1 - delete like from mdb
            wallPostService.deleteLikeFromMDB(like);

            boolean himself = false;
            if (like.getFromUserId() == globalVariable.getCurrentUser().getUserId()) {
                himself = true;
            }

            //2 - update wall post counter
            wallPostService.updateWallPostLikeNumberMDB(like, false, himself);

            //3 - remove like from cash
            globalVariable.removeLikesFromCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));
            //TODO why it is like this
//        if (globalVariable.getLikeCash().get(like.getWallMessageId()) != null) {
//            globalVariable.removeLikesFromCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));
//        } else {
//            globalVariable.addLikesToCash(like.getWallMessageId(), new HashSet<>(Arrays.asList(like)));
//        }
        }
    }

}

package com.pkletsko.russ.chat.websocket.message.json.model.user;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */
public class UserLogin extends BasicTransportJSONObject implements JSONResponse {

    private UserProfile userProfile;

    private String loginStatus;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }
}

package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class WallPost extends BasicTransportJSONObject implements JSONResponse {

    private Long wallMessageMDBId;

    private Long wallMessageId;

    private Long fromUserId;

    private String messageText;

    private Long wallId;

    private int likeNumber = 0;

    private int commentNumber = 0;

    private boolean liked = false;

    private boolean imageAttached = false;

    private Long attachmentId;

    private int imageNumber = 0;

    private boolean received = false;

    private Set<String> attachments = new HashSet<String>();

    private Date created;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Set<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<String> attachments) {
        this.attachments = attachments;
    }

    public boolean isImageAttached() {
        return imageAttached;
    }

    public void setImageAttached(boolean imageAttached) {
        this.imageAttached = imageAttached;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public int getImageNumber() {
        return imageNumber;
    }

    public void setImageNumber(int imageNumber) {
        this.imageNumber = imageNumber;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getWallMessageMDBId() {
        return wallMessageMDBId;
    }

    public void setWallMessageMDBId(Long wallMessageMDBId) {
        this.wallMessageMDBId = wallMessageMDBId;
    }

    public Long getWallId() {
        return wallId;
    }

    public void setWallId(Long wallId) {
        this.wallId = wallId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

    public int getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber = likeNumber;
    }

    public int getCommentNumber() {
        return commentNumber;
    }

    public void setCommentNumber(int commentNumber) {
        this.commentNumber = commentNumber;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }
//    @Override
//    public int compareTo(Object o) {
//        WallPost msg = (WallPost) o;
//        Long result = this.wallMessageId - msg.getWallMessageId();
//        return result.intValue();
//    }
//
//    public int compare(Object firstObject, Object secondObject) {
//        WallPost first = (WallPost) firstObject;
//        WallPost second = (WallPost) secondObject;
//        return second.compareTo(first);
//    }


}

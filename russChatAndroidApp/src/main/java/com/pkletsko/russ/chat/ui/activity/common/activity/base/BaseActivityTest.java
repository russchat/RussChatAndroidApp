package com.pkletsko.russ.chat.ui.activity.common.activity.base;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.ui.activity.login.SplashActivity;

public abstract class BaseActivityTest extends SlidingFragmentActivity {

    private static final String TAG = BaseActivityTest.class.getSimpleName();

    protected GlobalClass globalVariable;

    protected Utils utils;

    protected Activity activity;

    protected SwipeRefreshLayout swipeContainer;

    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected ActionBar actionBar;

    protected UserService userService;

    protected Long currentUserId;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        beforeSetContentView();
        setContentView(getLayoutResourceId());

        actionBar = getActionBar();

        globalVariable = (GlobalClass) getApplicationContext();

        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {
            utils = new Utils(getApplicationContext());

            client = globalVariable.getClient();

            userService = new UserServiceImpl(globalVariable, utils);

            intentBroadcastService = new Intent(this, BroadcastService.class);

        /*Set base vars*/
            currentUserId = globalVariable.getCurrentUser().getUserId();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.statusbarred));
            }

            actionBar.setDisplayHomeAsUpEnabled(true); // Enabling Back navigation on Action Bar icon

            startService(intentBroadcastService);

            initSlidingMenu();

            serverCheck();
        }
    }

    private void initSlidingMenu () {
        // set the Behind View
        setBehindContentView(R.layout.profile_menu_frame);

        // customize the SlidingMenu
        SlidingMenu sm = getSlidingMenu();
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setFadeDegree(0.35f);
        sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
    }

    public void restartAppAfterSleepMode() {
        //this is special method to prevent app from crash after it was in a sleep mode
        //whole app should be re-init
        if (globalVariable.getCurrentUser() == null) {
            Intent intentRedirect = new Intent(this, SplashActivity.class);
            intentRedirect.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentRedirect);
            finish();
        }
    }

    protected abstract int getLayoutResourceId();

    protected void beforeSetContentView(){
        setTheme(R.style.ChatAppRedTheme); // Set here
    }

    protected void serverCheck() {
        Log.d(TAG, "serverCheck");
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    Log.d(TAG, "onPostServerAlive: OK");
                    //basic functionality is empty, to add you code this method should be override
                    //startService(intentBroadcastService);
                } else {
                    Log.d(TAG, "onPostServerAlive: No internet connection or server is down.");
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            stopService(intentBroadcastService);
                        }
                    });
                }
            }
        }).isServerAlive(globalVariable);
    }

    /**
     * Method to send message to web socket server
     */
    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }
}

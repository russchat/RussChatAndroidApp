package com.pkletsko.russ.chat.ui.activity.profile.fragment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.ui.activity.profile.activity.UserProfileActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.ArrayList;
import java.util.List;


public class UserListRecyclerAdapter extends RecyclerView.Adapter<UserListRecyclerAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<UserProfile> userProfiles;
    //private LayoutInflater inflater = null;
    private GlobalClass globalVariable;
    private AssetManager assetManager;
    private WebSocketClient client;
    private Utils utils;
    private RecyclerView recyclerView;

    private static final int PROFILE_PIC_SIZE = 400;

    private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    // LogCat tag
    private static final String TAG = UserListRecyclerAdapter.class.getSimpleName();

    public UserListRecyclerAdapter(Activity activity, ArrayList<UserProfile> userProfiles, GlobalClass globalVariable, RecyclerView recyclerView) {
        Log.d(TAG, "initialization of WallMessagesListAdapter");
        this.recyclerView = recyclerView;
        this.assetManager = activity.getAssets();
        this.activity = activity;
        this.userProfiles = userProfiles;
        this.globalVariable = globalVariable;
        this.client = globalVariable.getClient();
        this.utils = new Utils(activity.getApplicationContext());
    }

    @Override
    public UserListRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View vi = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false);
        //UI initialization
        vi.setOnClickListener(mOnClickListener);
        ViewHolder viewHolder = new ViewHolder(vi);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        viewHolder.userProfile = userProfiles.get(position);

        viewHolder.imageButton.setTag(viewHolder.userProfile);
        //vi.setTag(userProfile);

        //Current user cannot follow himself
        if (viewHolder.userProfile.getUserId() != globalVariable.getCurrentUser().getUserId()) {
            if (viewHolder.userProfile.isFollowed()) {
                viewHolder.imageButton.setImageResource(R.drawable.oval1);
            } else {
                viewHolder.imageButton.setImageResource(R.drawable.follow_list2);
            }

            viewHolder.imageButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (((UserProfile) arg0.getTag()).isFollowed()) {
                        viewHolder.imageButton.setImageResource(R.drawable.follow_list2);

                        List<Long> wallIdList = new ArrayList<Long>();
                        wallIdList.add(((UserProfile) arg0.getTag()).getMyWallId());
                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendUnFollowUserJSON(wallIdList));


                        ((UserProfile) arg0.getTag()).setFollowed(false);
                    } else {
                        viewHolder.imageButton.setImageResource(R.drawable.oval1);

                        List<Long> wallIdList = new ArrayList<Long>();
                        wallIdList.add(((UserProfile) arg0.getTag()).getMyWallId());
                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendFollowUserJSON(wallIdList));

                        ((UserProfile) arg0.getTag()).setFollowed(true);
                    }
                }

            });
        } else {
            viewHolder.imageButton.setVisibility(View.GONE);
        }
//        vi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                UserProfile u = (UserProfile)view.getTag();
//                globalVariable.setOpenProfile(((UserProfile)view.getTag()).getUserId());
//                Intent i = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
//                activity.startActivity(i);
//            }
//        });

        // set new values to template
        viewHolder.txtTitle.setText(viewHolder.userProfile.getFullName());

        String personPhotoUrl = viewHolder.userProfile.getProfileIcon();
        personPhotoUrl = personPhotoUrl.substring(0,
                personPhotoUrl.length() - 2)
                + PROFILE_PIC_SIZE;

        globalVariable.getPicassoHelper().loadImageWithFit(personPhotoUrl, viewHolder.imageView, globalVariable.getPicassoHelper().getRoundedTransformation());
    }

    private void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return userProfiles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public ImageView imageView;
        public ImageButton imageButton;

        public UserProfile userProfile;

        public ViewHolder(View vi) {
            super(vi);

            txtTitle = (TextView) vi.findViewById(R.id.userName);
            imageView = (ImageView) vi.findViewById(R.id.userIcon);
            imageButton = (ImageButton) vi.findViewById(R.id.followButton);


//            likeNumberTextView.setOnClickListener(new View.OnClickListener() {
//                public void onClick(final View v) {
//                    if (likesNumber > 0) {
//
//                        globalVariable.setSelectedWallPost(wallPost);
//
//                        Intent intentRedirect = new Intent(activity.getApplicationContext(), LikesActivity.class);
//                        activity.startActivity(intentRedirect);
//                    }
//                }
//            });
//
//            //button listeners
//            //like button handler
//            likeButton.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View arg0) {
//                    WallPost currentWallPost = (WallPost) arg0.getTag();
//                    if (currentWallPost.isLiked()) {
//                        likeButton.setImageResource(R.drawable.dislike_small);
//
//                        // Sending message to web socket server
//                        sendMessageToServer(utils.getSendDislikeJSON(globalVariable.getCurrentUser().getUserId(), currentWallPost.getWallMessageId()));
//
//                        ((WallPost) arg0.getTag()).setLiked(false);
//                    } else {
//                        likeButton.setImageResource(R.drawable.like_small);
//
//                        // Sending message to web socket server
//                        sendMessageToServer(utils.getSendLikeJSON(globalVariable.getCurrentUser().getUserId(), currentWallPost.getWallMessageId()));
//
//                        ((WallPost) arg0.getTag()).setLiked(true);
//                    }
//                }
//
//            });
//
//            //comment button handler
//            commentNumberTextView.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View arg0) {
//                    WallPost currentWallPost = (WallPost) arg0.getTag();
//                    globalVariable.setSelectedWallPost(wallPost);
//
//                    Intent i = new Intent(activity.getApplicationContext(), CommentActivity.class);
//                    activity.startActivity(i);
//                }
//
//            });
//
//            //comment button handler
//            commentButton.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View arg0) {
//                    WallPost currentWallPost = (WallPost) arg0.getTag();
//                    globalVariable.setSelectedWallPost(currentWallPost);
//
//                    Intent i = new Intent(activity.getApplicationContext(), CommentEditActivity.class);
//                    activity.startActivity(i);
//                }
//
//            });
//
//            wallPostOwnerProfileIcon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    globalVariable.setOpenProfile(userProfile.getUserId());
//                    Intent i = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
//                    activity.startActivity(i);
//                }
//            });

        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            UserProfile u = (UserProfile) view.getTag();


            int itemPosition = recyclerView.getChildLayoutPosition(view);
            UserProfile item = userProfiles.get(itemPosition);
            globalVariable.setOpenProfile(item.getUserId());
            Intent i = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
            activity.startActivity(i);
        }
    }
}
package com.pkletsko.russ.chat.ui.activity.wall.like.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.ui.activity.profile.activity.UserProfileActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;

import java.util.ArrayList;
import java.util.List;


public class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<Like> likes;
    private GlobalClass globalVariable;
    private WebSocketClient client;
    private Utils utils;
    private RecyclerView recyclerView;

    private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    // LogCat tag
    private static final String TAG = LikeAdapter.class.getSimpleName();

    public LikeAdapter(Activity activity, ArrayList<Like> likes, GlobalClass globalVariable, RecyclerView recyclerView) {
        Log.d(TAG, "initialization of LikeAdapter");
        this.recyclerView = recyclerView;
        this.activity = activity;
        this.likes = likes;
        this.globalVariable = globalVariable;
        this.client = globalVariable.getClient();
        this.utils = new Utils(activity.getApplicationContext());
    }

    @Override
    public LikeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View vi = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false);
        //UI initialization
        vi.setOnClickListener(mOnClickListener);
        ViewHolder viewHolder = new ViewHolder(vi);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        viewHolder.userProfile = globalVariable.getUsersCash().get(likes.get(position).getFromUserId());

        if (globalVariable.getCurrentUser().getUserId() != viewHolder.userProfile.getUserId()) {
            viewHolder.imageButton.setTag(viewHolder.userProfile);
            //vi.setTag(userProfile);

            if (viewHolder.userProfile.isFollowed()) {
                viewHolder.imageButton.setImageResource(R.drawable.oval1);
            } else {
                viewHolder.imageButton.setImageResource(R.drawable.follow_list2);
            }

            viewHolder.imageButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (globalVariable.isAppRunning()) {
                        if (((UserProfile) arg0.getTag()).isFollowed()) {
                            viewHolder.imageButton.setImageResource(R.drawable.follow_list2);

                            List<Long> wallIdList = new ArrayList<Long>();
                            wallIdList.add(((UserProfile) arg0.getTag()).getMyWallId());
                            // Sending message to web socket server
                            sendMessageToServer(utils.getSendUnFollowUserJSON(wallIdList));


                            ((UserProfile) arg0.getTag()).setFollowed(false);
                        } else {
                            viewHolder.imageButton.setImageResource(R.drawable.oval1);

                            List<Long> wallIdList = new ArrayList<Long>();
                            wallIdList.add(((UserProfile) arg0.getTag()).getMyWallId());
                            // Sending message to web socket server
                            sendMessageToServer(utils.getSendFollowUserJSON(wallIdList));

                            ((UserProfile) arg0.getTag()).setFollowed(true);
                        }
                    } else {
                        Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                    }
                }

            });
        } else {
            viewHolder.imageButton.setVisibility(View.GONE);
        }
        // set new values to template
        viewHolder.txtTitle.setText(viewHolder.userProfile.getFullName());

        globalVariable.getPicassoHelper().loadImageWithFit(viewHolder.userProfile.getProfileIcon(), viewHolder.imageView, globalVariable.getPicassoHelper().getRoundedTransformation());
    }

    private void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return likes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public ImageView imageView;
        public ImageButton imageButton;

        public UserProfile userProfile;

        public ViewHolder(View vi) {
            super(vi);

            txtTitle = (TextView) vi.findViewById(R.id.userName);
            imageView = (ImageView) vi.findViewById(R.id.userIcon);
            imageButton = (ImageButton) vi.findViewById(R.id.followButton);
        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            if (globalVariable.isAppRunning()) {
                int itemPosition = recyclerView.getChildLayoutPosition(view);
                UserProfile item = globalVariable.getUsersCash().get(likes.get(itemPosition).getFromUserId());
                globalVariable.setOpenProfile(item.getUserId());
                Intent i = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
                activity.startActivity(i);
            } else {
                Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
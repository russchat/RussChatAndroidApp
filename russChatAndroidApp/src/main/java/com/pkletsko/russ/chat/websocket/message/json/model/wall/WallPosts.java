package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class WallPosts extends BasicTransportJSONObject implements JSONResponse {

    private Set<WallPost> wallPosts;

    public Set<WallPost> getWallPosts() {
        return wallPosts;
    }

    public void setWallPosts(Set<WallPost> wallPosts) {
        this.wallPosts = wallPosts;
    }
}

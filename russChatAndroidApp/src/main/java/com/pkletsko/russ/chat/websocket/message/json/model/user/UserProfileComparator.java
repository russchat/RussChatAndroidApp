package com.pkletsko.russ.chat.websocket.message.json.model.user;

import java.util.Comparator;

/**
 * Created by pkletsko on 19.04.2015.
 */
public class UserProfileComparator implements Comparator<UserProfile> {
    @Override
    public int compare(UserProfile e1, UserProfile e2) {
        final long id1 = e1.getUserId(), id2 = e2.getUserId();
        if (id1 == id2)
            return 0;
        return id1 < id2 ? 1 : -1;
    }
}

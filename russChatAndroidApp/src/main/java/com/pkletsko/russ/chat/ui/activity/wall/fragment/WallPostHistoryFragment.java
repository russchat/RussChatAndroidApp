package com.pkletsko.russ.chat.ui.activity.wall.fragment;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.base.BaseWallPostFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.putToCash;


public class WallPostHistoryFragment extends BaseWallPostFragment {

    private static final String TAG = WallPostHistoryFragment.class.getSimpleName();

    @Override
    protected Set<WallPost> getCash() {
        return new HashSet<>();
    }

    @Override
    protected Map<String, String> getRequestParameters() {
        final Map<String,String> requestParameters = new HashMap<>();
        requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + GlobalClass.CONFIG_GET_USER_WALL_POSTS_URL);
        requestParameters.put("id", globalVariable.getOpenProfile().toString());
        return requestParameters;
    }

    @Override
    protected void removeWallPost(ArrayList<WallPost> clearedWallPosts, final int index){
        final Long wallPostId = clearedWallPosts.get(index).getWallMessageId();
        wallPostService.deleteWallPostFromMDB(wallPostId);
        removeWallPostFromCash(wallPostId);
        //TODO should be some cash
        //removedFromFollowedWallPostCash(clearedWallPosts.get(index));
        clearedWallPosts.remove(index);
    }

    @Override
    protected void saveWallPost(final WallPost wallPost) {
        wallPost.setReceived(false);
        putToCash(wallPost, globalVariable.getWallPostCash());
        //TODO should be some cash
        //putToCash(wallPost, globalVariable.getFollowedWallPostCash());
        //putToWallPostCash(wallPost);
        //putToFollowedWallPostCash(wallPost);
        wallPostService.saveWallPostToMDB(wallPost, true);
    }
}

package com.pkletsko.russ.chat.other;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.pkletsko.russ.chat.common.GlobalClass;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by pkletsko on 26.05.2015.
 */
public abstract class RequestUtil {

    private static final String TAG = RequestUtil.class.getSimpleName();

    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";

    public static final String SERVICE_NAME = "RussChatService";
    public static final String SERVICE_PATH = "/" + SERVICE_NAME + "/main";

    public static final String REQUEST_URL_KEY = "url";

    public static final String REQUEST_ATTACHMENT_ID_KEY = "attachmentId";

    public static final String REQUEST_FILE_NAME_KEY = "fileName";
    public static final String REQUEST_FILE_PATH_KEY = "filePath";

    private static final MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");

    public void request(final Map<String, String> requestParameters, final GlobalClass globalClass) {
        Log.i(TAG, "request : Start");
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                OkHttpClient client = globalClass.getOkHttpClient();
                String attachmentId = requestParameters.get(REQUEST_ATTACHMENT_ID_KEY);
                RequestBody formBody;
                if (attachmentId == null) {
                    FormBody.Builder formEncodingBuilder = new FormBody.Builder();
                    for (Map.Entry<String, String> entry : requestParameters.entrySet()) {
                        if (!entry.getKey().equals(REQUEST_URL_KEY)) {
                            if (entry.getKey() != null && entry.getValue() != null) {
                                formEncodingBuilder.add(entry.getKey(), entry.getValue());
                            }
                        }
                    }

                    formBody = formEncodingBuilder.build();
                } else {
                    MultipartBody.Builder formEncodingBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM);
                    for (Map.Entry<String, String> entry : requestParameters.entrySet()) {
                        if (!entry.getKey().equals(REQUEST_URL_KEY) && !entry.getKey().equals(REQUEST_FILE_NAME_KEY) && !entry.getKey().equals(REQUEST_FILE_PATH_KEY)) {
                            if (entry.getKey() != null && entry.getValue() != null) {
                                formEncodingBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                            }
                        }
                    }

                    File miFoto = new File(requestParameters.get(REQUEST_FILE_PATH_KEY));
                    Bitmap bmp = BitmapFactory.decodeFile(requestParameters.get(REQUEST_FILE_PATH_KEY));
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 20, bos);
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(miFoto);

                        // Put data in your baos

                        bos.writeTo(fos);
                    } catch(IOException ioe) {
                        // Handle exception here
                        ioe.printStackTrace();
                    } finally {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    formEncodingBuilder.addFormDataPart("file", requestParameters.get(REQUEST_FILE_NAME_KEY) + ".jpg",
                            RequestBody.create(MEDIA_TYPE_JPEG, miFoto));

                    formBody = formEncodingBuilder.build();
                }

                Request request = new Request.Builder()
                        .url(requestParameters.get(REQUEST_URL_KEY))
                        .post(formBody)
                        .build();

                Response response;
                String result = null;
                try {
                    response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    Log.e(TAG, "request:" + e);
                    onPostRequestResponse("Fail");
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.i(TAG, "onPostExecute : request:" + result);
                onPostRequestResponse(result);
            }
        };
        task.execute();
    }

    public void onPostRequestResponse(String result) {
        Log.i(TAG, "onPostServerAlive : SHOULD BE EMPTY:" + result);
        // should be empty
    }
}

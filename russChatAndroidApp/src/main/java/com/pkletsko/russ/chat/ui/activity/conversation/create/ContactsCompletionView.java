package com.pkletsko.russ.chat.ui.activity.conversation.create;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.ui.activity.common.tokenautocomplete.TokenCompleteTextView;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

/**
 * Sample token completion view for basic contact info
 *
 * Created on 9/12/13.
 * @author mgod
 */
public class ContactsCompletionView extends TokenCompleteTextView<UserProfile> {

    public ContactsCompletionView(Context context) {
        super(context);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(UserProfile person) {

        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout)l.inflate(R.layout.contact_token, (ViewGroup)ContactsCompletionView.this.getParent(), false);
        ((TextView)view.findViewById(R.id.name)).setText(person.getFullName());

        return view;
    }

    @Override
    protected UserProfile defaultObject(String completionText) {
//        //Stupid simple example of guessing if we have an email or not
//        int index = completionText.indexOf('@');
//        if (index == -1) {
//            return new Person(completionText, completionText.replace(" ", "") + "@example.com");
//        } else {
//            return new Person(completionText.substring(0, index), completionText);
//        }
        return null;
    }
}

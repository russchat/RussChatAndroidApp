package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONRequest;

import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
public class FollowingChangeRequest extends BasicTransportJSONObject implements JSONRequest {

    private List<Long> walls;

    public List<Long> getWalls() {
        return walls;
    }

    public void setWalls(List<Long> walls) {
        this.walls = walls;
    }
}

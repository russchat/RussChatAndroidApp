package com.pkletsko.russ.chat.ui.activity.wall.post;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.ui.activity.common.activity.CustomViewPager;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseProfileActivity;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.WallPostFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class WallPostActivity extends BaseProfileActivity {

    // LogCat tag
    private static final String TAG = WallPostActivity.class.getSimpleName();

    private ImageView wallPostOwnerProfileIcon;
    private TextView wallPostOwnerName;
    private TextView wallPostTime;
    private TextView wallPostTextView;

    private ProgressDialog mDialog;

    private ImageButton backButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {
            final Toolbar mCustomView = (Toolbar) findViewById(R.id.wall_post_custom_action_toolbar);
            setSupportActionBar(mCustomView);

            //mDialog = ProgressDialog.show(this, "Loading", "Wait while loading...");

            wallPostOwnerName = (TextView) findViewById(R.id.wallPostOwnerName);
            wallPostTime = (TextView) findViewById(R.id.wallPostTime);
            wallPostTextView = (TextView) findViewById(R.id.wallPost);

            backButton = (ImageButton) findViewById(R.id.backActionCustomActionBar);

            backButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    onBackPressed();
                }

            });

            updateWallPostInfo();
        }
    }

    @Override
    protected void beforeSetContentView() {
        setTheme(R.style.AppThemeWallPost);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        startService(intentBroadcastService);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.wall_post_main;
    }

    @Override
    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
//                if (result != null && result.trim().equals("OK")) {
//                    sendMessageToServer(utils.getSendUserProfileRequestJSON(globalVariable.getOpenProfile()));
//                } else {
//                    runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            Toast.makeText(getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
//
//                            // offline mode
//                            //load user from cash if exist
//                            updateUserProfile(globalVariable.getUsersCash().get(globalVariable.getOpenProfile()));
//
//                            globalVariable.setIsAppRunning(false);
//                            stopService(intentBroadcastService);
//                        }
//                    });
//                }
                //mDialog.dismiss();
            }
        }).isServerAlive(globalVariable);
    }

    private void updateWallPostInfo() {
            initViewPagerAndTabs();

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    final WallPost wallPost = globalVariable.getSelectedWallPost();

                    final Date createdDate = wallPost.getCreated();
                    //2 December 12:00 PM

                    if (createdDate != null) {
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:mm");
                        String dateToStr = format.format(createdDate);
                        wallPostTime.setText(dateToStr);
                    }

                    //get latest version of user (owner of this post)
                    final UserProfile userProfile = globalVariable.getUsersCash().get(wallPost.getFromUserId());

                    wallPostOwnerName.setText(userProfile.getFullName());

                    wallPostTextView.setText(wallPost.getMessageText());

                    wallPostOwnerProfileIcon = (ImageView) findViewById(R.id.wallPostOwnerProfileIcon);

                    //TODO 150 ???? round
                    globalVariable.getPicassoHelper().loadImageWithFit(userProfile.getProfileIcon(), wallPostOwnerProfileIcon, globalVariable.getPicassoHelper().getRoundedTransformation());

                   // mDialog.dismiss();
                }
            });
    }

    private void initViewPagerAndTabs() {
        CustomViewPager viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getFragmentManager());

        pagerAdapter.addFragment(new WallPostFragment(), String.valueOf(globalVariable.getCurrentUser().getMyPosts()), getApplicationContext());

        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title, Context mycon) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
            context = mycon;
        }

        Context context;
        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.profile_tab_header, null);
            return v;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}

package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class MessageMDAO {
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_MESSAGE_MDB_ID,
            RussSQLiteHelper.COLUMN_MESSAGE_TEXT, 
            RussSQLiteHelper.COLUMN_MESSAGE_ID,
            RussSQLiteHelper.COLUMN_CONVERSATION_ID, 
            RussSQLiteHelper.COLUMN_MESSAGE_FROM_USER_ID, 
            RussSQLiteHelper.COLUMN_MESSAGE_FROM_USER_NAME,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public MessageMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public ChatMessage createMessage(final ChatMessage chatMessage, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_MESSAGE_TEXT, chatMessage.getMessageText());
        values.put(RussSQLiteHelper.COLUMN_MESSAGE_ID, chatMessage.getMessageId());
        values.put(RussSQLiteHelper.COLUMN_CONVERSATION_ID, chatMessage.getConversationId());
        values.put(RussSQLiteHelper.COLUMN_MESSAGE_FROM_USER_ID, chatMessage.getFromUserId());
        values.put(RussSQLiteHelper.COLUMN_MESSAGE_FROM_USER_NAME, chatMessage.getFromUserName());
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
        long insertId = database.insert(RussSQLiteHelper.TABLE_MESSAGE, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_MESSAGE_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        ChatMessage newChatMessage = cursorToChatMessage(cursor);
        cursor.close();
        return newChatMessage;
    }

    public void createChatMessages(final Set<ChatMessage> chatMessages, final Long appOwnerId) {
        for (ChatMessage chatMessage: chatMessages) {
            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_MESSAGE_TEXT, chatMessage.getMessageText());
            values.put(RussSQLiteHelper.COLUMN_MESSAGE_ID, chatMessage.getMessageId());
            values.put(RussSQLiteHelper.COLUMN_CONVERSATION_ID, chatMessage.getConversationId());
            values.put(RussSQLiteHelper.COLUMN_MESSAGE_FROM_USER_ID, chatMessage.getFromUserId());
            values.put(RussSQLiteHelper.COLUMN_MESSAGE_FROM_USER_NAME, chatMessage.getFromUserName());
            values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
            database.insert(RussSQLiteHelper.TABLE_MESSAGE, null, values);
        }
    }

    public void deleteChatMessage(ChatMessage chatMessage) {
        long id = chatMessage.getMessageMDBId();
        database.delete(RussSQLiteHelper.TABLE_MESSAGE, RussSQLiteHelper.COLUMN_MESSAGE_MDB_ID + " = " + id, null);
    }

    public List<ChatMessage> getAllChatMessages() {
        List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_MESSAGE, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ChatMessage chatMessage = cursorToChatMessage(cursor);
            chatMessageList.add(chatMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return chatMessageList;
    }

    public Set<ChatMessage> getChatMessagesByConversationId(final Long conversationId, final Long appOwnerId) {
        Set<ChatMessage> chatMessageList = new TreeSet<ChatMessage>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_CONVERSATION_ID + " = " + conversationId+ " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ChatMessage chatMessage = cursorToChatMessage(cursor);
            chatMessageList.add(chatMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return chatMessageList;
    }

    private ChatMessage cursorToChatMessage(Cursor cursor) {
        ChatMessage messageMDTO = new ChatMessage();
        messageMDTO.setMessageMDBId(cursor.getLong(0));
        messageMDTO.setMessageText(cursor.getString(1));
        messageMDTO.setMessageId(cursor.getLong(2));
        messageMDTO.setConversationId(cursor.getLong(3));
        messageMDTO.setFromUserId(cursor.getLong(4));
        messageMDTO.setFromUserName(cursor.getString(5));
        return messageMDTO;
    }
}

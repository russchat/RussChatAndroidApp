package com.pkletsko.russ.chat.common;

/**
 * Created by pkletsko on 04.02.2015.
 */

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Locale;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;


public class BroadcastService extends Service implements GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = BroadcastService.class.getSimpleName();

    private GlobalClass globalVariable;

    private WebSocketClient client;

    private Utils utils;

    private EventHub eventHub;

    private ServerCheckUtil serverCheckUtil;

    private GoogleApiClient mGoogleApiClient;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        FacebookSdk.sdkInitialize(getApplicationContext());

        globalVariable = (GlobalClass) getApplicationContext();

        utils = new Utils(getApplicationContext());

        //all received messages will be passed to even hub to appropriate handling
        eventHub = new EventHub(globalVariable, utils, this);

        //call is server alive check (simple rest post call without special parameters which expect receive "OK" response)
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                Log.d(TAG, " onPostServerAlive : result = " + result + ", globalVariable.isAppRunning() = " + globalVariable.isAppRunning());
                if (result != null && result.trim().equals("OK") && !globalVariable.isAppRunning()) {

                    //TODO reconnect to google+ /facebook

                    if (globalVariable.getCurrentUser() == null) {
                        Log.d(TAG, " New User Case : before first websocket connection to server. User Initialization will be started.");
                        connectToServer();
                    } else {
                        Log.d(TAG, " User Already Logged In Case : Connection to Provider and then connection to websocket");
                        if (globalVariable.getCurrentUser().getLoginSrc() == 1) {
                            Log.d(TAG, " User Already Logged In Case : Connection to Facebook");

                            if (AccessToken.getCurrentAccessToken() != null) {
                                Log.d(TAG, "User Already Logged In Case : FACEBOOK AccessToken.getCurrentAccessToken() != null");
                                globalVariable.setLoginSourceType(LoginSourceType.FACEBOOK);
                                globalVariable.setToken(AccessToken.getCurrentAccessToken().getToken());
                                connectToServer();
                            }
                        } else if (globalVariable.getCurrentUser().getLoginSrc() == 2) {
                            Log.d(TAG, " User Already Logged In Case : Connection to Google");
                            mGoogleApiClient = buildGoogleApiClient();
                            mGoogleApiClient.connect();
                        }
                    }
                }
            }
        }).isServerAlive(globalVariable);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason.
        // We call connect() to attempt to re-establish the connection or get a
        // ConnectionResult that we can attempt to resolve.
        mGoogleApiClient.connect();
    }

    /* onConnected is called when our Activity successfully connects to Google
 * Play services.  onConnected indicates that an account was selected on the
 * device, that the selected account has granted any requested permissions to
 * our app and that we were able to establish a service connection to Google
 * Play services.
 */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Reaching onConnected means we consider the user signed in.
        Log.i(TAG, "onConnected");

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String accessToken = null;

                try {
                    String mScope = "oauth2:https://www.googleapis.com/auth/plus.login";

                    Bundle appActivities = new Bundle();

                    appActivities.putString(GoogleAuthUtil.KEY_REQUEST_VISIBLE_ACTIVITIES, "http://schemas.google.com/AddActivity");
                    if (mGoogleApiClient.isConnected()) {
                        accessToken = GoogleAuthUtil.getToken(getApplicationContext(), Plus.AccountApi.getAccountName(mGoogleApiClient), mScope);
                    }
                } catch (IOException transientEx) {
                    // Network or server error, try later
                    Log.e(TAG, transientEx.toString());
                } catch (UserRecoverableAuthException e) {
                    // Recover (with e.getIntent())
                    Log.e(TAG, e.toString());
                    //Intent recover = e.getIntent();
                    //startActivityForResult(recover, GoogleLoginActivity.STATE_DEFAULT);
                } catch (GoogleAuthException authEx) {
                    // The call is not ever expected to succeed
                    // assuming you have already verified that
                    // Google Play services is installed.
                    Log.e(TAG, authEx.toString());
                }

                return accessToken;
            }

            @Override
            protected void onPostExecute(String accessToken) {
                if (accessToken != null) {
                    Log.i(TAG, "GOOGLE Access token retrieved:" + accessToken);

                    globalVariable.setLoginSourceType(LoginSourceType.GOOGLE);
                    globalVariable.setToken(accessToken);

                    connectToServer();

//                        Intent intentRedirect = new Intent(GoogleLoginActivity.this, LoginRedirectActivity.class);
//                        startActivity(intentRedirect);
                }
            }
        };

        task.execute();

    }

    private GoogleApiClient buildGoogleApiClient() {
        Log.i(TAG, "buildGoogleApiClient");
        // When we build the GoogleApiClient we specify where connected and
        // connection failed callbacks should be returned, which Google APIs our
        // app uses and which OAuth 2.0 scopes our app requests.
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }

    private void connectToServer() {
        Log.d(TAG, " connectToServer method");
        //init websocket connection to the server
        createWebSocketConnection();

        //init of websocket should be only one time
        globalVariable.setIsAppRunning(true);

        //store websocket link to send messages from whole application
        globalVariable.setClient(client);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (client != null && client.isConnected()) {
            client.disconnect();
        }
        super.onDestroy();
    }

    public void createWebSocketConnection() {
        Log.d(TAG, "createWebSocketConnection method");
        // create connection to server
        if (globalVariable != null && globalVariable.getToken() != null && globalVariable.getLoginSourceType() != null) {
            String token = null;
            try {
                token = URLEncoder.encode(globalVariable.getToken(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                //TODO chamhe it later
                e.printStackTrace();
            }
            Log.d(TAG, "createWebSocketConnection: token = " + token + " , scr = " + globalVariable.getLoginSourceType());
            StringBuilder url = new StringBuilder();
            url.append("wss://");
            url.append(globalVariable.getIp());
            url.append(GlobalClass.URL_WEBSOCKET);
            url.append(token);
            url.append("&src=");
            url.append(globalVariable.getLoginSourceType().getCode());
            Log.d(TAG, "websocket URL:" + url.toString());
            URI websocketURI = URI.create(url.toString());

            WebSocketClient.Listener listener = new WebSocketClient.Listener() {

                @Override
                public void onConnect() {
                    Log.d(TAG, "Connected");
                    //showToast("Websocket Connected");
                }

                /**
                 * On receiving the message from web socket server
                 */
                @Override
                public void onMessage(String message) {
                    Log.d(TAG, String.format("Got string message! %s", message));

                    parseMessage(message);

                }

                @Override
                public void onMessage(byte[] data) {
                //Log.d(TAG, String.format("Got binary message! %s", bytesToHex(data)));
                    Log.d(TAG, "sdasd");
//
//                // Message will be in JSON format
//                parseMessage(bytesToHex(data));
                }

                /**
                 * Called when the connection is terminated
                 */
                @Override
                public void onDisconnect(int code, String reason) {

                    String message = String.format(Locale.US, "Disconnected! Code: %d Reason: %s", code, reason);

                    //showToast(message);

                    // clear the session id from shared preferences
                    utils.storeSessionId(null);
                    globalVariable.setIsAppRunning(false);
                }

                @Override
                public void onError(Exception error) {
                    Log.e(TAG, "Error! : " + error);
                    //showToast("Error! : " + error);
                    globalVariable.setIsAppRunning(false);
                }

            };



            client = new WebSocketClient(websocketURI, listener, null);

            client.setTrustManagers(getTrustManagers());

            client.connect();
        }
    }

    private TrustManager[] getTrustManagers() {
        // Add support for self-signed (local) SSL certificates
        // Based on http://developer.android.com/training/articles/security-ssl.html#UnknownCa
        try {

            // Load CAs from an InputStream
            // (could be from a resource or ByteArrayInputStream or ...)
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // From https://www.washington.edu/itconnect/security/ca/load-der.crt
            InputStream is = getApplicationContext().getResources().openRawResource(R.raw.jetty);
            InputStream caInput = new BufferedInputStream(is);
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                // System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }


            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);


            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);


//            // Create an SSLContext that uses our TrustManager
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, tmf.getTrustManagers(), null);
            return tmf.getTrustManagers();


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void showToast(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }

    /**
     * Parsing the JSON message received from server The intent of message will
     * be identified by JSON node 'flag'. flag = self, message belongs to the
     * person. flag = new, a new person joined the conversation. flag = message,
     * a new message received from server. flag = exit, somebody left the
     * conversation.
     */
    private void parseMessage(final String msg) {
        try {
            JSONObject jObj = new JSONObject(msg);

            MessageObjectType objectType = MessageObjectType.findByCode(jObj.getInt("objectType"));

            switch (objectType) {
                case MOT_LOGIN_STATUS:
                    eventHub.loginStatusEventHandler(msg);
                    break;
                case MOT_UPDATE_USER_PROFILE_INFO:
                    eventHub.updateUserProfileInfoEventHandler(msg);
                    break;
                case MOT_UNRECEIVED_MESSAGES:
                    eventHub.unreceivedChatMessagesEventHandler(msg);
                    break;
                case MOT_UNRECEIVED_WALL_MESSAGES:
                    eventHub.unreceivedWallPostsEventHandler(msg);
                    break;
                case MOT_UNRECEIVED_LIKES:
                    eventHub.unreceivedLikesEventHandler(msg);
                    break;
                case MOT_UNRECEIVED_COMMENTS:
                    eventHub.unreceivedCommentsEventHandler(msg);
                    break;
                case MOT_SEARCH:
                    eventHub.searchEventHandler(msg);
                    break;
                case MOT_CHAT_USER_SEARCH:
                    eventHub.chatUserSearchEventHandler(msg);
                    break;
                case MOT_CREATE_NEW_CONVERSATIONS:
                    eventHub.conversationEventHandler(msg);
                    break;
                case MOT_NEW_MESSAGE:
                    eventHub.chatMessageEventHandler(msg);
                    break;
                case MOT_NEW_WALL_MESSAGE:
                    eventHub.wallPostEventHandler(msg);
                    break;
                case MOT_LIKE:
                    eventHub.likeEventHandler(msg);
                    break;
                case MOT_DISLIKE:
                    eventHub.dislikeEventHandler(msg);
                    break;
                case MOT_COMMENT:
                    eventHub.commentEventHandler(msg);
                    break;
                case MOT_GET_USER_PROFILE:
                    eventHub.userProfileEventHandler(msg);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

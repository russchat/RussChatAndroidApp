package com.pkletsko.russ.chat.storage.service;

import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedComments;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedLikes;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedWallPosts;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnloadedAttachmentWallPostLink;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.Set;

/**
 * Created by pkletsko on 25.05.2015.
 */
public interface WallPostService {
    public void saveWallPostToMDB(final WallPost wallMessage, final boolean checkUser);

    public void saveWallPostsToMDB(final Set<WallPost> wallPosts, final boolean received);

    public void saveWallPostsToMDB(final Set<WallPost> wallPosts, final boolean received, final boolean checkUser);

    public void saveUnReceivedWallPostsToMDB(final UnReceivedWallPosts unReceivedWallPosts);

    public void saveCommentToMDB(final Comment comment);

    public void saveLikeToMDB(final Like like);

    public void deleteLikeFromMDB(final Like like);

    public void deleteUnloadAttachmentFromMDB(final FileToUpload fileToUpload);

    public void deleteUnloadAttachmentWallPostLinkFromMDB(final Long attachmentId);

    public void deleteWallPostFromMDB(final Long wallPostId);

    public void storeUnReceivedLikesToMDB(final UnReceivedLikes unReceivedLikes);

    public void removeUnReceivedLikeToMDB(final Like like);

    public void saveUnloadedAttachmentWallPostLinkToMDB(final UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink);

    public void initializationOfWall(final UnReceivedWallPosts unReceivedWallPosts);

    public void uploadFileToServer(final FileToUpload fileToUpload);

    public void updateWallPostLikeNumberMDB(final Like like, final boolean likeFlag, final boolean currentUserLike);

    public void updateWallPostCommentNumberMDB(final Comment comment, final String msg);

    public void initializationOfComments(final UnReceivedComments unReceivedComments);

    public void initializationOfLikes(final UnReceivedLikes unReceivedLikes);

    public void putToWallPostCash(final WallPost wallPost);
}

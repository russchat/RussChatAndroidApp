package com.pkletsko.russ.chat.websocket.message.json.model.chat;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessage extends BasicTransportJSONObject implements JSONResponse, Comparable {

    private Long messageMDBId;

    private Long messageId;

    private Long fromUserId;

    private String fromUserName;

    private String messageText;

    private Long conversationId;

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getMessageMDBId() {
        return messageMDBId;
    }

    public void setMessageMDBId(Long messageMDBId) {
        this.messageMDBId = messageMDBId;
    }

    @Override
    public int compareTo(Object o) {
        ChatMessage msg = (ChatMessage) o;
        Long result = this.messageId - msg.getMessageId();
        return result.intValue();
    }
}

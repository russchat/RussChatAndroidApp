package com.pkletsko.russ.chat.other;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.gson.Gson;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Conversations;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.CreateNewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.NewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ReNewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.UnReceivedChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolClassInfoSearchResult;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfo;
import com.pkletsko.russ.chat.websocket.message.json.model.user.SchoolInfoSearchResult;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UnReceivedInformationRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserLogin;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileEdit;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfileResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfiles;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comments;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FollowingChangeRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Likes;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SearchRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SearchResult;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.SendCompletedWallPostRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedComments;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedLikes;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedWallPosts;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Uploaded;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPosts;

import java.util.List;
import java.util.Set;

import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_CHAT_USER_SEARCH;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_COMMENT;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_CREATE_NEW_CONVERSATIONS;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_CREATE_NEW_SCHOOL;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_CREATE_NEW_SCHOOL_CLASS;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_DISLIKE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_FOLLOW_WALL;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_GET_UNRECEIVED_INFORMATION;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_GET_USER_PROFILE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_LIKE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_NEW_MESSAGE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_NEW_WALL_MESSAGE;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_RE_NEW_CONVERSATIONS;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_SEARCH;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_SEND_COMPLETED_WALL_POST;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_UNFOLLOW_WALL;
import static com.pkletsko.russ.chat.websocket.message.json.model.common.MessageObjectType.MOT_UPDATE_USER_PROFILE_INFO;


public class Utils {

	private Context context;
	private SharedPreferences sharedPref;

    private static final String TAG = Utils.class.getSimpleName();

	private static final String KEY_SHARED_PREF = "ANDROID_WEB_CHAT";
	private static final int KEY_MODE_PRIVATE = 0;
	private static final String KEY_SESSION_ID = "sessionId";

    Gson gson = new Gson();

	public Utils(Context context) {
		this.context = context;
		sharedPref = this.context.getSharedPreferences(KEY_SHARED_PREF,
				KEY_MODE_PRIVATE);
	}

	public void storeSessionId(String sessionId) {
		Editor editor = sharedPref.edit();
		editor.putString(KEY_SESSION_ID, sessionId);
		editor.commit();
	}

	public String getSessionId() {
		return sharedPref.getString(KEY_SESSION_ID, null);
	}

    public Comment getComment(final String jsonFromServer) {
        Comment comment = null;
        try {
            comment = gson.fromJson(jsonFromServer, Comment.class);
        } catch (Exception e) {
            Log.e(TAG, "getComment " + e);
        }

        return comment;
    }

    public ChatMessage getChatMessage(final String jsonFromServer) {
        ChatMessage chatMessage = null;
        try {
            chatMessage = gson.fromJson(jsonFromServer, ChatMessage.class);
        } catch (Exception e) {
            Log.e(TAG, "getChatMessage " + e);
        }

        return chatMessage;
    }

    public WallPost getWallMessageUI(final String jsonFromServer) {
        WallPost result = null;
        try {
            result = gson.fromJson(jsonFromServer, WallPost.class);
        } catch (Exception e) {
            Log.e(TAG, "getWallMessageUI " + e);
        }

        return result;
    }

    public NewChat getNewConversationResponse(final String jsonFromServer) {
        NewChat result = null;
        try {
            result = gson.fromJson(jsonFromServer, NewChat.class);
        } catch (Exception e) {
            Log.e(TAG, "getNewConversationResponse " + e);
        }

        return result;
    }

    public UnReceivedChatMessages getUnReceivedChatMessages(final String jsonFromServer) {
        UnReceivedChatMessages result = null;
        try {
            result = gson.fromJson(jsonFromServer, UnReceivedChatMessages.class);
        } catch (Exception e) {
            Log.e(TAG, "getUnReceivedChatMessages " + e);
        }

        return result;
    }

    public UnReceivedWallPosts getUnReceivedWallMessages(final String jsonFromServer) {
        UnReceivedWallPosts result = null;
        try {
            result = gson.fromJson(jsonFromServer, UnReceivedWallPosts.class);
        } catch (Exception e) {
            Log.e(TAG, "getUnReceivedWallMessages " + e);
        }

        return result;
    }

    public UnReceivedLikes getUnReceivedLikes(final String jsonFromServer) {
        UnReceivedLikes result = null;
        try {
            result = gson.fromJson(jsonFromServer, UnReceivedLikes.class);
        } catch (Exception e) {
            Log.e(TAG, "getUnReceivedLikes " + e);
        }

        return result;
    }

    public UnReceivedComments getUnReceivedComments(final String jsonFromServer) {
        UnReceivedComments result = null;
        try {
            result = gson.fromJson(jsonFromServer, UnReceivedComments.class);
        } catch (Exception e) {
            Log.e(TAG, "getUnReceivedComments " + e);
        }

        return result;
    }

    public SearchResult getSearchResult(final String jsonFromServer) {
        SearchResult result = null;
        try {
            result = gson.fromJson(jsonFromServer, SearchResult.class);
        } catch (Exception e) {
            Log.e(TAG, "getSearchResult " + e);
        }

        return result;
    }

    public UserProfiles getUserProfiles(final String jsonFromServer) {
        UserProfiles userProfiles = null;
        try {
            userProfiles = gson.fromJson(jsonFromServer, UserProfiles.class);
        } catch (Exception e) {
            Log.e(TAG, "getUserProfiles " + e);
        }

        return userProfiles;
    }

    public UserProfileResponse getUserProfileResponse(final String jsonFromServer) {
        UserProfileResponse result = null;
        try {
            result = gson.fromJson(jsonFromServer, UserProfileResponse.class);
        } catch (Exception e) {
            Log.e(TAG, "getUserProfileResponse " + e);
        }

        return result;
    }

    public Likes getLikes(final String jsonFromServer) {
        Likes result = null;
        try {
            result = gson.fromJson(jsonFromServer, Likes.class);
        } catch (Exception e) {
            Log.e(TAG, "getLikes " + e);
        }

        return result;
    }

    public Comments getComments(final String jsonFromServer) {
        Comments result = null;
        try {
            result = gson.fromJson(jsonFromServer, Comments.class);
        } catch (Exception e) {
            Log.e(TAG, "getComments " + e);
        }

        return result;
    }

    public Conversations getConversations(final String jsonFromServer) {
        Conversations result = null;
        try {
            result = gson.fromJson(jsonFromServer, Conversations.class);
        } catch (Exception e) {
            Log.e(TAG, "getConversations " + e);
        }

        return result;
    }

    public ChatMessages getChatMessages(final String jsonFromServer) {
        ChatMessages result = null;
        try {
            result = gson.fromJson(jsonFromServer, ChatMessages.class);
        } catch (Exception e) {
            Log.e(TAG, "getChatMessages " + e);
        }

        return result;
    }

    public SchoolInfoSearchResult getSchoolInfoSearchResult(final String jsonFromServer) {
        SchoolInfoSearchResult result = null;
        try {
            result = gson.fromJson(jsonFromServer, SchoolInfoSearchResult.class);
        } catch (Exception e) {
            Log.e(TAG, "getSchoolInfoSearchResult " + e);
        }

        return result;
    }

    public SchoolClassInfoSearchResult getSchoolClassInfoSearchResult(final String jsonFromServer) {
        SchoolClassInfoSearchResult result = null;
        try {
            result = gson.fromJson(jsonFromServer, SchoolClassInfoSearchResult.class);
        } catch (Exception e) {
            Log.e(TAG, "getSchoolClassInfoSearchResult " + e);
        }

        return result;
    }

    public SchoolInfo getSchoolInfo(final String jsonFromServer) {
        SchoolInfo result = null;
        try {
            result = gson.fromJson(jsonFromServer, SchoolInfo.class);
        } catch (Exception e) {
            Log.e(TAG, "getSchoolInfo " + e);
        }

        return result;
    }

    public SchoolClassInfo getSchoolClassInfo(final String jsonFromServer) {
        SchoolClassInfo result = null;
        try {
            result = gson.fromJson(jsonFromServer, SchoolClassInfo.class);
        } catch (Exception e) {
            Log.e(TAG, "getSchoolClassInfo " + e);
        }

        return result;
    }

    public WallPosts getUserWallPostsResponse(final String jsonFromServer) {
        WallPosts wallPosts = null;
        try {
            wallPosts = gson.fromJson(jsonFromServer, WallPosts.class);
        } catch (Exception e) {
            Log.e(TAG, "getUserWallPostsResponse " + e);
        }

        return wallPosts;
    }

    public UserLogin getUserLoginResponse(final String jsonFromServer) {
        UserLogin result = null;
        try {
            result = gson.fromJson(jsonFromServer, UserLogin.class);
        } catch (Exception e) {
            Log.e(TAG, "getUserLoginResponse " + e);
        }

        return result;
    }

    public Uploaded getUploadedResult(final String jsonFromServer) {
        Uploaded result = null;
        try {
            result = gson.fromJson(jsonFromServer, Uploaded.class);
        } catch (Exception e) {
            Log.e(TAG, "getUploadedResult " + e);
        }

        return result;
    }

    public Like getLike(final String jsonFromServer) {
        Like result = null;
        try {
            result = gson.fromJson(jsonFromServer, Like.class);
        } catch (Exception e) {
            Log.e(TAG, "getLike " + e);
        }

        return result;
    }

    public String getGSONbyRequestbject (JSONRequest requestObject) {
        return gson.toJson(requestObject);
    }

    public String getGSONbyResponseObject (JSONResponse responseObject) {
        return gson.toJson(responseObject);
    }

    public String getSendUserProfileRequestJSON(final Long userId) {
        UserProfileRequest userProfileRequest = new UserProfileRequest();
        userProfileRequest.setUserId(userId);
        userProfileRequest.setObjectType(MOT_GET_USER_PROFILE);
        return gson.toJson(userProfileRequest);
    }

    public String getSendMessageJSON(final UserProfile currentUser, final String message, final Long conversationId) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setObjectType(MOT_NEW_MESSAGE);
        chatMessage.setMessageText(message);
        chatMessage.setFromUserId(currentUser.getUserId());
        chatMessage.setFromUserName(currentUser.getFullName());
        chatMessage.setConversationId(conversationId);
		return gson.toJson(chatMessage);
	}

    public String getSendWallMessageJSON(String message, final Long wallId, final boolean imageAttachment, final Long attachmentId, final Set<String> attachmentNames) {
        WallPost wallPost = new WallPost();
        wallPost.setObjectType(MOT_NEW_WALL_MESSAGE);
        wallPost.setMessageText(message);
        wallPost.setWallId(wallId);
        wallPost.setAttachmentId(attachmentId);
        wallPost.setImageAttached(imageAttachment);
        wallPost.setImageNumber(attachmentNames.size());
        wallPost.setAttachments(attachmentNames);
        return gson.toJson(wallPost);
    }

    public String getSendCommentJSON(final String message, final Long wallMessageId) {
        Comment comment = new Comment();
        comment.setObjectType(MOT_COMMENT);
        comment.setWallMessageId(wallMessageId);
        comment.setCommentText(message);
        return gson.toJson(comment);
    }

    public String getSendCreateNewConversationJSON(final List<Long> userIdList, final String conversationName) {
        CreateNewChat createNewChat = new CreateNewChat();
        createNewChat.setConversationName(conversationName);
        createNewChat.setUserList(userIdList);
        createNewChat.setObjectType(MOT_CREATE_NEW_CONVERSATIONS);
        return getGSONbyRequestbject(createNewChat);
    }

    public String getSendCreateNewSchoolJSON(SchoolInfo schoolInfo) {
        schoolInfo.setObjectType(MOT_CREATE_NEW_SCHOOL);
        return getGSONbyRequestbject(schoolInfo);
    }

    public String getSendCreateNewSchoolClassJSON(SchoolClassInfo schoolClassInfo) {
        schoolClassInfo.setObjectType(MOT_CREATE_NEW_SCHOOL_CLASS);
        return getGSONbyRequestbject(schoolClassInfo);
    }

    public String getSendUpdateUserProfileInfoJSON(UserProfileEdit userProfileEdit) {
        userProfileEdit.setObjectType(MOT_UPDATE_USER_PROFILE_INFO);
        return getGSONbyRequestbject(userProfileEdit);
    }

    public String getSendFollowUserJSON(final List<Long> wallIdList) {
        FollowingChangeRequest followingChangeRequest = new FollowingChangeRequest();
        followingChangeRequest.setWalls(wallIdList);
        followingChangeRequest.setObjectType(MOT_FOLLOW_WALL);
        return getGSONbyRequestbject(followingChangeRequest);
    }

    public String getSendUnFollowUserJSON(final List<Long> wallIdList) {
        FollowingChangeRequest followingChangeRequest = new FollowingChangeRequest();
        followingChangeRequest.setWalls(wallIdList);
        followingChangeRequest.setObjectType(MOT_UNFOLLOW_WALL);
        return getGSONbyRequestbject(followingChangeRequest);
    }

    public String getSendReNewConversationJSON(final Long conversationId){
        ReNewChat reNewChat = new ReNewChat();
        reNewChat.setConversationId(conversationId);
        reNewChat.setObjectType(MOT_RE_NEW_CONVERSATIONS);
        return getGSONbyRequestbject(reNewChat);
    }

    public String getNewChatJSON(final NewChat newChat){
        return getGSONbyResponseObject(newChat);
    }

    public String getSendLikeJSON(final Long fromUserId, final Long wallMessageId) {
        Like like = new Like();
        like.setFromUserId(fromUserId);
        like.setWallMessageId(wallMessageId);
        like.setDisabled(false);
        like.setObjectType(MOT_LIKE);
        return getGSONbyRequestbject(like);
    }

    public String getSendDislikeJSON(final Long fromUserId, final Long wallMessageId) {
        Like like = new Like();
        like.setFromUserId(fromUserId);
        like.setWallMessageId(wallMessageId);
        like.setDisabled(true);
        like.setObjectType(MOT_DISLIKE);
        return getGSONbyRequestbject(like);
    }

    public String getUnReceivedInformationRequestJSON() {
        //TODO limits for undeceived info will be set for request
        UnReceivedInformationRequest unReceivedInformationRequest = new UnReceivedInformationRequest();
        unReceivedInformationRequest.setObjectType(MOT_GET_UNRECEIVED_INFORMATION);
        return getGSONbyRequestbject(unReceivedInformationRequest);
    }

    public String getSendCompletedWallPostRequestJSON(final Long wallPostId) {
        SendCompletedWallPostRequest sendCompletedWallPostRequest = new SendCompletedWallPostRequest();
        sendCompletedWallPostRequest.setWallPostId(wallPostId);
        sendCompletedWallPostRequest.setObjectType(MOT_SEND_COMPLETED_WALL_POST);
        return getGSONbyRequestbject(sendCompletedWallPostRequest);
    }

    public String getSearchRequestJSON(final String searchQuery) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setSearchQuery(searchQuery);
        searchRequest.setObjectType(MOT_SEARCH);
        return getGSONbyRequestbject(searchRequest);
    }

    public String getChatUserSearchRequestJSON(final String searchQuery) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setSearchQuery(searchQuery);
        searchRequest.setObjectType(MOT_CHAT_USER_SEARCH);
        return getGSONbyRequestbject(searchRequest);
    }
}

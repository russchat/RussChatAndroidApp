package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.ui.activity.settings.IPAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class IPAddressMDAO {
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_IP_ADDRESS_MDB_ID,
            RussSQLiteHelper.COLUMN_IP_ADDRESS,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public IPAddressMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void saveOrUpdateIpAddress(final String ip, final Long appOwnerId) {
        IPAddress existedIpAddress = getIpAddress(appOwnerId);
        if(existedIpAddress == null) {
            createIPAddres(ip, appOwnerId);
        } else {
            existedIpAddress.setIpAddress(ip);
            updateIpAddress(existedIpAddress, appOwnerId);
        }
    }

    public IPAddress createIPAddres(final String ip, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_IP_ADDRESS, ip);
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
        long insertId = database.insert(RussSQLiteHelper.TABLE_IP_ADDRESS, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_IP_ADDRESS, allColumns, RussSQLiteHelper.COLUMN_IP_ADDRESS_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        IPAddress newIpAddres = cursorToIPAddress(cursor);
        cursor.close();
        return newIpAddres;
    }

    private void updateIpAddress(final IPAddress ipAddress, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_IP_ADDRESS, ipAddress.getIpAddress());
        database.update(RussSQLiteHelper.TABLE_IP_ADDRESS, values, RussSQLiteHelper.COLUMN_IP_ADDRESS_MDB_ID + " = " + ipAddress.getIpMDBId() + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public IPAddress getIpAddress(final Long appOwnerId) {
        List<IPAddress> ipAddressList = new ArrayList<>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_IP_ADDRESS, allColumns, RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ipAddressList.add(cursorToIPAddress(cursor));
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        if(ipAddressList.isEmpty()) {
            return null;
        }

        return ipAddressList.get(0);
    }

    private IPAddress cursorToIPAddress(Cursor cursor) {
        IPAddress ipAddress = new IPAddress();
        ipAddress.setIpMDBId(cursor.getLong(0));
        ipAddress.setIpAddress(cursor.getString(1));
        return ipAddress;
    }
}

package com.pkletsko.russ.chat.ui.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.ui.activity.common.activity.MainActivity;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeIPActivity extends BaseActivity {

    // LogCat tag
    private static final String TAG = ChangeIPActivity.class.getSimpleName();

    private static final String IPADDRESS_PATTERN =
            "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}):(\\d{1,5})";

    private EditText ipAddress;

    private String currentIpAddres;

    private Pattern pattern;
    private Matcher matcher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Change Ip");

        currentIpAddres = globalVariable.getIp();

        ipAddress = (EditText) findViewById(R.id.ipAddress);

        ipAddress.setText(globalVariable.getIp());

        pattern = Pattern.compile(IPADDRESS_PATTERN);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.settings_ip_edit;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        startService(intentBroadcastService);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.comment_edit, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send_comment:
                String newIpAddress = ipAddress.getText().toString();
                //Validation that the string looks like 0.0.0.0:8080
                if (validate(newIpAddress)) {
                    //Store to DB
                    globalVariable.getIpAddressMDAO().saveOrUpdateIpAddress(newIpAddress, globalVariable.getCurrentUser().getUserId());
                    globalVariable.setIp(newIpAddress);
                    onBackPressed();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //stopService(intentActiveConversation);
        globalVariable.setSelectedWallPost(null);
        startActivity(new Intent(ChangeIPActivity.this, MainActivity.class));
        finish();
    }

    public boolean validate(final String ip){
        matcher = pattern.matcher(ip);
        return matcher.matches();
    }
}

package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.LikeComp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class LikeMDAO {
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_LIKE_MDB_ID,
            RussSQLiteHelper.COLUMN_LIKE_ID,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID,
            RussSQLiteHelper.COLUMN_LIKE_FROM_USER_ID,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public LikeMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Like createLike(final Like like, final Long appOwnerId) {
        Like existedLike = getLike(like.getLikeId(), appOwnerId);
        if(existedLike == null) {
            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_LIKE_ID, like.getLikeId());
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, like.getWallMessageId());
            values.put(RussSQLiteHelper.COLUMN_LIKE_FROM_USER_ID, like.getFromUserId());
            values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
            long insertId = database.insert(RussSQLiteHelper.TABLE_LIKE, null, values);
            Cursor cursor = database.query(RussSQLiteHelper.TABLE_LIKE, allColumns, RussSQLiteHelper.COLUMN_LIKE_MDB_ID + " = " + insertId, null, null, null, null);
            cursor.moveToFirst();
            existedLike = cursorToLike(cursor);
            cursor.close();
        }
        return existedLike;
    }

    public void createLikes(final Set<Like> likes, final Long appOwnerId) {
        for (Like like: likes) {
            Like existedLike = getLike(like.getLikeId(), appOwnerId);
            if(existedLike == null) {
                ContentValues values = new ContentValues();
                values.put(RussSQLiteHelper.COLUMN_LIKE_ID, like.getLikeId());
                values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, like.getWallMessageId());
                values.put(RussSQLiteHelper.COLUMN_LIKE_FROM_USER_ID, like.getFromUserId());
                values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
                database.insert(RussSQLiteHelper.TABLE_LIKE, null, values);
            }
        }
    }

    public void deleteLike(final Like like, final Long appOwnerId) {
        Long id = like.getLikeMDBId();
        database.delete(RussSQLiteHelper.TABLE_LIKE, RussSQLiteHelper.COLUMN_LIKE_MDB_ID + " = " + id + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public void deleteLikeById(final Long likeId, final Long appOwnerId) {
        database.delete(RussSQLiteHelper.TABLE_LIKE, RussSQLiteHelper.COLUMN_LIKE_ID + " = " + likeId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public void deleteLikes(final Set<Like> likes, final Long appOwnerId) {
        for(Like like: likes) {
            deleteLikeById(like.getLikeId(), appOwnerId);
        }
    }

    public List<Like> getAllLikes() {
        List<Like> likeList = new ArrayList<Like>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_LIKE, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Like like = cursorToLike(cursor);
            likeList.add(like);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return likeList;
    }

    public Set<Like> getLikesByWallMessageId(final Long wallMessageId, final Long appOwnerId) {
        Set<Like> likeList = new TreeSet<Like>(new LikeComp());

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_LIKE, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallMessageId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Like like = cursorToLike(cursor);
            likeList.add(like);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return likeList;
    }

    public boolean isLikedThePost(final Long wallMessageId, final Long userId) {
        Set<Like> likeList = new TreeSet<Like>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_LIKE, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallMessageId + " AND " + RussSQLiteHelper.COLUMN_LIKE_FROM_USER_ID + " = " + userId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Like like = cursorToLike(cursor);
            likeList.add(like);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        if (likeList.size() == 1) {
            return true;
        }

        return false;
    }

    public Like getLike(final Long likeId, final Long appOwnerId) {
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_LIKE, allColumns, RussSQLiteHelper.COLUMN_LIKE_ID + " = " + likeId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);
        Like like = null;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            like = cursorToLike(cursor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return like;
    }

    private Like cursorToLike(Cursor cursor) {
        Like like = new Like();
        like.setLikeMDBId(cursor.getLong(0));
        like.setLikeId(cursor.getLong(1));
        like.setWallMessageId(cursor.getLong(2));
        like.setFromUserId(cursor.getLong(3));
        return like;
    }
}

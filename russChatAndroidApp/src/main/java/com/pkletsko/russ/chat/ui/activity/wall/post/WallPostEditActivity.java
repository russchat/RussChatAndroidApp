package com.pkletsko.russ.chat.ui.activity.wall.post;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class WallPostEditActivity extends BaseActivity {

    // LogCat tag
    private static final String TAG = WallPostEditActivity.class.getSimpleName();

    private ImageButton photoButton;
    private EditText inputMsg;

    private Long attachmentId = System.currentTimeMillis() % 1000;

    private static final int INTENT_REQUEST_GET_IMAGES = 13;

    ArrayList<Uri> image_uris = new ArrayList<Uri>();
    private ViewGroup mSelectedImagesContainer;

    private void getImages() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Post");

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {

            photoButton = (ImageButton) findViewById(R.id.photoButton);

            inputMsg = (EditText) findViewById(R.id.inputMsg);

            mSelectedImagesContainer = (ViewGroup) findViewById(R.id.selected_photos_container);

            photoButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    getImages();
                }
            });
        }

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.wall_post_edit_main;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //globalVariable.setSelectedWallPost(null);
        finish();
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        //startService(intentBroadcastService);
    }

    private void showMedia() {
        // Remove all views before
        // adding the new ones.
        mSelectedImagesContainer.removeAllViews();
        if (image_uris.size() >= 1) {
            mSelectedImagesContainer.setVisibility(View.VISIBLE);
        }

        int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());


        for (Uri uri : image_uris) {

            String imageName = getRandomStorageName();
            FileToUpload fileToUpload = new FileToUpload();
            fileToUpload.setFileName(imageName);
            fileToUpload.setFilePath(uri.getPath());
            fileToUpload.setAttachmentId(attachmentId);

            fileUpload(fileToUpload);


            View imageHolder = LayoutInflater.from(this).inflate(R.layout.image_item, null);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);

            Glide.with(this)
                    .load(uri.toString())
                    .fitCenter()
                    .into(thumbnail);

            mSelectedImagesContainer.addView(imageHolder);

            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_REQUEST_GET_IMAGES && resultCode == Activity.RESULT_OK) {

            image_uris = data.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
            if (image_uris != null) {
                showMedia();
            }
            //do something
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.comment_edit, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send_comment:
                // Sending message to web socket server

                if (isPostReadyToSend()) {
                    boolean imageAttachment = false;
                    Set<String> attachmentNames = new HashSet<String>();

                    //TODO save globalVariable.getQueueToUploadFiles() in case of app error to continue uploading
                    Set<FileToUpload> attachments = globalVariable.getQueueToUploadFiles().get(attachmentId);

                    if (attachments != null && attachments.size() > 0) {
                        imageAttachment = true;

                        for (FileToUpload fileToUpload : attachments) {
                            attachmentNames.add(fileToUpload.getFileName() + ".jpg");
                        }

                        saveUnloadedAttachmentsToMDB(attachments);
                    }

                    sendMessageToServer(utils.getSendWallMessageJSON(inputMsg.getText().toString(), globalVariable.getCurrentUser().getMyWallId(), imageAttachment, attachmentId, attachmentNames));
                    // Clearing the input filed once message was sent
                    inputMsg.setText("");
                    onBackPressed();
                } else {
                    Toast.makeText(getApplicationContext(), "You cannot send an empty post.",
                            Toast.LENGTH_LONG).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isPostReadyToSend() {
        final Set<FileToUpload> attachments = globalVariable.getQueueToUploadFiles().get(attachmentId);

        if (!inputMsg.getText().toString().trim().isEmpty() || (attachments != null && attachments.size() > 0)) {
            return true;
        }

        return false;
    }

    private void fileUpload(final FileToUpload fileToUpload) {
        try {

            Set<FileToUpload> queueToUploadFiles = globalVariable.getQueueToUploadFiles().get(attachmentId);
            if (queueToUploadFiles == null) {
                Set<FileToUpload> fileToUploadSet = new HashSet<FileToUpload>();
                fileToUploadSet.add(fileToUpload);
                globalVariable.getQueueToUploadFiles().put(attachmentId, fileToUploadSet);
            } else {
                globalVariable.getQueueToUploadFiles().get(attachmentId).add(fileToUpload);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getRandomStorageName() {
        return UUID.randomUUID().toString();
    }

    private void saveUnloadedAttachmentsToMDB(final Set<FileToUpload> attachments) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getUnloadedAttachmentMDAO().createFileToUploads(attachments, globalVariable.getCurrentUser().getUserId());
                globalVariable.getQueueToUploadFiles().remove(attachmentId);
                globalVariable.getQueueToUploadFiles().put(attachmentId, globalVariable.getUnloadedAttachmentMDAO().getFileToUploadsByAttachmentId(attachmentId, globalVariable.getCurrentUser().getUserId()));
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }
}
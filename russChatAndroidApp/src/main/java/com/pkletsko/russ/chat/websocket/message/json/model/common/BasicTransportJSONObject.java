package com.pkletsko.russ.chat.websocket.message.json.model.common;

public class BasicTransportJSONObject {

    private MessageObjectType objectType;

    public MessageObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(MessageObjectType objectType) {
        this.objectType = objectType;
    }
}

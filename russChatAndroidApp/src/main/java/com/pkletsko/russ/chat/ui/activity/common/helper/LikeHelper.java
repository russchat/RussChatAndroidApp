package com.pkletsko.russ.chat.ui.activity.common.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.pkletsko.russ.chat.websocket.message.json.model.wall.Like;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by pkletsko on 02.11.2015.
 */
public class LikeHelper {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int LIKE_LIST_SIZE_LIMIT = 36;
    public static final Long INIT_VALUE = 0L;
    public static final int FIRST_ITEM = 0;

    public static void notifyDataSetChanged(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public static Long getLikeId(final int index, ArrayList<Like> existedLikes) {
        return existedLikes.get(index).getLikeId();
    }

     /**
     * Appending message to list view
     */
    public static void appendLikes(final ArrayList<Like> existedLikes, Collection... likeCollections) {
        for (Collection likes : likeCollections) {
            existedLikes.addAll(likes);
        }
    }
}

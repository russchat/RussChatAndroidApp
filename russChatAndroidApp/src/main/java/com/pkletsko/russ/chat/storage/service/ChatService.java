package com.pkletsko.russ.chat.storage.service;

import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.UnReceivedChatMessages;

/**
 * Created by pkletsko on 25.05.2015.
 */
public interface ChatService {
    void saveMessageToMDB(final ChatMessage message);

    void saveConversationToMDB(final Chat newConversation);

    void storeUnReceivedChatMessagesToMDB(final UnReceivedChatMessages unReceivedChatMessages);

    void initializationOfApplicationData(final UnReceivedChatMessages unReceivedChatMessages);
}

package com.pkletsko.russ.chat.ui.activity.conversation.fragment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.ui.activity.chat.ChatActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;

import java.util.ArrayList;


public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<Chat> conversations;
    private GlobalClass globalVariable;
    private AssetManager assetManager;
    private WebSocketClient client;
    private RecyclerView recyclerView;


    private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    // LogCat tag
    private static final String TAG = ConversationAdapter.class.getSimpleName();

    public ConversationAdapter(Activity activity, ArrayList<Chat> conversations, GlobalClass globalVariable, RecyclerView recyclerView) {
        Log.d(TAG, "initialization of ChatRecyclerAdapter");
        this.recyclerView = recyclerView;
        this.assetManager = activity.getAssets();
        this.activity = activity;
        this.conversations = conversations;
        this.globalVariable = globalVariable;
        this.client = globalVariable.getClient();
    }

    @Override
    public ConversationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View vi = LayoutInflater.from(context).inflate(R.layout.conversation_item, parent, false);
        //UI initialization
        vi.setOnClickListener(mOnClickListener);
        ViewHolder viewHolder = new ViewHolder(vi);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        //font initialization
        Typeface userNameFont = Typeface.createFromAsset(assetManager, "Calibri Bold.ttf");
        Typeface postTextFont = Typeface.createFromAsset(assetManager, "Gidole-Regular.otf");

        Chat chat = conversations.get(position);
        Long conversationId = chat.getConversationId();
        Integer unReceivedMessagesNumber = globalVariable.getUnreadMessagesCash().get(conversationId);
        String conversationName = chat.getConversationName();

        viewHolder.conversation = globalVariable.getChatCash().get(conversationId);

        if (unReceivedMessagesNumber != null) {
            viewHolder.unReadMessagesNumberTextView.setText(unReceivedMessagesNumber.toString());
            viewHolder.unReadMessagesNumberTextView.setTypeface(postTextFont);
        }

        viewHolder.conversationNameTextView.setText(conversationName);
        viewHolder.conversationNameTextView.setTypeface(userNameFont);

        if (chat.getUsers() != null && chat.getUsers().size() > 2) {
            viewHolder.userIconImageView.setImageResource(R.drawable.group_chat);
        } else {
            globalVariable.getPicassoHelper().loadImageWithFit(chat.getConversationIcon(), viewHolder.userIconImageView, globalVariable.getPicassoHelper().getRoundedTransformation());
        }
    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return conversations.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView conversationNameTextView;
        public ImageView userIconImageView;
        public TextView unReadMessagesNumberTextView;
        public Chat conversation;

        public ViewHolder(View vi) {
            super(vi);

            conversationNameTextView =(TextView) vi.findViewById(R.id.conversationName);
            userIconImageView=(ImageView) vi.findViewById(R.id.icon);
            unReadMessagesNumberTextView=(TextView) vi.findViewById(R.id.unReadMessagesNumber);
        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            int itemPosition = recyclerView.getChildLayoutPosition(view);
            globalVariable.setActiveConversation(conversations.get(itemPosition));
            Intent i = new Intent(activity.getApplicationContext(), ChatActivity.class);
            activity.startActivity(i);
        }
    }
}
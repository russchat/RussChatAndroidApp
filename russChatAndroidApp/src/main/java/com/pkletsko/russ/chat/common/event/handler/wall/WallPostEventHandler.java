package com.pkletsko.russ.chat.common.event.handler.wall;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.storage.service.WallPostService;
import com.pkletsko.russ.chat.storage.service.WallPostServiceImpl;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.FileToUpload;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedWallPosts;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnloadedAttachmentWallPostLink;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pkletsko on 26.05.2015.
 */
public class WallPostEventHandler {

    private static final String TAG = WallPostEventHandler.class.getSimpleName();

    private UserService userService;

    private WallPostService wallPostService;

    private GlobalClass globalVariable;

    private Utils utils;

    private Context context;

    private Intent intentMyWall;
    private Intent intentWall;

    public WallPostEventHandler(final GlobalClass globalVariable, final Utils utils, final Context context) {
        this.globalVariable = globalVariable;
        this.utils = utils;
        this.context = context;

        userService = new UserServiceImpl(globalVariable, utils);

        wallPostService = new WallPostServiceImpl(globalVariable, userService, context);

        intentMyWall = new Intent(EventHub.BROADCAST_ACTIVE_MY_WALL_UPDATE);
        intentWall = new Intent(EventHub.BROADCAST_ACTIVE_WALL_UPDATE);

    }

    public void unreceivedLikesEventHandler(final String msg) {
        if (globalVariable.getCurrentUser() != null) {
            // async get data from database and after set data to global variable and than call redirect to login activity
            UnReceivedWallPosts unReceivedWallPosts = utils.getUnReceivedWallMessages(msg);
            if (unReceivedWallPosts == null) {
                showToast("Something went wrong. Bad response from server.");
            } else {
                wallPostService.initializationOfWall(unReceivedWallPosts);
            }
        }
    }
    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    public void wallPostEventHandler(final String msg) {
        Log.d(TAG, "wallPostEventHandler: " + msg);

        final WallPost wallPost = utils.getWallMessageUI(msg);
        if (wallPost == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            wallPost.setReceived(true);

            wallPostService.putToWallPostCash(wallPost);

            wallPostService.saveWallPostToMDB(wallPost, true);

            if (globalVariable.getCurrentUser().getUserId() == wallPost.getFromUserId()) {

                // wall Post with attachment (this is )
                if (wallPost.isImageAttached()) {
                    final Set<FileToUpload> attachments = globalVariable.getQueueToUploadFiles().get(wallPost.getAttachmentId());
                    if (attachments != null) {

                        //show local images till they are loading to server
                        globalVariable.getWallPostLocalImages().put(wallPost.getWallMessageId(), new HashSet<>(attachments));

                        globalVariable.getAttachmentOfWallPostQueueCash().put(wallPost.getAttachmentId(), wallPost.getWallMessageId());

                        //save it to database in case error in appliation and upload will be unsuccessful
                        final UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink = new UnloadedAttachmentWallPostLink();
                        unloadedAttachmentWallPostLink.setAttachmentId(wallPost.getAttachmentId());
                        unloadedAttachmentWallPostLink.setWallPostId(wallPost.getWallMessageId());

                        wallPostService.saveUnloadedAttachmentWallPostLinkToMDB(unloadedAttachmentWallPostLink);

                        for (FileToUpload file : attachments) {
                            wallPostService.uploadFileToServer(file);
                        }
                    }
                }

                globalVariable.getMyWallPostCash().add(wallPost);

                intentMyWall.putExtra("message", msg);
                context.sendBroadcast(intentMyWall);
            } else {
                //check if this user in db
                //if not load profile and add it and to cash
                userService.userChecking(wallPost.getFromUserId());

                globalVariable.getFollowedWallPostCash().add(wallPost);

                intentWall.putExtra("message", msg);
                context.sendBroadcast(intentWall);
            }
        }
    }
}

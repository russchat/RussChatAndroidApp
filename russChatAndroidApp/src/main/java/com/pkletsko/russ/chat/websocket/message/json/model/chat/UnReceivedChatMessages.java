package com.pkletsko.russ.chat.websocket.message.json.model.chat;


import java.util.Set;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class UnReceivedChatMessages extends BasicTransportJSONObject implements JSONResponse {

    private Set<ChatMessage> chatMessages;

    private Set<Chat> chats;

    public Set<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(Set<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public Set<Chat> getChats() {
        return chats;
    }

    public void setChats(Set<Chat> chats) {
        this.chats = chats;
    }
}

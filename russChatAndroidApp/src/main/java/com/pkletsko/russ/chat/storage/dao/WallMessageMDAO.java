package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPostComp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class WallMessageMDAO {

    // LogCat tag
    private static final String TAG = WallMessageMDAO.class.getSimpleName();

    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_WALL_MESSAGE_MDB_ID,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_TEXT,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID,
            RussSQLiteHelper.COLUMN_WALL_ID,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_FROM_USER_ID,
            RussSQLiteHelper.COLUMN_LIKE_NUMBER,
            RussSQLiteHelper.COLUMN_COMMENT_NUMBER,
            RussSQLiteHelper.COLUMN_LIKED_BY_ME,
            RussSQLiteHelper.COLUMN_WALL_POST_ATTACHMENT,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID,
            RussSQLiteHelper.COLUMN_WALL_POST_RECEIVED,
            RussSQLiteHelper.COLUMN_WALL_POST_CREATED_DATE_TIME};
    private SimpleDateFormat format;

    public WallMessageMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        format = new SimpleDateFormat("dd MMM yyyy hh:mm");


    }

    public void close() {
        dbHelper.close();
    }

    public WallPost createWallPost(final WallPost wallMessage, final Long appOwnerId) {
        WallPost newWallPost = null;

        WallPost existedWallPost = getWallPostByWallId(wallMessage.getWallMessageId(), appOwnerId);

        if (existedWallPost == null) {

            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_TEXT, wallMessage.getMessageText());
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, wallMessage.getWallMessageId());
            values.put(RussSQLiteHelper.COLUMN_WALL_ID, wallMessage.getWallId());
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_FROM_USER_ID, wallMessage.getFromUserId());
            values.put(RussSQLiteHelper.COLUMN_LIKE_NUMBER, wallMessage.getLikeNumber());
            values.put(RussSQLiteHelper.COLUMN_COMMENT_NUMBER, wallMessage.getCommentNumber());
            values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);

            int liked = 0;
            if (wallMessage.isLiked()) {
                liked = 1;
            }

            values.put(RussSQLiteHelper.COLUMN_LIKED_BY_ME, liked);

            int attached = 0;
            if (wallMessage.isImageAttached()) {
                attached = 1;
            }

            values.put(RussSQLiteHelper.COLUMN_WALL_POST_ATTACHMENT, attached);

            int received = 0;
            if (wallMessage.isReceived()) {
                received = 1;
            }
            values.put(RussSQLiteHelper.COLUMN_WALL_POST_RECEIVED, received);

            final Date createdDate = wallMessage.getCreated();
            //2 December 12:00 PM

            if (createdDate != null) {
                String dateToStr = format.format(createdDate);
                values.put(RussSQLiteHelper.COLUMN_WALL_POST_CREATED_DATE_TIME, dateToStr);
            }

            long insertId = database.insert(RussSQLiteHelper.TABLE_WALL_MESSAGE, null, values);
            Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_MDB_ID + " = " + insertId, null, null, null, null);
            cursor.moveToFirst();
            newWallPost = cursorToWallMessageUI(cursor);
            cursor.close();
        } else if (!existedWallPost.isReceived() && wallMessage.isReceived()) {
            //this is a case when we loaded post and now we received it , so I should update this post with received flag and new data
            existedWallPost.setReceived(true);
            newWallPost = updateWallPost(existedWallPost);
        }

        return newWallPost;
    }

    public void createWallPosts(final Set<WallPost> wallPosts, final Long appOwnerId) {
        for (WallPost wallPost: wallPosts) {
            createWallPost(wallPost, appOwnerId);
        }
    }

    public WallPost updateWallPost(WallPost wallMessage) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_TEXT, wallMessage.getMessageText());
        values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, wallMessage.getWallMessageId());
        values.put(RussSQLiteHelper.COLUMN_WALL_ID, wallMessage.getWallId());
        values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_FROM_USER_ID, wallMessage.getFromUserId());
        values.put(RussSQLiteHelper.COLUMN_LIKE_NUMBER, wallMessage.getLikeNumber());
        values.put(RussSQLiteHelper.COLUMN_COMMENT_NUMBER, wallMessage.getCommentNumber());

        int liked = 0;
        if (wallMessage.isLiked()) {
            liked = 1;
        }

        values.put(RussSQLiteHelper.COLUMN_LIKED_BY_ME, liked);

        int attached = 0;
        if (wallMessage.isImageAttached()) {
            attached = 1;
        }

        values.put(RussSQLiteHelper.COLUMN_WALL_POST_ATTACHMENT, attached);

        int received = 0;
        if (wallMessage.isReceived()) {
            received = 1;
        }
        values.put(RussSQLiteHelper.COLUMN_WALL_POST_RECEIVED, received);

        final Date createdDate = wallMessage.getCreated();
        //2 December 12:00 PM

        if (createdDate != null) {
            String dateToStr = format.format(createdDate);
            values.put(RussSQLiteHelper.COLUMN_WALL_POST_CREATED_DATE_TIME, dateToStr);
        }

        long result = database.update(RussSQLiteHelper.TABLE_WALL_MESSAGE, values, RussSQLiteHelper.COLUMN_WALL_MESSAGE_MDB_ID + " = " + wallMessage.getWallMessageMDBId(), null);

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_MDB_ID + " = " + wallMessage.getWallMessageMDBId(), null, null, null, null);
        cursor.moveToFirst();
        WallPost newWallPost = cursorToWallMessageUI(cursor);
        cursor.close();
        return newWallPost;
    }

    public void deleteWallPostByMDBId(final WallPost wallMessage, final Long appOwnerId) {
        long id = wallMessage.getWallMessageMDBId();
        database.delete(RussSQLiteHelper.TABLE_WALL_MESSAGE, RussSQLiteHelper.COLUMN_WALL_MESSAGE_MDB_ID + " = " + id + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public void deleteWallPost(final Long wallPostId, final Long appOwnerId) {
        database.delete(RussSQLiteHelper.TABLE_WALL_MESSAGE, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallPostId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public List<WallPost> getAllWallMessageUIs() {
        List<WallPost> wallMessageList = new ArrayList<WallPost>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WallPost wallMessage = cursorToWallMessageUI(cursor);
            wallMessageList.add(wallMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return wallMessageList;
    }

    public Set<WallPost> getWallMessageUIsByWallId(final Long wallId) {
        Set<WallPost> wallMessageList = new TreeSet<WallPost>( new WallPostComp());

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_WALL_ID + " = " + wallId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WallPost wallMessage = cursorToWallMessageUI(cursor);
            wallMessageList.add(wallMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return wallMessageList;
    }

    public Set<WallPost> getWallPostsByAppOwnerId(final Long appOwnerId) {
        Set<WallPost> wallMessageList = new TreeSet<WallPost>( new WallPostComp());

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WallPost wallMessage = cursorToWallMessageUI(cursor);
            wallMessageList.add(wallMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return wallMessageList;
    }

    public WallPost getWallPostByWallId(final Long wallPostId, final Long appOwnerId) {
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallPostId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);
        WallPost newWallPost = null;
        try {
            if (cursor.moveToFirst()) {
                newWallPost = cursorToWallMessageUI(cursor);
                // do your stuff
            } else {
                // cursor is empty
            }
        }finally {
            cursor.close();
        }

        return newWallPost;
    }

    public Set<WallPost> getWallMessageUIsExcludeWallId(final Long wallId) {
        Set<WallPost> wallMessageList = new TreeSet<WallPost>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, RussSQLiteHelper.COLUMN_WALL_ID + " <> " + wallId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WallPost wallMessage = cursorToWallMessageUI(cursor);
            wallMessageList.add(wallMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return wallMessageList;
    }

    public Set<WallPost> getFollowedWallMessageUIs(final List<Long> followedWallIds) {
        Set<WallPost> wallMessageList = new TreeSet<WallPost>(new WallPostComp());

        String sqlCondition = "";
        int count = 0;
        for (Long wallId: followedWallIds) {
            count++;
            sqlCondition+= RussSQLiteHelper.COLUMN_WALL_ID + " == " + wallId;
            if (followedWallIds.size() != count) {
                sqlCondition+=" OR ";
            }
        }

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_WALL_MESSAGE, allColumns, sqlCondition , null, null, null, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID +" DESC");

        cursor.moveToFirst();

        int i = 0;

        while (!cursor.isAfterLast()) {
            WallPost wallMessage = cursorToWallMessageUI(cursor);

            Log.d(TAG, "wall message #" + i + " , message id = " + wallMessage.getWallMessageId() + ", text = " + wallMessage.getMessageText());
            i++;

            wallMessageList.add(wallMessage);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return wallMessageList;
    }

    private WallPost cursorToWallMessageUI(Cursor cursor) {
        WallPost wallPost = new WallPost();
        wallPost.setWallMessageMDBId(cursor.getLong(0));
        wallPost.setMessageText(cursor.getString(1));
        wallPost.setWallMessageId(cursor.getLong(2));
        wallPost.setWallId(cursor.getLong(3));
        wallPost.setFromUserId(cursor.getLong(4));

        wallPost.setLikeNumber(cursor.getInt(5));
        wallPost.setCommentNumber(cursor.getInt(6));

        boolean liked = false;
        if (cursor.getInt(7) == 1) {
            liked = true;
        }

        wallPost.setLiked(liked);

        boolean attached = false;
        if (cursor.getInt(8) == 1) {
            attached = true;
        }

        wallPost.setImageAttached(attached);

        boolean received = false;
        if (cursor.getInt(10) == 1) {
            received = true;
        }

        wallPost.setReceived(received);

        try {
            String created = cursor.getString(11);
            if (created != null && created.length() > 5) {
                Date date = format.parse(cursor.getString(11));
                wallPost.setCreated(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return wallPost;
    }
}

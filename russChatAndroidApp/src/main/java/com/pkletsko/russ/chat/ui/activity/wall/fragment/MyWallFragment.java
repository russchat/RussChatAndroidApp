package com.pkletsko.russ.chat.ui.activity.wall.fragment;

import android.app.Activity;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.common.tab.TabEnum;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.base.BaseWallPostFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.appendWallPost;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.parseMessage;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.putToCash;


public class MyWallFragment extends BaseWallPostFragment {

    private static final String TAG = MyWallFragment.class.getSimpleName();

    @Override
    protected Set<WallPost> getCash() {
        return globalVariable.getMyWallPostCash();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    protected Map<String, String> getRequestParameters() {
        final Map<String,String> requestParameters = new HashMap<>();
        requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + GlobalClass.CONFIG_GET_USER_WALL_POSTS_URL);
        requestParameters.put("id", globalVariable.getCurrentUser().getUserId().toString());
        return requestParameters;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        if (visible) {
            if (globalVariable != null) {
                globalVariable.setActiveTab(TabEnum.TAB_MY_WALL);
            }
        }
        super.setMenuVisibility(visible);
    }

    @Override
    protected void removeWallPost(ArrayList<WallPost> clearedWallPosts, final int index){
        final Long wallPostId = clearedWallPosts.get(index).getWallMessageId();
        wallPostService.deleteWallPostFromMDB(wallPostId);
        removeWallPostFromCash(wallPostId);
        globalVariable.getMyWallPostCash().remove(clearedWallPosts.get(index));
        clearedWallPosts.remove(index);
    }

    @Override
    protected void saveWallPost(final WallPost wallPost) {
        wallPost.setReceived(true);
        putToCash(wallPost, globalVariable.getWallPostCash(), globalVariable.getMyWallPostCash());
        wallPostService.saveWallPostToMDB(wallPost, true);
    }

    @Override
    protected void broadcastMyWallPostReceiverHandler(String broadcastedMessage) {
        WallPost wallPost = parseMessage(broadcastedMessage, utils);
        if (wallPost == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            appendWallPost(wallPost, wallPosts);
        }
    }
}

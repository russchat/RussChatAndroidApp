package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class UserMDAO {
    // LogCat tag
    private static final String TAG = UserMDAO.class.getSimpleName();
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = {RussSQLiteHelper.COLUMN_USER_MDB_ID,
            RussSQLiteHelper.COLUMN_USER_ID,
            RussSQLiteHelper.COLUMN_USER_PROFILE_VERSION,
            RussSQLiteHelper.COLUMN_USER_NAME,
            RussSQLiteHelper.COLUMN_USER_PROFILE_ICON,
            RussSQLiteHelper.COLUMN_USER_CURRENT_FLAG,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID,
            RussSQLiteHelper.COLUMN_USER_LOGIN_SRC,
            RussSQLiteHelper.COLUMN_USER_FOLLOW};

    public UserMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public UserProfile saveOrUpdateUser(final UserProfile newUserProfile, final Long appOwnerId) {
        Log.d(TAG, "saveOrUpdateUser: newUserProfile = " + newUserProfile);

        final UserProfile existedUserProfile = getUserByIdAndAppOwner(newUserProfile.getUserId(), appOwnerId);

        if (existedUserProfile == null) {
            return createUser(newUserProfile, appOwnerId);
        } else if (existedUserProfile.getVersion() < newUserProfile.getVersion()) {
            newUserProfile.setUserMDBId(existedUserProfile.getUserMDBId());
            updateUser(newUserProfile);
        } else {
            Log.d(TAG, "saveOrUpdateUser: THIS USER EXIST AND UPDATED ALREADY userProfile = " + newUserProfile);
        }

        return newUserProfile;
    }

    private UserProfile createUser(final UserProfile userProfile, final Long appOwnerId) {

        Log.d(TAG, "createUser: userProfile = " + userProfile);

        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_USER_ID, userProfile.getUserId());
        values.put(RussSQLiteHelper.COLUMN_USER_PROFILE_VERSION, userProfile.getVersion());
        values.put(RussSQLiteHelper.COLUMN_USER_NAME, userProfile.getFullName());
        values.put(RussSQLiteHelper.COLUMN_USER_PROFILE_ICON, userProfile.getProfileIcon());
        values.put(RussSQLiteHelper.COLUMN_USER_CURRENT_FLAG, userProfile.getLoggedInUser());
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
        values.put(RussSQLiteHelper.COLUMN_USER_LOGIN_SRC, userProfile.getLoginSrc());

        int followed = 0;
        if (userProfile.isFollowed()) {
            followed = 1;
        }

        values.put(RussSQLiteHelper.COLUMN_USER_FOLLOW, followed);

        long insertId = database.insert(RussSQLiteHelper.TABLE_USER, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_USER, allColumns, RussSQLiteHelper.COLUMN_USER_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();

        UserProfile newUserProfile = cursorToUserProfile(cursor);

        cursor.close();

        return newUserProfile;
    }

    private void updateUser(final UserProfile userProfile) {

        Log.d(TAG, "updateUser: userProfile = " + userProfile);

        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_USER_ID, userProfile.getUserId());
        values.put(RussSQLiteHelper.COLUMN_USER_PROFILE_VERSION, userProfile.getVersion());
        values.put(RussSQLiteHelper.COLUMN_USER_NAME, userProfile.getFullName());
        values.put(RussSQLiteHelper.COLUMN_USER_PROFILE_ICON, userProfile.getProfileIcon());

        int followed = 0;
        if (userProfile.isFollowed()) {
            followed = 1;
        }

        values.put(RussSQLiteHelper.COLUMN_USER_FOLLOW, followed);

        database.update(RussSQLiteHelper.TABLE_USER, values, RussSQLiteHelper.COLUMN_USER_MDB_ID + " = " + userProfile.getUserMDBId(), null);
    }

    public void removeCurrentUser(UserProfile userProfile) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_USER_CURRENT_FLAG, 0);

        //database.update(RussSQLiteHelper.TABLE_USER, values, RussSQLiteHelper.COLUMN_USER_ID + " = " + userProfile.getUserId() + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + userProfile.getUserId(), null);
        database.delete(RussSQLiteHelper.TABLE_USER, RussSQLiteHelper.COLUMN_USER_ID + " = " + userProfile.getUserId() + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + userProfile.getUserId(), null);
    }

    private void deleteAll() {
        database.execSQL("delete from " + RussSQLiteHelper.TABLE_USER);
    }

    private void deleteUser(UserProfile userProfile) {
        long id = userProfile.getUserMDBId();
        database.delete(RussSQLiteHelper.TABLE_USER, RussSQLiteHelper.COLUMN_USER_MDB_ID + " = " + id, null);
    }

    public Set<UserProfile> getAllUsersByAppOwnerId(final Long appOwnerId) {
        Log.d(TAG, "1) getAllUsersByAppOwnerId: appOwnerId = " + appOwnerId);

        Set<UserProfile> userProfiles = new HashSet<>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_USER, allColumns, RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UserProfile conversation = cursorToUserProfile(cursor);
            userProfiles.add(conversation);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        Log.d(TAG, "2) getAllUsersByAppOwnerId: size = " + userProfiles.size());

        return userProfiles;
    }

    public UserProfile getUserByIdAndAppOwner(final Long serverUserId, final Long appOwnerId) {
        Log.d(TAG, "1) getUserByIdAndAppOwner: serverUserId = " + serverUserId + " appOwnerId = " + appOwnerId);

        UserProfile userProfile = null;
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_USER, allColumns, RussSQLiteHelper.COLUMN_USER_ID + " = " + serverUserId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        if (cursor == null) {
            Log.e(TAG, "getUserByIdAndAppOwner: cursor is null");
            return null;
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            userProfile = cursorToUserProfile(cursor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        if (userProfile != null) {
            Log.d(TAG, "2) getUserByIdAndAppOwner: userName = " + userProfile.getFullName());
        }
        return userProfile;
    }


    public UserProfile getCurrentUser() {
        Log.d(TAG, "1) getCurrentUser");

        UserProfile conversationMDTO = null;
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_USER, allColumns, RussSQLiteHelper.COLUMN_USER_CURRENT_FLAG + " = " + 1, null, null, null, null);

        if (cursor == null) {
            Log.e(TAG, "getCurrentUser: cursor is null");
            return null;
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            conversationMDTO = cursorToUserProfile(cursor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        if (conversationMDTO != null) {
            Log.d(TAG, "2) getCurrentUser: userName = " + conversationMDTO.getFullName());
        }
        return conversationMDTO;
    }

    private UserProfile cursorToUserProfile(Cursor cursor) {

        //Log.d(TAG, "cursorToUserProfile: cursor = " + cursor);

        UserProfile userProfile = new UserProfile();
        userProfile.setUserMDBId(cursor.getLong(0));
        userProfile.setUserId(cursor.getLong(1));
        userProfile.setVersion(cursor.getInt(2));
        userProfile.setFullName(cursor.getString(3));
        userProfile.setProfileIcon(cursor.getString(4));
        userProfile.setLoggedInUser(cursor.getInt(5));
        userProfile.setLoginSrc(cursor.getInt(7));

        boolean followed = false;
        if (cursor.getInt(8) == 1) {
            followed = true;
        }

        userProfile.setFollowed(followed);

        Log.d(TAG, "cursorToUserProfile: RESULT userId = " + userProfile.getUserId() + ", version = " + userProfile.getVersion());

        return userProfile;
    }
}

package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnloadedAttachmentWallPostLink;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class UnloadedAttachmentWallPostLinkMDAO {
    // LogCat tag
    private static final String TAG = UnloadedAttachmentWallPostLinkMDAO.class.getSimpleName();
    // Database fields

    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_WALLPOST_LINK_MDB_ID,
            RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public UnloadedAttachmentWallPostLinkMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public UnloadedAttachmentWallPostLink createUnloadedAttachmentWallPostLink(final UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID, unloadedAttachmentWallPostLink.getAttachmentId());
        values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, unloadedAttachmentWallPostLink.getWallPostId());
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);

        long insertId = database.insert(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK, allColumns, RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_WALLPOST_LINK_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        UnloadedAttachmentWallPostLink newUnloadedAttachmentWallPostLink = cursorToUnloadedAttachmentWallPostLink(cursor);
        cursor.close();
        return newUnloadedAttachmentWallPostLink;
    }

    public void createUnloadedAttachmentWallPostLinks(final Set<UnloadedAttachmentWallPostLink> unloadedAttachmentWallPostLinks, final Long appOwnerId) {
        for (UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink: unloadedAttachmentWallPostLinks) {
            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID, unloadedAttachmentWallPostLink.getAttachmentId());
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, unloadedAttachmentWallPostLink.getWallPostId());
            values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);

            database.insert(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK, null, values);
        }
    }

    public void deleteUnloadedAttachmentWallPostLink(final Long attachmentId, final Long appOwnerId) {
        Log.d(TAG, "deleteUnloadedAttachmentWallPostLink: attachmentId = " + attachmentId );
        database.delete(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK, RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID + " = " + attachmentId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null);
    }

    public List<UnloadedAttachmentWallPostLink> getAllUnloadedAttachmentWallPostLinks(final Long appOwnerId) {
        Log.d(TAG, "getAllUnloadedAttachmentWallPostLinks: Start");
        List<UnloadedAttachmentWallPostLink> unloadedAttachmentWallPostLinkList = new ArrayList<UnloadedAttachmentWallPostLink>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK, allColumns,RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        Log.d(TAG, "getAllUnloadedAttachmentWallPostLinks: cursor = " + cursor);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink = cursorToUnloadedAttachmentWallPostLink(cursor);
            unloadedAttachmentWallPostLinkList.add(unloadedAttachmentWallPostLink);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return unloadedAttachmentWallPostLinkList;
    }

    public Set<UnloadedAttachmentWallPostLink> getUnloadedAttachmentWallPostLinksByAttachmentId(final Long wallMessageId) {
        Set<UnloadedAttachmentWallPostLink> unloadedAttachmentWallPostLinkList = new TreeSet<UnloadedAttachmentWallPostLink>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK, allColumns, RussSQLiteHelper.COLUMN_UNLOADED_ATTACHMENT_ID + " = " + wallMessageId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UnloadedAttachmentWallPostLink unloadedAttachmentWallPostLink = cursorToUnloadedAttachmentWallPostLink(cursor);
            unloadedAttachmentWallPostLinkList.add(unloadedAttachmentWallPostLink);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return unloadedAttachmentWallPostLinkList;
    }

    private UnloadedAttachmentWallPostLink cursorToUnloadedAttachmentWallPostLink(Cursor cursor) {
        UnloadedAttachmentWallPostLink link = new UnloadedAttachmentWallPostLink();
        link.setUnloadedAttachmentWallPostLinkMDBId(cursor.getLong(0));
        link.setAttachmentId(cursor.getLong(1));
        link.setWallPostId(cursor.getLong(2));
        return link;
    }
}

package com.pkletsko.russ.chat.ui.activity.common.helper;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ConversationComp;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.NewChat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 02.11.2015.
 */
public class ConversationHelper {

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int CONVERSATIONS_SIZE_LIMIT = 36;
    public static final Long INIT_VALUE = 0L;
    public static final int FIRST_ITEM = 0;

    public static void notifyDataSetChanged(final Activity activity, final RecyclerView.Adapter adapter) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public static NewChat parseMessage(final String msg, final Utils utils) {
        return utils.getNewConversationResponse(msg);
//        try {
//            JSONObject jObj = new JSONObject(msg);
//
//            MessageObjectType objectType = MessageObjectType.findByCode(jObj.getInt("objectType"));
//
//            if (objectType != null && objectType.equals(MOT_CREATE_NEW_CONVERSATIONS)) {
//                // Appending the message to chat list
//                return utils.getNewConversationResponse(msg);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    /**
     * Appending message to list view
     */
    public static void appendConversations(final ArrayList<Chat> existedConversation, Collection... conversationCollections) {
        for (Collection conversation : conversationCollections) {
            existedConversation.addAll(conversation);
        }

        Set<Chat> conversationCash = new TreeSet<>(new ConversationComp());
        conversationCash.addAll(existedConversation);
        existedConversation.clear();
        existedConversation.addAll(conversationCash);
    }

    public static void appendConversation(final Chat newConversation, final ArrayList<Chat> existedConversations) {
        existedConversations.add(0, newConversation);
    }

    public static Long getConversationId(final int index, ArrayList<Chat> existedConversation) {
        return existedConversation.get(index).getConversationId();
    }

    @SuppressWarnings("unchecked")
    public static void putToCash(final Chat conversation, Object... cashes) {
        for (Object cash : cashes) {
            if (cash instanceof Map<?,?>){
                ((Map<Long, Chat>) cash).put(conversation.getConversationId(), conversation);
            } else if (cash instanceof Set<?>) {
                ((Set<Chat>) cash).add(conversation);
            }
        }
    }
}

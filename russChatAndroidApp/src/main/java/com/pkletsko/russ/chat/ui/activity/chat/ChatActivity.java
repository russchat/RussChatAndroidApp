package com.pkletsko.russ.chat.ui.activity.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.chat.adapter.ChatMessageAdapter;
import com.pkletsko.russ.chat.ui.activity.common.activity.MainActivity;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivityTest;
import com.pkletsko.russ.chat.ui.activity.common.view.EndlessRecyclerOnScrollListener;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.CHAT_MESSAGE_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.INIT_VALUE;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.appendChatMessage;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.appendChatMessages;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.getChatMessageId;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.parseMessage;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.DOWN;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.FIRST_ITEM;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.UP;
import static com.pkletsko.russ.chat.ui.activity.common.helper.ChatHelper.notifyDataSetChanged;

public class ChatActivity extends BaseActivityTest {

    private static final String TAG = ChatActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    
    private ChatMessageAdapter chatMessageAdapter;
    
    private ArrayList<ChatMessage> chatMessages = new ArrayList<>();
    
    private Long currentConversationId;
    
    @Override
    protected int getLayoutResourceId() {
        return R.layout.chat_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {

            setTitle(globalVariable.getActiveConversation().getConversationName());

            activity = this;

            currentConversationId = globalVariable.getActiveConversation().getConversationId();

            swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

            mRecyclerView = (RecyclerView) findViewById(R.id.chatMsgRecyclerView);

            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

            mRecyclerView.setLayoutManager(mLayoutManager);

            chatMessageAdapter = new ChatMessageAdapter(this, chatMessages, globalVariable, mRecyclerView);

            mRecyclerView.setAdapter(chatMessageAdapter);

            mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
                    mLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    loadMoreDown();
                }
            });

            // Setup refresh listener which triggers new data loading
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    // Make sure you call swipeContainer.setRefreshing(false)
                    // once the network request has completed successfully.
                    loadMoreUp();
                }
            });

            // Configure the refreshing colors
            swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

            final ImageButton btnSend = (ImageButton) findViewById(R.id.btnSend);
            final EditText inputMsg = (EditText) findViewById(R.id.inputMsg);

            btnSend.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (globalVariable.isAppRunning()) {
                        // Sending message to web socket server
                        sendMessageToServer(utils.getSendMessageJSON(globalVariable.getCurrentUser(), inputMsg.getText().toString(), globalVariable.getActiveConversation().getConversationId()));
                        // Clearing the input filed once message was sent
                        inputMsg.setText("");
                    } else {
                        Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            globalVariable.getUnreadMessagesCash().remove(currentConversationId);

            final Set<ChatMessage> messagesCashSet = globalVariable.getMessageCash().get(currentConversationId);

            if (messagesCashSet == null) {
                initializationOfChatData();
            } else {
                appendChatMessages(chatMessages, messagesCashSet);
                notifyDataSetChanged(activity, chatMessageAdapter);
            }
        }
    }

    protected void loadMoreUp() {
        if (chatMessages.isEmpty()) {
            // 5 Case
            loadFromServer(INIT_VALUE);
        } else {
            // 6 Case
            loadFromServer(getChatMessageId(FIRST_ITEM, chatMessages), UP);
        }
    }

    protected void loadMoreDown() {
        if (chatMessages.size() > 0) {
            Log.d(TAG, "LoadMore userProfiles size = " + chatMessages.size() + ", totalItemsCount = " + chatMessages.size() + " , wallPostId = " + chatMessages.get(chatMessages.size() - 1).getMessageId());
            // 4 Case
            loadFromServer(chatMessages.get(chatMessages.size() - 1).getMessageId());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(intentBroadcastService);
        registerReceiver(broadcastReceiver, new IntentFilter(EventHub.BROADCAST_ACTIVE_CONVERSATION_UPDATE));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void goBack() {
        globalVariable.setActiveConversation(null);
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("FirstTab", 4);
        startActivity(i);
    }

    private void initializationOfChatData() {
        AsyncTask<Void, Void, Set<ChatMessage>> task = new AsyncTask<Void, Void, Set<ChatMessage>>() {
            @Override
            protected Set<ChatMessage> doInBackground(Void... params) {
                return globalVariable.getMessageMDAO().getChatMessagesByConversationId(currentConversationId, currentUserId);
            }

            @Override
            protected void onPostExecute(Set<ChatMessage> existedChatMessages) {
                Log.i(TAG, "initializationOfChatMessagesData onPostExecute: existedChatMessages size = " + existedChatMessages.size());

                if (existedChatMessages.isEmpty()){
                    loadFromServer(INIT_VALUE);
                } else {
                    //TODO do we need chat messages in cash
                    globalVariable.getMessageCash().put(currentConversationId, existedChatMessages);

                    appendChatMessages(chatMessages, existedChatMessages);
                    notifyDataSetChanged(activity, chatMessageAdapter);

                    if (chatMessages.size() < CHAT_MESSAGE_SIZE_LIMIT) {
                        loadFromServer(chatMessages.get(chatMessages.size() - 1).getMessageId());
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }
    
    /**
     * load received wall posts from server
     * */
    protected void loadFromServer(final Long lastLoadedChatMessageId) {
        loadFromServer(lastLoadedChatMessageId, DOWN);
    }

    /**
     * load received wall posts from server with direction
     * */
    protected void loadFromServer(final Long lastLoadedChatMessageId, final int direction) {
        if (globalVariable.isAppRunning()) {
        final Map<String,String> requestParameters = new HashMap<>();
        requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_CONVERSATION_MESSAGES_URL);
        requestParameters.put("token", globalVariable.getServerToken());
        requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
        requestParameters.put("id", currentConversationId.toString());
        requestParameters.put("chatMessageId", String.valueOf(lastLoadedChatMessageId));
        requestParameters.put("direction", String.valueOf(direction));

        (new RequestUtil() {
            @Override
            public void onPostRequestResponse(String result) {
                if (result != null && !result.isEmpty()) {
                    Log.d(TAG, "loadFromServer result = " + result);

                    final ChatMessages chatMessagesObj = utils.getChatMessages(result);
                    if (chatMessagesObj == null) {
                        showToast("Something went wrong. Bad response from server.");
                    } else {
                        final Set<ChatMessage> receivedChatMessages = chatMessagesObj.getChatMessages();

                        final int currentChatMessagesSize = chatMessages.size();
                        final int totalSize = currentChatMessagesSize + receivedChatMessages.size();

                        //we need user to show name and icon (we need them temporary)
                        for (UserProfile userProfile : chatMessagesObj.getUserProfiles()) {
                            //TODO check if this is not overwrite more extended userProfile object existed in cash
                            globalVariable.getUsersCash().put(userProfile.getUserId(), userProfile);
                        }

                        for (final ChatMessage chatMessage : receivedChatMessages) {
                            // save chatMessage in cash and mdb
                            saveChatMessage(chatMessage);
                        }

                        if (globalVariable.getMessageCash().get(currentConversationId) != null) {
                            globalVariable.getMessageCash().get(currentConversationId).addAll(chatMessages);
                        } else {
                            globalVariable.getMessageCash().put(currentConversationId, new HashSet<>(chatMessages));
                        }

                        if (totalSize > CHAT_MESSAGE_SIZE_LIMIT) {
                            //remove items according to limit

                            final int removeItemsNumber = totalSize - CHAT_MESSAGE_SIZE_LIMIT;
                            final ArrayList<ChatMessage> clearedChatMessages = (ArrayList<ChatMessage>) chatMessages.clone();

                            chatMessages.clear();

                            if (UP == direction) {
                                final int removeLimit = currentChatMessagesSize - removeItemsNumber;
                                final int removeFrom = currentChatMessagesSize - 1;

                                for (int i = removeFrom; i >= removeLimit; i--) {
                                    removeChatMessage(clearedChatMessages, i);
                                }

                                appendChatMessages(chatMessages, receivedChatMessages, clearedChatMessages);
                            } else {
                                //DOWN
                                for (int i = 0; i < removeItemsNumber; i++) {
                                    removeChatMessage(clearedChatMessages);
                                }
                                appendChatMessages(chatMessages, clearedChatMessages, receivedChatMessages);
                            }
                        } else {
                            final ArrayList<ChatMessage> clearedChatMessages = (ArrayList<ChatMessage>) chatMessages.clone();

                            chatMessages.clear();

                            if (UP == direction) {
                                appendChatMessages(chatMessages, receivedChatMessages, clearedChatMessages);
                            } else {
                                appendChatMessages(chatMessages, clearedChatMessages, receivedChatMessages);
                            }
                        }
                        notifyDataSetChanged(activity, chatMessageAdapter);
                    }
                }
                swipeContainer.setRefreshing(false);
            }
        }).request(requestParameters, globalVariable);
        } else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    private void removeChatMessage(ArrayList<ChatMessage> clearedChatMessages){
        removeChatMessage(clearedChatMessages, 0);
    }

    public void removeChatMessage(ArrayList<ChatMessage> clearedChatMessages, final int index){
        final Long userId = clearedChatMessages.get(index).getMessageId();
        //wallPostService.deleteWallPostFromMDB(userId);
        //removeWallPostFromCash(wallPostId);
        //globalVariable.getFollowedWallPostCash().remove(clearedWallPosts.get(index));
        //clearedWallPosts.remove(index);
    }

    public void saveChatMessage(final ChatMessage chatMessage) {
        //wallPost.setReceived(true);
        // putToCash(wallPost, globalVariable.getWallPostCash(), globalVariable.getFollowedWallPostCash());
        //wallPostService.saveWallPostToMDB(wallPost, true);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ChatMessage chatMessage = parseMessage(intent.getStringExtra("message"), utils);
            if (chatMessage == null) {
                showToast("Something went wrong. Bad response from server.");
            } else {
                // Checking if this chatMessage related to this conversation . If this chatMessage is not for this conversation, do not add it.
                if (chatMessage.getConversationId().equals(currentConversationId)) {
                    // Appending the message to chat list
                    appendChatMessage(chatMessage, chatMessages);
                    notifyDataSetChanged(activity, chatMessageAdapter);
                    mRecyclerView.scrollToPosition(chatMessages.size() - 1);
                }
            }
        }
    };

    private void showToast(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }
}

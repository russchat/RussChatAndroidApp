package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONRequest;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class Like extends BasicTransportJSONObject implements JSONResponse, JSONRequest {

    private Long likeMDBId;

    private Long likeId;

    private Long fromUserId;

    private Long wallMessageId;

    private Boolean disabled;

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Long getLikeMDBId() {
        return likeMDBId;
    }

    public void setLikeMDBId(Long likeMDBId) {
        this.likeMDBId = likeMDBId;
    }

    public Long getLikeId() {
        return likeId;
    }

    public void setLikeId(Long likeId) {
        this.likeId = likeId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }
//
//    @Override
//    public int compareTo(Object o) {
//        Like like = (Like) o;
//        Long result = this.likeId - like.getLikeId();
//        return result.intValue();
//    }
}

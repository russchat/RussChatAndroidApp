package com.pkletsko.russ.chat.ui.activity.wall.comment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.ui.activity.profile.activity.UserProfileActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;

import java.util.ArrayList;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<Comment> comments;
    private GlobalClass globalVariable;
    private AssetManager assetManager;
    private WebSocketClient client;
    private RecyclerView recyclerView;


    private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    // LogCat tag
    private static final String TAG = CommentAdapter.class.getSimpleName();

    public CommentAdapter(Activity activity, ArrayList<Comment> comments, GlobalClass globalVariable, RecyclerView recyclerView) {
        Log.d(TAG, "initialization of CommentRecyclerAdapter");
        this.recyclerView = recyclerView;
        this.assetManager = activity.getAssets();
        this.activity = activity;
        this.comments = comments;
        this.globalVariable = globalVariable;
        this.client = globalVariable.getClient();
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View vi = LayoutInflater.from(context).inflate(R.layout.comment_item, parent, false);
        //UI initialization
        vi.setOnClickListener(mOnClickListener);
        ViewHolder viewHolder = new ViewHolder(vi);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        //font initialization
        Typeface userNameFont = Typeface.createFromAsset(assetManager, "Calibri Bold.ttf");
        Typeface postTextFont = Typeface.createFromAsset(assetManager, "Gidole-Regular.otf");

        viewHolder.userProfile = globalVariable.getUsersCash().get(comments.get(position).getFromUserId());

        viewHolder.commentMsg.setText(comments.get(position).getCommentText());
        viewHolder.commentMsg.setTypeface(postTextFont);

        // set new values to template
        viewHolder.txtTitle.setText(viewHolder.userProfile.getFullName());
        viewHolder.txtTitle.setTypeface(userNameFont);

        globalVariable.getPicassoHelper().loadImageWithFit(viewHolder.userProfile.getProfileIcon(), viewHolder.imageView, globalVariable.getPicassoHelper().getRoundedTransformation());
    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return comments.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public ImageView imageView;
        public TextView commentMsg;

        public UserProfile userProfile;

        public ViewHolder(View vi) {
            super(vi);

            txtTitle = (TextView) vi.findViewById(R.id.commentUserName);
            imageView = (ImageView) vi.findViewById(R.id.commentUserProfileIcon);
            commentMsg = (TextView) vi.findViewById(R.id.commentMsg);
        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            if (globalVariable.isAppRunning()) {
                int itemPosition = recyclerView.getChildLayoutPosition(view);
                UserProfile item = globalVariable.getUsersCash().get(comments.get(itemPosition).getFromUserId());
                globalVariable.setOpenProfile(item.getUserId());
                Intent i = new Intent(activity.getApplicationContext(), UserProfileActivity.class);
                activity.startActivity(i);
            } else {
                Toast.makeText(activity.getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
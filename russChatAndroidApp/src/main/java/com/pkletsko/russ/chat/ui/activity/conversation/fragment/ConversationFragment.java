package com.pkletsko.russ.chat.ui.activity.conversation.fragment;


import android.app.Activity;

import com.pkletsko.russ.chat.ui.activity.common.tab.TabEnum;
import com.pkletsko.russ.chat.ui.activity.conversation.fragment.base.BaseConversationFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;

import java.util.ArrayList;
import java.util.Set;

public class ConversationFragment extends BaseConversationFragment {

    private static final String TAG = ConversationFragment.class.getSimpleName();

    @Override
    protected Set<Chat> getCash() {
        return globalVariable.getConversationCash();
    }

    @Override
    protected void removeConversation(ArrayList<Chat> clearedChats, int index) {
    }

    @Override
    protected void saveConversation(Chat chat) {
        globalVariable.getConversationCash().add(chat);
        globalVariable.getChatCash().put(chat.getConversationId(), chat);
        chatService.saveConversationToMDB(chat);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        if (visible) {
            if (globalVariable != null) {
                globalVariable.setActiveTab(TabEnum.TAB_CONVERSATION);
            }
        }
        super.setMenuVisibility(visible);
    }
}
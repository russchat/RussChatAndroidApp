package com.pkletsko.russ.chat.common.event.handler;

import android.content.Context;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.chat.ChatEventHandler;
import com.pkletsko.russ.chat.common.event.handler.user.UserEventHandler;
import com.pkletsko.russ.chat.common.event.handler.wall.CommentEventHandler;
import com.pkletsko.russ.chat.common.event.handler.wall.LikeEventHandler;
import com.pkletsko.russ.chat.common.event.handler.wall.WallPostEventHandler;
import com.pkletsko.russ.chat.other.Utils;

public class EventHub {

    /*BROADCAST_ACTIVE_CONVERSATION_UPDATE - conversation list will be updated*/
    public static final String BROADCAST_CONVERSATION_LIST_UPDATE = "com.pkletsko.russchat.update.conversation.list";

    /*BROADCAST_ACTIVE_CONVERSATION_UPDATE - new chat message will be parsed and added to chat activity*/
    public static final String BROADCAST_ACTIVE_CONVERSATION_UPDATE = "com.pkletsko.russchat.update.active.conversation";

    //TODO seems like not used
    public static final String BROADCAST_CREATE_NEW_CONVERSATION = "com.pkletsko.russchat.cteate.new.conversation";

    /*BROADCAST_ACTIVE_COMMENT_UPDATE - new comment will be parsed and added to comment activity if it is currently opened*/
    public static final String BROADCAST_ACTIVE_COMMENT_UPDATE = "com.pkletsko.russchat.update.active.comment";

    /*BROADCAST_ACTIVE_MY_WALL_UPDATE - new my wall post will be parsed and added to my wall*/
    public static final String BROADCAST_ACTIVE_MY_WALL_UPDATE = "com.pkletsko.russchat.update.active.my.wall";

    /*BROADCAST_ACTIVE_WALL_UPDATE - new wall post will be parsed and added to wall*/
    public static final String BROADCAST_ACTIVE_WALL_UPDATE = "com.pkletsko.russchat.update.active.wall";

    /*BROADCAST_NOTIFY_DATA_SET_CHANGED - trigger update for wall/my wall/ profile history wall*/
    public static final String BROADCAST_NOTIFY_DATA_SET_CHANGED = "com.pkletsko.russchat.notify.data.set.changed";
    public static final String BROADCAST_NOTIFY_LIKE_CHANGED = "com.pkletsko.russchat.notify.like.changed";
    public static final String BROADCAST_NOTIFY_COMMENT_CHANGED = "com.pkletsko.russchat.notify.comment.changed";

    /*BROADCAST_PROFILE_UPDATE - trigger activity change from edit used to main activity in case user profile was updated successfully*/
    public static final String BROADCAST_PROFILE_UPDATE = "com.pkletsko.russchat.update.profile";

    /*BROADCAST_GET_USER_PROFILE - user info will be updated on main and user profile activities*/
    public static final String BROADCAST_GET_USER_PROFILE = "com.pkletsko.russchat.get.user.profile";

    /*BROADCAST_GET_USER_PROFILE - allow user to proceed father when login*/
    public static final String BROADCAST_LOGIN = "com.pkletsko.russchat.login";

    public static final String BROADCAST_CHAT_USER_SEARCH_RESULT = "com.pkletsko.russchat.chat.user.search.result";

    //TODO need investigation
    public static final String BROADCAST_SEARCH_RESULT = "com.pkletsko.russchat.search.result";

    //TODO what is it ? seems like old and not used at all
    public static final String BROADCAST_PROFILE_WALL_UPDATE = "com.pkletsko.russchat.update.profile.wall";

    public static final String BROADCAST_NOTIFY_UPLOAD_PROGRESS_BAR_CHANGED = "com.pkletsko.russchat.notify.upload.progress.bar.changed";
    public static final String BROADCAST_NOTIFY_UPLOAD_COMPLETED = "com.pkletsko.russchat.notify.upload.completed";


    private ChatEventHandler chatEventHandler;

    private UserEventHandler userEventHandler;

    private WallPostEventHandler wallPostEventHandler;

    private CommentEventHandler commentEventHandler;

    private LikeEventHandler likeEventHandler;

    public EventHub(final GlobalClass globalVariable, final Utils utils, final Context context) {


        chatEventHandler = new ChatEventHandler(globalVariable, utils, context);

        userEventHandler = new UserEventHandler(globalVariable, utils, context);

        wallPostEventHandler = new WallPostEventHandler(globalVariable, utils, context);

        commentEventHandler = new CommentEventHandler(globalVariable, utils, context);

        likeEventHandler = new LikeEventHandler(globalVariable, utils, context);

    }

    public void searchEventHandler(final String msg) {
        userEventHandler.searchEventHandler(msg);
    }
    public void chatUserSearchEventHandler(final String msg) {
        userEventHandler.chatUserSearchEventHandler(msg);
    }

    public void loginStatusEventHandler(final String msg) {
        userEventHandler.loginStatusEventHandler(msg);
    }

    public void updateUserProfileInfoEventHandler(final String msg) {
        userEventHandler.updateUserProfileInfoEventHandler(msg);
    }

    public void chatMessageEventHandler(final String msg) {
        chatEventHandler.chatMessageEventHandler(msg);
    }

    public void conversationEventHandler(final String msg) {
        chatEventHandler.conversationEventHandler(msg);
    }

    public void unreceivedChatMessagesEventHandler(final String msg) {
        chatEventHandler.unreceivedChatMessagesEventHandler(msg);
    }

    public void unreceivedLikesEventHandler(final String msg) {
        wallPostEventHandler.unreceivedLikesEventHandler(msg);
    }

    public void wallPostEventHandler(final String msg) {
        wallPostEventHandler.wallPostEventHandler(msg);
    }

    public void unreceivedCommentsEventHandler(final String msg) {
        commentEventHandler.unreceivedCommentsEventHandler(msg);
    }

    public void commentEventHandler(final String msg) {
        commentEventHandler.commentEventHandler(msg);
    }

    public void userProfileEventHandler(final String msg) {
        userEventHandler.userProfileEventHandler(msg);
    }

    public void unreceivedWallPostsEventHandler(final String msg) {
        likeEventHandler.unreceivedWallPostsEventHandler(msg);
    }

    public void likeEventHandler(final String msg) {
        likeEventHandler.likeEventHandler(msg);
    }

    public void dislikeEventHandler(final String msg) {
        likeEventHandler.dislikeEventHandler(msg);
    }
}

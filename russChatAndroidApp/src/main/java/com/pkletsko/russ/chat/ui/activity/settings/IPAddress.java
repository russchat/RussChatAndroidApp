package com.pkletsko.russ.chat.ui.activity.settings;

/**
 * Created by pkletsko on 18.01.2016.
 */
public class IPAddress {
    private Long ipMDBId;

    private String ipAddress;

    public Long getIpMDBId() {
        return ipMDBId;
    }

    public void setIpMDBId(Long ipMDBId) {
        this.ipMDBId = ipMDBId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}

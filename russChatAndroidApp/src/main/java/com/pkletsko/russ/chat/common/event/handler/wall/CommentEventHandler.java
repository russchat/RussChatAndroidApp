package com.pkletsko.russ.chat.common.event.handler.wall;

import android.content.Context;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.storage.service.WallPostService;
import com.pkletsko.russ.chat.storage.service.WallPostServiceImpl;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.UnReceivedComments;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pkletsko on 26.05.2015.
 */
public class CommentEventHandler {

    private static final String TAG = CommentEventHandler.class.getSimpleName();

    private UserService userService;

    private WallPostService wallPostService;

    private GlobalClass globalVariable;

    private Utils utils;

    private Context context;

    public CommentEventHandler(final GlobalClass globalVariable, final Utils utils, final Context context) {
        this.globalVariable = globalVariable;
        this.utils = utils;
        this.context = context;

        userService = new UserServiceImpl(globalVariable, utils);

        wallPostService = new WallPostServiceImpl(globalVariable, userService ,context);

    }

    public void unreceivedCommentsEventHandler(final String msg) {
        if (globalVariable.getCurrentUser() != null) {
            // async get data from database and after set data to global variable and than call redirect to login activity
            UnReceivedComments unReceivedComments = utils.getUnReceivedComments(msg);
            if (unReceivedComments == null) {
                showToast("Something went wrong. Bad response from server.");
            } else {
                wallPostService.initializationOfComments(unReceivedComments);
            }
        }
    }

    public void commentEventHandler(final String msg) {
        Comment comment = utils.getComment(msg);

        if (comment == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {

            //1 - save comment to mdb
            wallPostService.saveCommentToMDB(comment);

            //check if this user in db
            //if not load profile and add it and to cash
            userService.userChecking(comment.getFromUserId());

            //2 - put comment to cash
            if (globalVariable.getCommentCash().get(comment.getWallMessageId()) != null) {
                globalVariable.getCommentCash().get(comment.getWallMessageId()).add(comment);
            } else {
                Set<Comment> comments = new HashSet<Comment>();
                comments.add(comment);
                globalVariable.getCommentCash().put(comment.getWallMessageId(), comments);
            }

            wallPostService.updateWallPostCommentNumberMDB(comment, msg);
        }
    }

    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }
}

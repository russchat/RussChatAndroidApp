package com.pkletsko.russ.chat.storage.service;

import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.Set;

/**
 * Created by pkletsko on 25.05.2015.
 */
public interface UserService {
    public void storeUserToMDB(final UserProfile userProfile);

    public void storeUsersToMDB(final Set<UserProfile> UserProfiles);

    public void saveOrUpdateUserMDB(final UserProfile userProfile);

    public void userChecking(final Long userId);
}

package com.pkletsko.russ.chat.websocket.message.json.model.wall;

/**
 * Created by pkletsko on 30.03.2015.
 */
public class Image extends Attachment {
    private String url;
    private String path;
    private FileToUpload fileToUpload;
    private Long attachmentId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FileToUpload getFileToUpload() {
        return fileToUpload;
    }

    public void setFileToUpload(FileToUpload fileToUpload) {
        this.fileToUpload = fileToUpload;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }
}

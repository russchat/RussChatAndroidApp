package com.pkletsko.russ.chat.ui.activity.common.search;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseActivityTest;
import com.pkletsko.russ.chat.ui.activity.profile.fragment.adapter.UserListRecyclerAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.ArrayList;

public class SearchableActivity extends BaseActivityTest {

    private static final String TAG = SearchableActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;

    private ArrayList<UserProfile> userProfiles = new ArrayList<>();

    private UserListRecyclerAdapter userProfileAdapter;

    private EditText searchRequest;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.search_main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Search");

        activity = this;

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        mRecyclerView = (RecyclerView) findViewById(R.id.searchRecyclerView);

        searchRequest = (EditText) findViewById(R.id.searchRequestEditText);
        searchRequest.setText(globalVariable.getSearchQuery());
        searchRequest.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String serachText = searchRequest.getText().toString();
                if (serachText.length() >= 3) {
                    globalVariable.setSearchQuery(serachText);
                    sendMessageToServer(utils.getSearchRequestJSON(serachText));
                }
            }
        });


        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);

        userProfiles.addAll(globalVariable.getSearchResult());
        userProfileAdapter = new UserListRecyclerAdapter(this, userProfiles, globalVariable, mRecyclerView);

        mRecyclerView.setAdapter(userProfileAdapter);

    }

    protected BroadcastReceiver broadcastsearchResult = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            userProfiles.clear();
            userProfiles.addAll(globalVariable.getSearchResult());
            userProfileAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(intentBroadcastService);
        registerReceiver(broadcastsearchResult, new IntentFilter(EventHub.BROADCAST_SEARCH_RESULT));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastsearchResult);
        super.onPause();
    }
}

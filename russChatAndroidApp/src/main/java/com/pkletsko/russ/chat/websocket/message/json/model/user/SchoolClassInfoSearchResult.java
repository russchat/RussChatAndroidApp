package com.pkletsko.russ.chat.websocket.message.json.model.user;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
public class SchoolClassInfoSearchResult extends BasicTransportJSONObject implements JSONRequest {

  private List<SchoolClassInfo> schoolClassInfos = new ArrayList<SchoolClassInfo>();

    public List<SchoolClassInfo> getSchoolClassInfos() {
        return schoolClassInfos;
    }

    public void setSchoolClassInfos(List<SchoolClassInfo> schoolClassInfos) {
        this.schoolClassInfos = schoolClassInfos;
    }
}

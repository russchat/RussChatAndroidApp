package com.pkletsko.russ.chat.ui.activity.common.search;

/**
 * Created by pkletsko on 21.03.2015.
 */
import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class PlacesSuggestionProvider extends ContentProvider {
    private static final String LOG_TAG = "ExampleApp";

    public static final String AUTHORITY = "com.example.google.places.search_suggestion_provider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/search");

    // UriMatcher constant for search suggestions
    private static final int SEARCH_SUGGEST = 1;

    private static final UriMatcher uriMatcher;

    public static final String[] SEARCH_SUGGEST_COLUMNS = {
            BaseColumns._ID,
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_TEXT_2,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID
    };

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST);
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH_SUGGEST);
    }

    private GlobalClass globalVariable;
    private Utils utils;

    @Override
    public int delete(Uri uri, String arg1, String[] arg2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case SEARCH_SUGGEST:
                return SearchManager.SUGGEST_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues arg1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean onCreate() {
        globalVariable = (GlobalClass) getContext();
        utils = new Utils(getContext());
        return true;
    }

    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(LOG_TAG, "query = " + uri);

        // Use the UriMatcher to see what kind of query we have
        switch (uriMatcher.match(uri)) {
            case SEARCH_SUGGEST:
                Log.d(LOG_TAG, "Search suggestions requested.");

                final MatrixCursor cursor = new MatrixCursor(SEARCH_SUGGEST_COLUMNS, 1);

                if (selectionArgs[0] != null && !selectionArgs[0].isEmpty() && selectionArgs[0].length() > 1) {

                    globalVariable.setSearchQuery(selectionArgs[0]);
                    sendMessageToServer(utils.getSearchRequestJSON(selectionArgs[0]));

//                Runnable task = new Runnable() {
//                    public void run() {
//                        int i = 1;
//                        for(UserProfile userProfile: globalVariable.getSearchResult()) {
//                            cursor.addRow(new String[] {String.valueOf(i), userProfile.getFirstName(), "", userProfile.getUserId().toString()});
//                            i++;
//                        }
//                    }
//                };
//                worker.schedule(task, 5, TimeUnit.SECONDS);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Do something after 5s = 5000ms
//                        int i = 1;
//                        for(UserProfile userProfile: globalVariable.getSearchResult()) {
//                            cursor.addRow(new String[] {String.valueOf(i), userProfile.getFirstName(), "", userProfile.getUserId().toString()});
//                            i++;
//                        }
//                    }
//                }, 5000);

                    int i = 1;
                    for (UserProfile userProfile : globalVariable.getSearchResult()) {
                        cursor.addRow(new String[]{String.valueOf(i), userProfile.getFullName(), "", userProfile.getUserId().toString()});
                        i++;
                    }
                }

//                if (globalVariable.isSearch()) {
//                    globalVariable.setSearch(false);
//
//                    int i = 1;
//                    for(UserProfile userProfile: globalVariable.getSearchResult()) {
//                        cursor.addRow(new String[] {String.valueOf(i), userProfile.getFirstName(), "", userProfile.getUserId().toString()});
//                        i++;
//                    }
//
//                } else {
//                    globalVariable.setSearch(true);
//                    sendMessageToServer(utils.getSearchRequestJSON(uri.getLastPathSegment()));
//                }

                return cursor;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues arg1, String arg2, String[] arg3) {
        throw new UnsupportedOperationException();
    }

    private void sendMessageToServer(String message) {
        WebSocketClient client = globalVariable.getClient();
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }
}

package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

public class Comment extends BasicTransportJSONObject implements JSONResponse {

    private Long commentMDBId;

    private Long commentId;

    private Long wallMessageId;

    private Long fromUserId;

    private String commentText;

    public Long getCommentMDBId() {
        return commentMDBId;
    }

    public void setCommentMDBId(Long commentMDBId) {
        this.commentMDBId = commentMDBId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

//    @Override
//    public int compareTo(Object o) {
//        Comment comment = (Comment) o;
//        Long result = this.commentId - comment.getCommentId();
//        return result.intValue();
//    }
}

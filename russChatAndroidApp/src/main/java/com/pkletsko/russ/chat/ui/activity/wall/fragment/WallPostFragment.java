package com.pkletsko.russ.chat.ui.activity.wall.fragment;

import com.pkletsko.russ.chat.ui.activity.wall.fragment.base.BaseWallPostDetailsFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.HashSet;
import java.util.Set;


public class WallPostFragment extends BaseWallPostDetailsFragment {

    private static final String TAG = WallPostFragment.class.getSimpleName();

    @Override
    protected Set<WallPost> getCash() {
        return new HashSet<>();
    }

//    protected Map<String, String> getRequestParameters() {
//        final Map<String,String> requestParameters = new HashMap<>();
//        requestParameters.put(RequestUtil.REQUEST_URL_KEY, GlobalClass.CONFIG_GET_USER_WALL_POSTS_URL);
//        requestParameters.put("id", globalVariable.getOpenProfile().toString());
//        return requestParameters;
//    }
//
//    @Override
//    protected void removeWallPost(ArrayList<WallPost> clearedWallPosts, final int index){
//        final Long wallPostId = clearedWallPosts.get(index).getWallMessageId();
//        wallPostService.deleteWallPostFromMDB(wallPostId);
//        removeWallPostFromCash(wallPostId);
//        //TODO should be some cash
//        //removedFromFollowedWallPostCash(clearedWallPosts.get(index));
//        clearedWallPosts.remove(index);
//    }
//
//    @Override
//    protected void saveWallPost(final WallPost wallPost) {
//        wallPost.setReceived(true);
//        putToCash(wallPost, globalVariable.getWallPostCash());
//        //TODO should be some cash
//        //putToCash(wallPost, globalVariable.getFollowedWallPostCash());
//        //putToWallPostCash(wallPost);
//        //putToFollowedWallPostCash(wallPost);
//        wallPostService.saveWallPostToMDB(wallPost, true);
//    }
}

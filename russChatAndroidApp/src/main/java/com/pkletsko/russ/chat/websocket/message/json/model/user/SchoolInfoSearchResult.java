package com.pkletsko.russ.chat.websocket.message.json.model.user;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
public class SchoolInfoSearchResult extends BasicTransportJSONObject implements JSONRequest {

  private List<SchoolInfo> schools = new ArrayList<SchoolInfo>();

    public List<SchoolInfo> getSchools() {
        return schools;
    }

    public void setSchools(List<SchoolInfo> schools) {
        this.schools = schools;
    }
}

package com.pkletsko.russ.chat.ui.activity.profile.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.ui.activity.chat.ChatActivity;
import com.pkletsko.russ.chat.ui.activity.common.activity.CustomViewPager;
import com.pkletsko.russ.chat.ui.activity.common.activity.MainActivity;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseProfileActivity;
import com.pkletsko.russ.chat.ui.activity.profile.edit.UserProfileEditActivity;
import com.pkletsko.russ.chat.ui.activity.profile.fragment.UserListFragment;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.WallPostHistoryFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class UserProfileActivity extends BaseProfileActivity {

    // LogCat tag
    private static final String TAG = UserProfileActivity.class.getSimpleName();

    private ImageView profileIconSmall;
    private ImageView profileIconBig;
    private TextView someUserName;

    private ProgressDialog mDialog;

    private ImageButton followButton;
    private ImageButton chatButton;
    private ImageButton backButton;
    private ImageButton editMyProfileButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeBlue);
        super.onCreate(savedInstanceState);

        //this two checks should be together
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {
            mDialog = ProgressDialog.show(this, "Loading", "Wait while loading...");

            someUserName = (TextView) findViewById(R.id.someUserName);

            followButton = (ImageButton) findViewById(R.id.followButton);

            chatButton = (ImageButton) findViewById(R.id.chatButton);

            editMyProfileButton = (ImageButton) findViewById(R.id.editProfileButton);

            editMyProfileButton.setVisibility(View.GONE);
            followButton.setVisibility(View.GONE);
            chatButton.setVisibility(View.GONE);

            backButton = (ImageButton) findViewById(R.id.backProfileButton);

            backButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }

            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Before Start service");
        startService(intentBroadcastService);
        registerReceiver(broadcastReceiverGetUserProfile, new IntentFilter(EventHub.BROADCAST_GET_USER_PROFILE));
        registerReceiver(broadcastCreateNewConversationReceiver, new IntentFilter(EventHub.BROADCAST_CREATE_NEW_CONVERSATION));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastReceiverGetUserProfile);
        unregisterReceiver(broadcastCreateNewConversationReceiver);
        super.onPause();
    }

    private BroadcastReceiver broadcastCreateNewConversationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent i = new Intent(UserProfileActivity.this, ChatActivity.class);
            startActivity(i);
        }
    };

    @Override
    protected int getLayoutResourceId() {
        return R.layout.profile_main;
    }

    @Override
    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    sendMessageToServer(utils.getSendUserProfileRequestJSON(globalVariable.getOpenProfile()));
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();

                            // offline mode
                            //load user from cash if exist
                            updateUserProfile(globalVariable.getUsersCash().get(globalVariable.getOpenProfile()));

                            globalVariable.setIsAppRunning(false);
                            stopService(intentBroadcastService);
                        }
                    });
                }
                mDialog.dismiss();
            }
        }).isServerAlive(globalVariable);
    }

    private BroadcastReceiver broadcastReceiverGetUserProfile = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUserProfile(globalVariable.getUsersCash().get(globalVariable.getOpenProfile()));
        }
    };

    private void updateUserProfile(final UserProfile userProfile) {
        if (userProfile != null) {
            initViewPagerAndTabs(userProfile);

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (userProfile.getUserId() == globalVariable.getCurrentUser().getUserId()) {
                        editMyProfileButton.setVisibility(View.VISIBLE);
                        followButton.setVisibility(View.GONE);
                        chatButton.setVisibility(View.GONE);
                    } else {
                        editMyProfileButton.setVisibility(View.GONE);
                        followButton.setVisibility(View.VISIBLE);
                        chatButton.setVisibility(View.VISIBLE);
                    }

                    someUserName.setText(userProfile.getFullName());


                    if (userProfile.isFollowed()) {
                        followButton.setImageResource(R.drawable.unfollow1);
                    } else {
                        followButton.setImageResource(R.drawable.follow);
                    }

                    followButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            if (userProfile.isFollowed()) {
                                followButton.setImageResource(R.drawable.follow);

                                List<Long> wallIdList = new ArrayList<Long>();
                                wallIdList.add(userProfile.getMyWallId());
                                // Sending message to web socket server
                                sendMessageToServer(utils.getSendUnFollowUserJSON(wallIdList));

                                userProfile.setFollowed(false);
                            } else {
                                followButton.setImageResource(R.drawable.unfollow1);

                                List<Long> wallIdList = new ArrayList<Long>();
                                wallIdList.add(userProfile.getMyWallId());
                                // Sending message to web socket server
                                sendMessageToServer(utils.getSendFollowUserJSON(wallIdList));

                                userProfile.setFollowed(true);
                            }
                        }

                    });

                    chatButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            List<Long> userIdList = new ArrayList<Long>();
                            userIdList.add(userProfile.getUserId());
                            sendMessageToServer(utils.getSendCreateNewConversationJSON(userIdList, userProfile.getFullName()));
                        }

                    });

                    editMyProfileButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            Intent intentRedirect = new Intent(UserProfileActivity.this, UserProfileEditActivity.class);
                            startActivity(intentRedirect);
                        }

                    });

                    profileIconBig = (ImageView) findViewById(R.id.some_profile_icon_big);

                    profileIconBig.setAlpha(20);
                    Picasso.with(getApplicationContext())
                            .load(userProfile.getProfileIcon())
                            .into(profileIconBig);

                    profileIconSmall = (ImageView) findViewById(R.id.some_profile_icon_small);

                    //TODO 150 !! round ???
                    globalVariable.getPicassoHelper().loadImageWithFit(userProfile.getProfileIcon(), profileIconSmall, globalVariable.getPicassoHelper().getRoundedTransformation());

                    mDialog.dismiss();
                }
            });
        }
    }

    private void initViewPagerAndTabs(final UserProfile userProfile) {
        CustomViewPager viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getFragmentManager());


        pagerAdapter.addFragment(new WallPostHistoryFragment(), String.valueOf(userProfile.getMyPosts()), getApplicationContext());
        pagerAdapter.addFragment(UserListFragment.createInstance(globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_FOLLOWING_URL), String.valueOf(userProfile.getFollowingWalls()), getApplicationContext());
        pagerAdapter.addFragment(UserListFragment.createInstance(globalVariable.getFullServicePath() + globalVariable.CONFIG_GET_FOLLOWERS_URL), String.valueOf(userProfile.getFollowers()), getApplicationContext());

        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }

        viewPager.setCurrentItem(globalVariable.getOpenProfileCurrentTab());
        globalVariable.setOpenProfileCurrentTab(0);
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title, Context mycon) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
            context = mycon;
        }

        Context context;
        private String tabTitles[] = new String[]{"POSTS", "FOLLOWING", "FOLLOWERS"};

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(context).inflate(R.layout.profile_tab_header, null);

            TextView text1 = (TextView) v.findViewById(R.id.text1);
            text1.setText(fragmentTitleList.get(position));
            TextView text2 = (TextView) v.findViewById(R.id.text2);
            text2.setText(tabTitles[position]);

            return v;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}

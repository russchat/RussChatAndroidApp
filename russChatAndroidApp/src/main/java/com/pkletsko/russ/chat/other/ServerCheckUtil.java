package com.pkletsko.russ.chat.other;

import android.os.AsyncTask;
import android.util.Log;

import com.pkletsko.russ.chat.common.GlobalClass;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by pkletsko on 26.05.2015.
 */
public abstract class ServerCheckUtil {

    private static final String TAG = ServerCheckUtil.class.getSimpleName();

    public void isServerAlive(final GlobalClass globalVariable) {

        final String fullServerPath = globalVariable.getFullServicePath();
        Log.i(TAG, "isServerAlive : Start");
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                OkHttpClient client = globalVariable.getOkHttpClient();
                if (client == null) {
                    Log.d(TAG, "client is null !!!!");
                    return null;
                }

                RequestBody formBody = new FormBody.Builder()
                        .add("app", "RussChat")
                        .build();

                Request request = new Request.Builder()
                        .url(fullServerPath + GlobalClass.CONFIG_CHECK_SERVER)
                        .post(formBody)
                        .build();

                Response response;
                String result = null;
                try {
                    response = client.newCall(request).execute();
                    result = response.body().string();
                } catch (IOException e) {
                    Log.e(TAG, "isServerAlive:" + e);
                    onPostServerAlive("Fail");
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.i(TAG, "onPostExecute : isServerAlive:" + result);
                onPostServerAlive(result);
            }
        };
        //request to server using web service to get list of users
        task.execute();
    }

    public void onPostServerAlive(String result) {
        Log.i(TAG, "onPostServerAlive : SHOULD BE EMPTY:" + result);
        // should be empty
    }
}

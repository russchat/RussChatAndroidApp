package com.pkletsko.russ.chat.ui.activity.chat.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.ArrayList;


public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.ViewHolder> {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<ChatMessage> chatMessages;
    private GlobalClass globalVariable;
    private AssetManager assetManager;
    private WebSocketClient client;
    protected Utils utils;
    private View viRight;
    private View viLeft;

    // LogCat tag
    private static final String TAG = ChatMessageAdapter.class.getSimpleName();

    public ChatMessageAdapter(Object object, ArrayList<ChatMessage> chatMessages, GlobalClass globalVariable, RecyclerView recyclerView) {
        this.activity = (Activity) object;
        this.assetManager = activity.getAssets();
        this.chatMessages = chatMessages;
        this.globalVariable = globalVariable;
        this.client = globalVariable.getClient();
        this.utils = new Utils(activity.getApplicationContext());
    }

    @Override
    public ChatMessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        viLeft = LayoutInflater.from(context).inflate(R.layout.chat_item_message_left, parent, false);
        viRight = LayoutInflater.from(context).inflate(R.layout.chat_item_message_right, parent, false);
        //UI initialization
        switch (viewType) {
            case 0: return new ViewHolder(viLeft);
            case 1: return new ViewHolder(viRight);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage chatMessage = chatMessages.get(position);

        int result = 0;
        //1 = right
        //0 = left
        // Identifying the message owner
        if (chatMessage.getFromUserId().equals(globalVariable.getCurrentUser().getUserId())) {
            Log.d(TAG, "from current user : " + chatMessage.getFromUserId());
            // message belongs to you, so load the right aligned layout
            result = 1;
        }
        return result;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ChatMessage chatMessage = chatMessages.get(position);

        //font initialization
        Typeface userNameFont = Typeface.createFromAsset(assetManager, "Calibri Bold.ttf");
        Typeface postTextFont = Typeface.createFromAsset(assetManager, "Gidole-Regular.otf");

        viewHolder.chatMessage = chatMessage;

        viewHolder.txtMsg.setText(chatMessage.getMessageText());
        viewHolder.txtMsg.setTypeface(postTextFont);

        // set new values to template
        viewHolder.txtTitle.setText(viewHolder.chatMessage.getFromUserName());
        viewHolder.txtTitle.setTypeface(userNameFont);

        UserProfile userProfile = globalVariable.getUsersCash().get(viewHolder.chatMessage.getFromUserId());
        if (userProfile != null) {
            globalVariable.getPicassoHelper().loadImageWithFit(userProfile.getProfileIcon(), viewHolder.imageView, globalVariable.getPicassoHelper().getRoundedTransformation());
        } else {
            sendMessageToServer(utils.getSendUserProfileRequestJSON(viewHolder.chatMessage.getFromUserId()));
        }
    }

    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }

    // Return the size arraylist
    @Override
    public int getItemCount() {
        return chatMessages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public ImageView imageView;
        public TextView txtMsg;
        public View view;

        public ChatMessage chatMessage;

        public ViewHolder(View vi) {
            super(vi);
            view = vi;
            txtTitle=(TextView) vi.findViewById(R.id.lblMsgFrom);
            imageView=(ImageView) vi.findViewById(R.id.userProfileIcon);
            txtMsg=(TextView) vi.findViewById(R.id.txtMsg);
        }
    }
}
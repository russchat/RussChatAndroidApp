/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pkletsko.russ.chat.ui.activity.login;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.storage.dao.AttachmentMDAO;
import com.pkletsko.russ.chat.storage.dao.CommentMDAO;
import com.pkletsko.russ.chat.storage.dao.ConversationMDAO;
import com.pkletsko.russ.chat.storage.dao.IPAddressMDAO;
import com.pkletsko.russ.chat.storage.dao.LikeMDAO;
import com.pkletsko.russ.chat.storage.dao.MessageMDAO;
import com.pkletsko.russ.chat.storage.dao.UnloadedAttachmentMDAO;
import com.pkletsko.russ.chat.storage.dao.UnloadedAttachmentWallPostLinkMDAO;
import com.pkletsko.russ.chat.storage.dao.UserMDAO;
import com.pkletsko.russ.chat.storage.dao.WallMessageMDAO;
import com.pkletsko.russ.chat.ui.activity.common.activity.MainActivity;
import com.pkletsko.russ.chat.ui.activity.settings.IPAddress;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.OkHttpClient;

/**
 * Android Google+ Quickstart activity.
 * <p/>
 * Demonstrates Google+ Sign-In and usage of the Google+ APIs to retrieve a
 * users profile information.
 */
public class SplashActivity extends Activity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    private GlobalClass globalVariable;

    private Intent intentGoogleLogin;

    private ConversationMDAO conversationMDAO;
    private MessageMDAO messageMDAO;
    private UserMDAO userMDAO;
    private WallMessageMDAO wallMessageMDAO;
    private CommentMDAO commentMDAO;
    private LikeMDAO likeMDAO;
    private AttachmentMDAO attachmentMDAO;
    private IPAddressMDAO ipAddressMDAO;
    private UnloadedAttachmentWallPostLinkMDAO unloadedAttachmentWallPostLinkMDAO;
    private UnloadedAttachmentMDAO unloadedAttachmentMDAO;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    private Intent intentBroadcastService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        Log.i(TAG, "onCreate");

        globalVariable = (GlobalClass) getApplicationContext();
        globalVariable.setIsAppRunning(false);

        intentBroadcastService = new Intent(this, BroadcastService.class);

        init();
    }

    private void httpClientInit() throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
        SSLSocketFactory sslSocketFactory = buildSslSocketFactory();
        OkHttpClient client = new OkHttpClient.Builder()
                .sslSocketFactory(sslSocketFactory)
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                .build();

        globalVariable.setOkHttpClient(client);
        globalVariable.setSslSocketFactory(sslSocketFactory);
    }

    private SSLSocketFactory buildSslSocketFactory() {
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        InputStream cert = getApplicationContext().getResources().openRawResource(R.raw.jetty);
        Certificate ca = null;
        try {
            ca = cf.generateCertificate(cert);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        try {
            cert.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // creating a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance(keyStoreType);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        try {
            keyStore.load(null, null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        try {
            keyStore.setCertificateEntry("ca", ca);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        try {
            return new AdditionalKeyStoresSSLSocketFactory(keyStore);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void init() {
        AsyncTask<Void, Void, UserProfile> task = new AsyncTask<Void, Void, UserProfile>() {
            @Override
            protected UserProfile doInBackground(Void... params) {
                try {
                    httpClientInit();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (UnrecoverableKeyException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                //Database initialization
                databaseInitialization();
                globalVariable.setPicassoHelper(new PicassoHelper(getApplicationContext(), "Offline"));

                return globalVariable.getUserMDAO().getCurrentUser();
            }

            @Override
            protected void onPostExecute(final UserProfile currentUser) {
                if (currentUser != null) {
                    Log.d(TAG, " User Already Logged In Case : Initialization");

                    globalVariable.setCurrentUser(currentUser);
                    userInitialization();
                    wallInitialization();
                    conversationInit();
                    ipInit();
                    // TODO chat INIT

                    serverCheck();
                } else {
                    new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                        @Override
                        public void run() {
                            // This method will be executed once the timer is over
                            Log.d(TAG, " New User Case : Initialization");
                            redirectToLoginActivity();
                        }
                    }, SPLASH_TIME_OUT);
                }

            }
        };

        task.execute();
    }

    private void redirectToLoginActivity() {
        Log.d(TAG, " redirectToLoginActivity");
        Intent intentRedirect = new Intent(SplashActivity.this, LoginActivity.class);
        intentRedirect.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentRedirect.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intentRedirect.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intentRedirect);
        // close this activity
        finish();
    }

    private void redirectToMainActivity() {
        Log.d(TAG, " redirectToMainActivity");

        Intent intentRedirect = new Intent(SplashActivity.this, MainActivity.class);
        intentRedirect.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentRedirect.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intentRedirect.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intentRedirect);
        // close this activity
        finish();
    }

    protected void serverCheck() {
        Log.d(TAG, "serverCheck");
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    Log.d(TAG, "Server Is Online");
                    startService(intentBroadcastService);
                } else {
                    Log.d(TAG, "onPostServerAlive: No internet connection or server is down.");
                    redirectToMainActivity();
                }
            }
        }).isServerAlive(globalVariable);
    }

    private BroadcastReceiver broadcastLoginToServerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "broadcastLoginToServerReceiver");

            redirectToMainActivity();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(broadcastLoginToServerReceiver, new IntentFilter(EventHub.BROADCAST_LOGIN));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastLoginToServerReceiver);
        super.onPause();
    }

    private void databaseInitialization() {
        conversationMDAO = new ConversationMDAO(this);
        conversationMDAO.open();
        globalVariable.setConversationMDAO(conversationMDAO);

        messageMDAO = new MessageMDAO(this);
        messageMDAO.open();
        globalVariable.setMessageMDAO(messageMDAO);

        userMDAO = new UserMDAO(this);
        userMDAO.open();
        globalVariable.setUserMDAO(userMDAO);

        wallMessageMDAO = new WallMessageMDAO(this);
        wallMessageMDAO.open();
        globalVariable.setWallMessageMDAO(wallMessageMDAO);

        commentMDAO = new CommentMDAO(this);
        commentMDAO.open();
        globalVariable.setCommentMDAO(commentMDAO);

        likeMDAO = new LikeMDAO(this);
        likeMDAO.open();
        globalVariable.setLikeMDAO(likeMDAO);

        attachmentMDAO = new AttachmentMDAO(this);
        attachmentMDAO.open();
        globalVariable.setAttachmentMDAO(attachmentMDAO);

        ipAddressMDAO = new IPAddressMDAO(this);
        ipAddressMDAO.open();
        globalVariable.setIpAddressMDAO(ipAddressMDAO);

        unloadedAttachmentWallPostLinkMDAO = new UnloadedAttachmentWallPostLinkMDAO(this);
        unloadedAttachmentWallPostLinkMDAO.open();
        globalVariable.setUnloadedAttachmentWallPostLinkMDAO(unloadedAttachmentWallPostLinkMDAO);

        unloadedAttachmentMDAO = new UnloadedAttachmentMDAO(this);
        unloadedAttachmentMDAO.open();
        globalVariable.setUnloadedAttachmentMDAO(unloadedAttachmentMDAO);
    }

    private void ipInit() {
        IPAddress ipAddress = globalVariable.getIpAddressMDAO().getIpAddress(globalVariable.getCurrentUser().getUserId());
        if (ipAddress == null) {
            globalVariable.getIpAddressMDAO().saveOrUpdateIpAddress(globalVariable.getIp(), globalVariable.getCurrentUser().getUserId());
        } else {
            globalVariable.setIp(ipAddress.getIpAddress());
        }
    }

    private void userInitialization() {
        Set<UserProfile> userProfiles = globalVariable.getUserMDAO().getAllUsersByAppOwnerId(globalVariable.getCurrentUser().getUserId());

        for (UserProfile userProfile : userProfiles) {
            globalVariable.addUserProfileToCash(userProfile);
        }
    }

    private void putToWallPostCash(final WallPost wallPost) {
        globalVariable.getWallPostCash().put(wallPost.getWallMessageId(), wallPost);
    }

    private void wallInitialization() {
        //preparing main wall messages
        Set<WallPost> wallMessages = globalVariable.getWallMessageMDAO().getWallPostsByAppOwnerId(globalVariable.getCurrentUser().getUserId());
        for (WallPost wallPost : wallMessages) {

            if (wallPost.isImageAttached()) {
                wallPost.setAttachments(globalVariable.getAttachmentMDAO().getAttachmentNamesByWallMessageId(wallPost.getWallMessageId(), globalVariable.getCurrentUser().getUserId()));
            }

            if (wallPost.getFromUserId() == globalVariable.getCurrentUser().getUserId()) {
                globalVariable.getMyWallPostCash().add(wallPost);
            } else if (wallPost.isReceived()) { //TODO check if this idea with received post works fine
                // there are all messages from database (including unread messages)
                globalVariable.getFollowedWallPostCash().add(wallPost);
            }

            putToWallPostCash(wallPost);
        }
    }

    private void conversationInit() {
        Set<Chat> conversations = globalVariable.getConversationMDAO().getAllConversationsByOwnerId(globalVariable.getCurrentUser().getUserId());
        globalVariable.setConversationCash(conversations);

        for (Chat conversation : conversations) {
            globalVariable.getChatCash().put(conversation.getConversationId(), conversation);
        }
    }
}

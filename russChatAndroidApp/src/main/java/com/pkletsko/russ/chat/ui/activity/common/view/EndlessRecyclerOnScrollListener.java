package com.pkletsko.russ.chat.ui.activity.common.view;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public abstract class EndlessRecyclerOnScrollListener extends
        RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class
            .getSimpleName();

    private int previousTotal = 0;
    private boolean loading = true;
    private boolean loadingWithLimit = true;
    private int visibleThreshold = 5;
    private int firstVisibleItem, visibleItemCount, totalItemCount;

    private int current_page = 1;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessRecyclerOnScrollListener(
            LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (!loadingWithLimit && totalItemCount > (visibleItemCount + firstVisibleItem)){
            loadingWithLimit  = true;
        }

        if (loading) {
            if (totalItemCount > previousTotal) {
                //Log.d(TAG, totalItemCount + " > " + previousTotal);
                loading = false;
                previousTotal = totalItemCount;
            } else if (loadingWithLimit && totalItemCount == (visibleItemCount + firstVisibleItem)) {
                loading = false;
                loadingWithLimit = false;
            }
        }

        Log.d(TAG, "loading = " + loading + " ,totalItemCount = " + totalItemCount + " , visibleItemCount = " + visibleItemCount + " , firstVisibleItem = " + firstVisibleItem + " , visibleThreshold = " + visibleThreshold);
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached

            // Do something
            current_page++;

            onLoadMore(current_page);

            loading = true;
        }
    }

    public abstract void onLoadMore(int current_page);

    public int getTotalItemCount() {
        return totalItemCount;
    }
}

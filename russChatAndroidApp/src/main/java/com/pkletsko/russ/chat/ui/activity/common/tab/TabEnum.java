package com.pkletsko.russ.chat.ui.activity.common.tab;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 15:24
 * To change this template use File | Settings | File Templates.
 */
public enum TabEnum {
    TAB_CONVERSATION("TAB_CONVERSATION"),
    TAB_WALL("TAB_WALL"),
    TAB_MY_WALL("TAB_MY_WALL"),
    TAB_GROUP("TAB_GROUP"),
    TAB_EVENT("TAB_EVENT");


    private final String value;

    private TabEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TabEnum create(String val) {
        TabEnum result = null;

        for (TabEnum unit : TabEnum.values()) {
            if (unit.toString().equalsIgnoreCase(val) || unit.getValue().equalsIgnoreCase(val)) {
                result = unit;
                break;
            }
        }

        return result;
    }
}

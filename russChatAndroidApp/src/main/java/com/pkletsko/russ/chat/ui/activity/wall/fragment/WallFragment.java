package com.pkletsko.russ.chat.ui.activity.wall.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.ui.activity.common.tab.TabEnum;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.base.BaseWallPostFragment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.appendWallPost;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.parseMessage;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.putToCash;


public class WallFragment extends BaseWallPostFragment {

    private static final String TAG = WallFragment.class.getSimpleName();

    @Override
    protected Set<WallPost> getCash() {
        return globalVariable.getFollowedWallPostCash();
    }

    @Override
    protected void fragmentInitializationBody(LayoutInflater inflater, ViewGroup container,
                                              Bundle savedInstanceState) {
        if (globalVariable.getActiveTab() == null) {
            globalVariable.setActiveTab(TabEnum.TAB_WALL);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        if (visible) {
            if (globalVariable != null) {
                globalVariable.setActiveTab(TabEnum.TAB_WALL);
            }
        }
        super.setMenuVisibility(visible);
    }

    @Override
    protected Map<String, String> getRequestParameters() {
        final Map<String,String> requestParameters = new HashMap<>();
        requestParameters.put(RequestUtil.REQUEST_URL_KEY, globalVariable.getFullServicePath() + GlobalClass.CONFIG_GET_USER_RECEIVED_WALL_POSTS_URL);
        return requestParameters;
    }

    @Override
    public void removeWallPost(ArrayList<WallPost> clearedWallPosts, final int index){
        final Long wallPostId = clearedWallPosts.get(index).getWallMessageId();
        wallPostService.deleteWallPostFromMDB(wallPostId);
        removeWallPostFromCash(wallPostId);
        globalVariable.getFollowedWallPostCash().remove(clearedWallPosts.get(index));
        clearedWallPosts.remove(index);
    }

    @Override
    public void saveWallPost(final WallPost wallPost) {
        wallPost.setReceived(true);
        putToCash(wallPost, globalVariable.getWallPostCash(), globalVariable.getFollowedWallPostCash());
        wallPostService.saveWallPostToMDB(wallPost, true);
    }

    @Override
    protected void broadcastWallPostReceiverHandler(String broadcastedMessage) {
        WallPost wallPost = parseMessage(broadcastedMessage, utils);
        if (wallPost == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {
            appendWallPost(wallPost, wallPosts);
        }
    }
}

package com.pkletsko.russ.chat.ui.activity.profile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper;
import com.pkletsko.russ.chat.ui.activity.common.view.EndlessRecyclerOnScrollListener;
import com.pkletsko.russ.chat.ui.activity.profile.fragment.adapter.UserListRecyclerAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.DOWN;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.FIRST_ITEM;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.INIT_VALUE;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.UP;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.USER_LIST_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.appendUserProfiles;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.getUserProfileId;
import static com.pkletsko.russ.chat.ui.activity.common.helper.UserHelper.notifyDataSetChanged;


public class UserListFragment extends Fragment {

    private static final String TAG = UserListFragment.class.getSimpleName();

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected String targetUrl;

    protected Activity activity;

    private ArrayList<UserProfile> userProfiles = new ArrayList<>();

    private UserListRecyclerAdapter userProfileAdapter;

    protected SwipeRefreshLayout swipeContainer;
    protected RecyclerView recyclerView;
    protected LinearLayoutManager mLayoutManager;

    public final static String GET_USERS_KEY = "UserListFragment$target";

    public static UserListFragment createInstance(String target) {
        UserListFragment userListFragment = new UserListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(GET_USERS_KEY, target);
        userListFragment.setArguments(bundle);
        return userListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle!=null) {
            targetUrl = bundle.getString(GET_USERS_KEY);
        }

        activity = getActivity();

        globalVariable = (GlobalClass) activity.getApplicationContext();

        utils = new Utils(activity.getApplicationContext());

        loadFromServer(INIT_VALUE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        swipeContainer = (SwipeRefreshLayout) inflater.inflate(R.layout.user_list, container, false);

        recyclerView = (RecyclerView) swipeContainer.findViewById(R.id.recyclerView);

        mLayoutManager = new LinearLayoutManager(activity);

        recyclerView.setLayoutManager(mLayoutManager);

        userProfileAdapter = new UserListRecyclerAdapter(activity, userProfiles, globalVariable, recyclerView);

        recyclerView.setAdapter(userProfileAdapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
                mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadMoreDown();
            }
        });

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                loadMoreUp();
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return swipeContainer;
    }

    protected void loadMoreUp() {
        if (userProfiles.isEmpty()) {
            // 5 Case
            loadFromServer(WallHelper.INIT_VALUE);
        } else {
            // 6 Case
            loadFromServer(getUserProfileId(FIRST_ITEM, userProfiles), UP);
        }
    }

    protected void loadMoreDown() {
        if (userProfiles.size() > 0) {
            Log.d(TAG, "LoadMore userProfiles size = " + userProfiles.size() + ", totalItemsCount = " + userProfiles.size() + " , wallPostId = " + userProfiles.get(userProfiles.size() - 1).getUserId());
            // 4 Case
            loadFromServer(userProfiles.get(userProfiles.size() - 1).getUserId());
        }
    }

    /**
     * load received wall posts from server
     * */
    protected void loadFromServer(final Long lastLoadedWallPostId) {
        loadFromServer(lastLoadedWallPostId, DOWN);
    }
    private boolean validateCriticalParameters(GlobalClass globalVariable, Long lastLoadedUserId) {
        if (globalVariable == null ||
                globalVariable.getServerToken() == null ||
                globalVariable.getCurrentUser() == null ||
                globalVariable.getCurrentUser().getUserId() == null ||
                globalVariable.getOpenProfile() == null ||
                lastLoadedUserId == null){
            return false;
        }
        return true;
    }

    /**
     * load received wall posts from server with direction
     * */
    protected void loadFromServer(final Long lastLoadedUserId, final int direction) {
        if (validateCriticalParameters(globalVariable, lastLoadedUserId) && globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put(RequestUtil.REQUEST_URL_KEY, targetUrl);
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("id", globalVariable.getOpenProfile().toString());
            requestParameters.put("lastUserId", String.valueOf(lastLoadedUserId));
            requestParameters.put("direction", String.valueOf(direction));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        Log.d(TAG, "loadFromServer result = " + result);
                        UserProfiles userProfilesFromResponse = utils.getUserProfiles(result);
                        if (userProfilesFromResponse == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            Set<UserProfile> receivedUserProfiles = userProfilesFromResponse.getUserProfiles();
                            final int currentUserProfilesSize = userProfiles.size();
                            final int totalSize = currentUserProfilesSize + receivedUserProfiles.size();

                            for (final UserProfile userProfile : receivedUserProfiles) {
                                // save user profile in cash and mdb
                                saveUserProfile(userProfile);
                            }

                            if (totalSize > USER_LIST_SIZE_LIMIT) {
                                //remove items according to limit

                                final int removeItemsNumber = totalSize - USER_LIST_SIZE_LIMIT;
                                final ArrayList<UserProfile> clearedUserProfiles = (ArrayList<UserProfile>) userProfiles.clone();

                                userProfiles.clear();

                                if (UP == direction) {
                                    final int removeLimit = currentUserProfilesSize - removeItemsNumber;
                                    final int removeFrom = currentUserProfilesSize - 1;

                                    for (int i = removeFrom; i >= removeLimit; i--) {
                                        removeUserProfile(clearedUserProfiles, i);
                                    }

                                    appendUserProfiles(userProfiles, receivedUserProfiles, clearedUserProfiles);
                                } else {
                                    //DOWN
                                    for (int i = 0; i < removeItemsNumber; i++) {
                                        removeUserProfile(clearedUserProfiles);
                                    }
                                    appendUserProfiles(userProfiles, clearedUserProfiles, receivedUserProfiles);
                                }
                            } else {
                                appendUserProfiles(userProfiles, receivedUserProfiles);
                            }
                            notifyDataSetChanged(activity, userProfileAdapter);
                        }
                    }
                    if (swipeContainer != null) {
                        swipeContainer.setRefreshing(false);
                    }
                }
            }).request(requestParameters, globalVariable);
        }
        if (swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }
    }

    private void removeUserProfile(ArrayList<UserProfile> clearedUserProfiles){
        removeUserProfile(clearedUserProfiles, 0);
    }

    public void removeUserProfile(ArrayList<UserProfile> clearedUserProfiles, final int index){
        final Long userId = clearedUserProfiles.get(index).getUserId();
        //wallPostService.deleteWallPostFromMDB(userId);
        //removeWallPostFromCash(wallPostId);
        //globalVariable.getFollowedWallPostCash().remove(clearedWallPosts.get(index));
        //clearedWallPosts.remove(index);
    }

    public void saveUserProfile(final UserProfile userProfile) {
        //wallPost.setReceived(true);
       // putToCash(wallPost, globalVariable.getWallPostCash(), globalVariable.getFollowedWallPostCash());
        //wallPostService.saveWallPostToMDB(wallPost, true);
    }

    private void showToast(String error) {
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
    }
}

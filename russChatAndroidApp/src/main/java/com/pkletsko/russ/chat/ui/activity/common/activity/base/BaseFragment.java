package com.pkletsko.russ.chat.ui.activity.common.activity.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;

public abstract class BaseFragment extends Fragment {
    // LogCat tag
    private static final String TAG = BaseFragment.class.getSimpleName();

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected View rootView;

    protected Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.rootView = inflater.inflate(getLayoutResourceId(), container, false);

        this.globalVariable = (GlobalClass) getActivity().getApplicationContext();
        this.utils = new Utils(getActivity().getApplicationContext());

        this.client = globalVariable.getClient();

        this.intentBroadcastService = new Intent(getActivity(), BroadcastService.class);

        activity = getActivity();

        serverCheck();

        fragmentInitializationBody(inflater, container, savedInstanceState);

        return this.rootView;
    }

    protected abstract int getLayoutResourceId();

    protected abstract void fragmentInitializationBody(LayoutInflater inflater, ViewGroup container,
                                                       Bundle savedInstanceState);

    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    //basic functionality is empty, to add you code this method should be override
                } else {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(activity.getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();

                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            activity.stopService(intentBroadcastService);
                        }
                    });


                }
            }
        }).isServerAlive(globalVariable);
    }

    /**
     * Method to send message to web socket server
     */
    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }
}

package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Attachment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class AttachmentMDAO {
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_ATTACHMENT_MDB_ID,
            RussSQLiteHelper.COLUMN_ATTACHMENT_NAME,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public AttachmentMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Attachment createAttachment(final Attachment attachment, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_ATTACHMENT_NAME, attachment.getAttachmnetName());
        values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, attachment.getWallMessageId());
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
        long insertId = database.insert(RussSQLiteHelper.TABLE_ATTACHMENT, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_ATTACHMENT, allColumns, RussSQLiteHelper.COLUMN_ATTACHMENT_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Attachment newAttachment = cursorToAttachment(cursor);
        cursor.close();
        return newAttachment;
    }

    public void createAttachments(Set<Attachment> attachments) {
        for (Attachment attachment: attachments) {
            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_ATTACHMENT_NAME, attachment.getAttachmnetName());
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, attachment.getWallMessageId());
            database.insert(RussSQLiteHelper.TABLE_ATTACHMENT, null, values);
        }
    }

    public void deleteAttachment(Attachment attachment) {
        long id = attachment.getAttachmentMDBId();
        database.delete(RussSQLiteHelper.TABLE_ATTACHMENT, RussSQLiteHelper.COLUMN_ATTACHMENT_MDB_ID + " = " + id, null);
    }

    public List<Attachment> getAllAttachments() {
        List<Attachment> attachmentList = new ArrayList<Attachment>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_ATTACHMENT, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Attachment attachment = cursorToAttachment(cursor);
            attachmentList.add(attachment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return attachmentList;
    }

    public Set<Attachment> getAttachmentsByWallMessageId(final Long wallMessageId) {
        Set<Attachment> attachmentList = new TreeSet<Attachment>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_ATTACHMENT, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallMessageId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Attachment attachment = cursorToAttachment(cursor);
            attachmentList.add(attachment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return attachmentList;
    }

    public Set<String> getAttachmentNamesByWallMessageId(final Long wallMessageId, final Long appOwnerId) {
        Set<String> attachmentList = new TreeSet<String>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_ATTACHMENT, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallMessageId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            attachmentList.add(cursor.getString(1));
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return attachmentList;
    }


    private Attachment cursorToAttachment(Cursor cursor) {
        Attachment attachment = new Attachment();
        attachment.setAttachmentMDBId(cursor.getLong(0));
        attachment.setAttachmnetName(cursor.getString(1));
        attachment.setWallMessageId(cursor.getLong(2));
        return attachment;
    }
}

package com.pkletsko.russ.chat.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class RussSQLiteHelper extends SQLiteOpenHelper {
    public static final String TABLE_CONVERSATION = "conversation";
    public static final String COLUMN_CONVERSATION_MDB_ID = "_conversationMDBId";
    public static final String COLUMN_MESSAGE_MDB_ID = "_messageMDBId";
    public static final String COLUMN_CONVERSATION_ID = "conversationId";
    public static final String COLUMN_CONVERSATION_NAME = "conversationName";
    public static final String COLUMN_CONVERSATION_ICON = "conversationIcon";

    public static final String TABLE_USER = "user";
    public static final String COLUMN_USER_MDB_ID = "_userMDBId";
    public static final String COLUMN_USER_ID = "userId";
    public static final String COLUMN_USER_NAME = "userName";
    public static final String COLUMN_USER_PROFILE_ICON = "userProfileIcon";
    public static final String COLUMN_USER_PROFILE_VERSION = "userProfileVersion";
    public static final String COLUMN_USER_CURRENT_FLAG = "userLoggedIn";
    public static final String COLUMN_USER_LOGIN_SRC = "userLoginSrc";
    public static final String COLUMN_USER_FOLLOW = "userLoginFollow";

    public static final String TABLE_MESSAGE = "message";
    public static final String COLUMN_MESSAGE_ID = "messageId";
    public static final String COLUMN_MESSAGE_TEXT = "messageText";
    public static final String COLUMN_MESSAGE_FROM_USER_NAME = "fromUserName";
    public static final String COLUMN_MESSAGE_FROM_USER_ID = "fromUserId";

    public static final String TABLE_WALL_MESSAGE = "wallmessage";
    public static final String COLUMN_WALL_MESSAGE_MDB_ID = "_wallMessageMDBId";
    public static final String COLUMN_WALL_MESSAGE_ID = "wallMessageId";
    public static final String COLUMN_WALL_MESSAGE_TEXT = "messageText";
    public static final String COLUMN_WALL_MESSAGE_FROM_USER_ID = "fromUserId";
    public static final String COLUMN_WALL_ID = "wallId";
    public static final String COLUMN_LIKE_NUMBER = "likeNumber";
    public static final String COLUMN_COMMENT_NUMBER = "commentNumber";
    public static final String COLUMN_LIKED_BY_ME = "liked";
    public static final String COLUMN_WALL_POST_ATTACHMENT = "attachment";
    public static final String COLUMN_WALL_POST_RECEIVED = "received";
    public static final String COLUMN_WALL_POST_CREATED_DATE_TIME = "wallPostCreated";


    public static final String TABLE_COMMENT = "comment";
    public static final String COLUMN_COMMENT_MDB_ID = "_commentMDBId";
    public static final String COLUMN_COMMENT_ID = "commentId";
    public static final String COLUMN_COMMENT_TEXT = "commentText";
    public static final String COLUMN_COMMENT_FROM_USER_ID = "fromUserId";

    public static final String TABLE_LIKE = "like";
    public static final String COLUMN_LIKE_MDB_ID = "_likeMDBId";
    public static final String COLUMN_LIKE_ID = "likeId";
    public static final String COLUMN_LIKE_FROM_USER_ID = "fromUserId";

    public static final String TABLE_ATTACHMENT = "attachment";
    public static final String COLUMN_ATTACHMENT_MDB_ID = "_attachmentMDBId";
    public static final String COLUMN_ATTACHMENT_NAME = "attachmentName";

    public static final String TABLE_IP_ADDRESS = "ip";
    public static final String COLUMN_IP_ADDRESS_MDB_ID = "_ipMDBId";
    public static final String COLUMN_IP_ADDRESS = "ipAddress";

    public static final String TABLE_UNLOADED_ATTACHMENT = "unloadedAttachment";
    public static final String COLUMN_UNLOADED_ATTACHMENT_MDB_ID = "_unloadedAttachmentMDBId";
    public static final String COLUMN_UNLOADED_ATTACHMENT_PATH = "attachmentPath";
    public static final String COLUMN_UNLOADED_ATTACHMENT_ID = "attachmentId";

    public static final String TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK = "attachmentWallPostLink";
    public static final String COLUMN_UNLOADED_ATTACHMENT_WALLPOST_LINK_MDB_ID = "_attachmentWallPostLinkMDBId";

    public static final String COLUMN_APP_OWNER_ID = "appOwnerId";

    private static final String DATABASE_NAME = "russchat.db";
    private static final int DATABASE_VERSION = 2;

    // Database creation sql statement
    private static final String DATABASE_CREATE_TABLE_CONVERSATION = "create table "
            + TABLE_CONVERSATION + "(" + COLUMN_CONVERSATION_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_CONVERSATION_ID
            + " integer not null, " + COLUMN_CONVERSATION_NAME
            + " text not null, " + COLUMN_CONVERSATION_ICON
            + " text not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_USER = "create table "
            + TABLE_USER + "(" + COLUMN_USER_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_USER_ID
            + " integer not null, " + COLUMN_USER_PROFILE_VERSION
            + " integer not null, " + COLUMN_USER_NAME
            + " text not null, " + COLUMN_USER_PROFILE_ICON
            + " text not null, " + COLUMN_USER_CURRENT_FLAG
            + " text not null, " + COLUMN_APP_OWNER_ID
            + " integer not null, " + COLUMN_USER_LOGIN_SRC
            + " integer not null, " + COLUMN_USER_FOLLOW
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_MESSAGE = "create table "
            + TABLE_MESSAGE + "(" + COLUMN_MESSAGE_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_MESSAGE_TEXT
            + " text not null, " + COLUMN_MESSAGE_ID
            + " integer not null, " + COLUMN_CONVERSATION_ID
            + " integer not null, " + COLUMN_MESSAGE_FROM_USER_ID
            + " integer not null, " + COLUMN_MESSAGE_FROM_USER_NAME
            + " text not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_WALL_MESSAGE = "create table "
            + TABLE_WALL_MESSAGE + "(" + COLUMN_WALL_MESSAGE_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_WALL_MESSAGE_TEXT
            + " text not null, " + COLUMN_WALL_MESSAGE_ID
            + " integer not null, " + COLUMN_WALL_ID
            + " integer not null, " + COLUMN_WALL_MESSAGE_FROM_USER_ID
            + " integer not null, " + COLUMN_LIKE_NUMBER
            + " integer not null, " + COLUMN_COMMENT_NUMBER
            + " integer not null, " + COLUMN_LIKED_BY_ME
            + " integer not null, " + COLUMN_WALL_POST_ATTACHMENT
            + " integer not null, " + COLUMN_APP_OWNER_ID
            + " integer not null, " + COLUMN_WALL_POST_RECEIVED
            + " integer not null, " + COLUMN_WALL_POST_CREATED_DATE_TIME
            + " string);";

    private static final String DATABASE_CREATE_TABLE_COMMENT = "create table "
            + TABLE_COMMENT + "(" + COLUMN_COMMENT_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_COMMENT_TEXT
            + " text not null, " + COLUMN_WALL_MESSAGE_ID
            + " integer not null, " + COLUMN_COMMENT_ID
            + " integer not null, " + COLUMN_COMMENT_FROM_USER_ID
            + " integer not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_LIKE = "create table "
            + TABLE_LIKE + "(" + COLUMN_LIKE_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_LIKE_ID
            + " integer not null, " + COLUMN_WALL_MESSAGE_ID
            + " integer not null, " + COLUMN_LIKE_FROM_USER_ID
            + " integer not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_ATTACHMENT = "create table "
            + TABLE_ATTACHMENT + "(" + COLUMN_ATTACHMENT_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_ATTACHMENT_NAME
            + " text not null, " + COLUMN_WALL_MESSAGE_ID
            + " integer not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_IP = "create table "
            + TABLE_IP_ADDRESS + "(" + COLUMN_IP_ADDRESS_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_IP_ADDRESS
            + " text not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_UNLOADED_ATTACHMENT = "create table "
            + TABLE_UNLOADED_ATTACHMENT + "(" + COLUMN_UNLOADED_ATTACHMENT_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_ATTACHMENT_NAME
            + " text not null, " + COLUMN_UNLOADED_ATTACHMENT_PATH
            + " text not null, " + COLUMN_UNLOADED_ATTACHMENT_ID
            + " integer not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    private static final String DATABASE_CREATE_TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK = "create table "
            + TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK + "(" + COLUMN_UNLOADED_ATTACHMENT_WALLPOST_LINK_MDB_ID
            + " integer primary key autoincrement, " + COLUMN_UNLOADED_ATTACHMENT_ID
            + " integer not null, " + COLUMN_WALL_MESSAGE_ID
            + " integer not null, " + COLUMN_APP_OWNER_ID
            + " integer not null);";

    public RussSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE_TABLE_CONVERSATION);
        database.execSQL(DATABASE_CREATE_TABLE_MESSAGE);
        database.execSQL(DATABASE_CREATE_TABLE_USER);

        database.execSQL(DATABASE_CREATE_TABLE_WALL_MESSAGE);
        database.execSQL(DATABASE_CREATE_TABLE_COMMENT);
        database.execSQL(DATABASE_CREATE_TABLE_LIKE);
        database.execSQL(DATABASE_CREATE_TABLE_ATTACHMENT);
        database.execSQL(DATABASE_CREATE_TABLE_IP);

        database.execSQL(DATABASE_CREATE_TABLE_UNLOADED_ATTACHMENT);
        database.execSQL(DATABASE_CREATE_TABLE_UNLOADED_ATTACHMENT_WALLPOST_LINK);
    }

    private static final String DATABASE_ALTER_WALL_POST_CREATED = "ALTER TABLE "
            + TABLE_WALL_MESSAGE + " ADD COLUMN " + COLUMN_WALL_POST_CREATED_DATE_TIME + " string;";

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            db.execSQL(DATABASE_ALTER_WALL_POST_CREATED);
        }
    }

}

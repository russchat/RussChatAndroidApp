package com.pkletsko.russ.chat.ui.activity.common.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.pkletsko.russ.chat.ui.activity.common.view.HorizontalListView;

/**
 * Created by pkletsko on 13.04.2015.
 */
public class CustomViewPager extends android.support.v4.view.ViewPager{
    private boolean enabled;
    // LogCat tag
    private static final String TAG = CustomViewPager.class.getSimpleName();

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if (v instanceof HorizontalListView) {
            if ((((HorizontalListView) v).mCurrentX) != 0 && (((HorizontalListView) v).mNextX != ((HorizontalListView) v).mMaxX) ) {
                //Log.d(TAG, "canScroll: HorizontialListView =  true");
                return true;
            }
            //Log.d(TAG, "canScroll: HorizontialListView =  end of Viewer should be next tab");
        } else {
            //Log.d(TAG, "canScroll: NOT  HorizontialListView" );
        }

        return super.canScroll(v, checkV, dx, x, y);
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    public boolean getPagingEnabled() {
        return enabled;
    }

}

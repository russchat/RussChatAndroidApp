package com.pkletsko.russ.chat.ui.activity.wall.post.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Image;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.util.ArrayList;

public class WallPostUploadImageListAdapter extends BaseAdapter {

    // Profile pic image size in pixels
    private Activity activity;
    private ArrayList<Image> images;
    private static LayoutInflater inflater = null;
    private GlobalClass globalVariable;
   // private ImageLoader imageLoader;

    // LogCat tag
    private static final String TAG = WallPostUploadImageListAdapter.class.getSimpleName();

    public WallPostUploadImageListAdapter(Activity activity, ArrayList<Image> images, GlobalClass globalVariable) {
        this.activity = activity;
        this.images = images;
        this.globalVariable = globalVariable;
       // imageLoader = new ImageLoader(activity.getApplicationContext());
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Image image = images.get(position);

        //UI initialization
        View vi = convertView;

        if(convertView == null) {
            vi = inflater.inflate(R.layout.wall_edit_image_item, null);
        }

        final ImageView wallPostImage = (ImageView) vi.findViewById(R.id.wallPostImage);

        final TextView wallPostOwnerNameTextView = (TextView) vi.findViewById(R.id.removeLink);

        wallPostOwnerNameTextView.setText("remove");
        wallPostOwnerNameTextView.setTag(image);
        wallPostImage.setTag(image);

        //wallPostImage.setImageURI(image.getUri());
        //loadImageFromServer(image, wallPostImage);
        //Picasso.with(activity).load(image.getPath()).into(wallPostImage);

        Transformation transformation = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = wallPostImage.getWidth();
                if(targetWidth == 0) {
                    targetWidth=100;
                }

                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };

        Uri uri = Uri.fromFile(new File(image.getPath()));

        Picasso.with(activity)
                .load(uri).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .transform(transformation)
                .into(wallPostImage);


        wallPostOwnerNameTextView.setOnClickListener(mOnButtonClicked);

        return vi;
    }
//
//    private void loadImageFromServer(final Image image, final ImageView wallPostImage) {
//        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... params) {
//                String accessToken = null;
//
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        if (image.getPath() != null) {
//                            //mImageFetcher.loadImage("file://"+image.getUri().getPath(), wallPostImage);
//                            Log.i(TAG, "loadImageFromServer: Path = " + image.getPath());
//                            imageLoader.DisplayImage(image.getPath(), wallPostImage, 70, false);
//                        } else if (image.getUrl() != null) {
//                            //mImageFetcher.loadImage(image.getUrl(), wallPostImage);
//                            Log.i(TAG, "loadImageFromServer: url = " + image.getUrl());
//                            imageLoader.DisplayImage(image.getUrl(), wallPostImage, 70, false);
//                        }
//
//                    }
//                });
//
//                return accessToken;
//            }
//
//            @Override
//            protected void onPostExecute(String accessToken) {
//                //Log.i(TAG, "POSSSSST Access token retrieved:");
//            }
//
//        };
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        } else {
//            task.execute();
//        }
//    }

    private View.OnClickListener mOnButtonClicked = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Image image1 = (Image)v.getTag();
            images.remove(image1);

            //remove image from upload queue
            globalVariable.getQueueToUploadFiles().get(image1.getAttachmentId()).remove(image1.getFileToUpload());

            notifyDataSetChanged();
        }
    };
}

package com.pkletsko.russ.chat.ui.activity.common.img.model;

import com.pkletsko.russ.chat.websocket.message.json.model.wall.Image;

import java.util.ArrayList;

/**
 * Created by pkletsko on 14.04.2015.
 */
public class FullScreenViewDataModel {

    private ArrayList<Image> images;
    private int position;

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}

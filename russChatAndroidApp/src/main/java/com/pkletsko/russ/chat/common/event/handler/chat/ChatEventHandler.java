package com.pkletsko.russ.chat.common.event.handler.chat;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.ChatService;
import com.pkletsko.russ.chat.storage.service.ChatServiceImpl;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.NewChat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.UnReceivedChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.Set;

/**
 * Created by pkletsko on 26.05.2015.
 */
public class ChatEventHandler {

    private ChatService chatService;

    private UserService userService;

    private GlobalClass globalVariable;

    private Utils utils;

    private Context context;

    private Intent intentActiveConversation;
    private Intent intentConversationList;
    private Intent intentProfile;
    private Intent intentNewConversation;

    public ChatEventHandler(final GlobalClass globalVariable, final Utils utils, final Context context) {
        this.chatService = new ChatServiceImpl(globalVariable);
        this.userService = new UserServiceImpl(globalVariable, utils);
        this.globalVariable = globalVariable;
        this.utils = utils;
        this.context = context;

        this.intentActiveConversation = new Intent(EventHub.BROADCAST_ACTIVE_CONVERSATION_UPDATE);
        this.intentNewConversation = new Intent(EventHub.BROADCAST_CREATE_NEW_CONVERSATION);
        this.intentConversationList = new Intent(EventHub.BROADCAST_CONVERSATION_LIST_UPDATE);
        this.intentProfile = new Intent(EventHub.BROADCAST_PROFILE_WALL_UPDATE);
    }

    public void unreceivedChatMessagesEventHandler(final String msg) {
        if (globalVariable.getCurrentUser() != null) {
            // async get data from database and after set data to global variable and than call redirect to login activity
            UnReceivedChatMessages unReceivedChatMessages = utils.getUnReceivedChatMessages(msg);
            if (unReceivedChatMessages == null) {
                showToast("Something went wrong. Bad response from server.");
            } else {
                chatService.initializationOfApplicationData(unReceivedChatMessages);
            }
        }
    }

    public void conversationEventHandler(final String msg) {
        NewChat newChat = utils.getNewConversationResponse(msg);
        if (newChat == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {

            // set correct conversation name
            if (newChat.getChat().getUsers().size() == 2) {
                for (UserProfile userProfile : newChat.getChat().getUsers()) {
                    if (userProfile.getUserId() != globalVariable.getCurrentUser().getUserId()) {
                        newChat.getChat().setConversationName(userProfile.getFullName());
                        newChat.getChat().setConversationIcon(userProfile.getProfileIcon());
                    }
                }
            } else {
                newChat.getChat().setConversationIcon("https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg");
                //TODO for group chat
            }

            // 1 - save conversation to phone database
            chatService.saveConversationToMDB(newChat.getChat());

            userService.storeUsersToMDB(newChat.getChat().getUsers());

            // 2 - put to conversation list
            globalVariable.addToConversationCash(newChat.getChat());

            if (newChat.getUserProfile() != null && newChat.getUserProfile().getUserId() == globalVariable.getCurrentUser().getUserId()) {
                globalVariable.setActiveConversation(newChat.getChat());
                //if this user created this chat redirect him to chat activity
                intentProfile.putExtra("message", msg);
                context.sendBroadcast(intentProfile);

                context.sendBroadcast(intentNewConversation);
            }
            intentConversationList.putExtra("message", utils.getNewChatJSON(newChat));
            context.sendBroadcast(intentConversationList);
        }
    }

    public void chatMessageEventHandler(final String msg) {
        ChatMessage chatMessage = utils.getChatMessage(msg);
        if (chatMessage == null) {
            showToast("Something went wrong. Bad response from server.");
        } else {

            reNewConversation(chatMessage.getConversationId());
            // 1 - save message to phone database
            chatService.saveMessageToMDB(chatMessage);

            // 2 - put new message to cash
            final Set<ChatMessage> messagesCashSet = globalVariable.getMessageCash().get(chatMessage.getConversationId());
            if (messagesCashSet != null) {
                globalVariable.getMessageCash().get(chatMessage.getConversationId()).add(chatMessage);
            }

            final Chat activeConversation = globalVariable.getActiveConversation();
            if (activeConversation != null && activeConversation.getConversationId() == chatMessage.getConversationId()) {
                intentActiveConversation.putExtra("message", msg);
                context.sendBroadcast(intentActiveConversation);
            } else {
                if (globalVariable.getUnreadMessagesCash().get(chatMessage.getConversationId()) == null) {
                    globalVariable.getUnreadMessagesCash().put(chatMessage.getConversationId(), 1);
                } else {
                    Integer newNumber = globalVariable.getUnreadMessagesCash().get(chatMessage.getConversationId()) + 1;
                    globalVariable.getUnreadMessagesCash().put(chatMessage.getConversationId(), newNumber);
                }
                //TODO what happaned when conversation was not received and message was sent
                context.sendBroadcast(intentConversationList);
            }
        }
    }

    private void showToast(String error) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }

    private void reNewConversation(final Long conversationId) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                if (globalVariable.getConversationMDAO().getConversationById(conversationId, globalVariable.getCurrentUser().getUserId()) == null) {
                    sendMessageToServer(utils.getSendReNewConversationJSON(conversationId));
                }
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Method to send message to web socket server
     */
    private void sendMessageToServer(String message) {
        if (globalVariable.getClient() != null && globalVariable.getClient().isConnected()) {
            globalVariable.getClient().send(message);
        }
    }
}

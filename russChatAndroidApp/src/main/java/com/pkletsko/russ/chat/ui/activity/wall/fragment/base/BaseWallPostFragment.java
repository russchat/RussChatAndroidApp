package com.pkletsko.russ.chat.ui.activity.wall.fragment.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.other.RequestUtil;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.storage.service.WallPostService;
import com.pkletsko.russ.chat.storage.service.WallPostServiceImpl;
import com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper;
import com.pkletsko.russ.chat.ui.activity.common.view.EndlessRecyclerOnScrollListener;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.adapter.WallPostAdapter;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPost;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.WallPosts;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.DOWN;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.FIRST_ITEM;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.INIT_VALUE;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.UP;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.WALL_POSTS_SIZE_LIMIT;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.addTempUser;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.appendWallPosts;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.getWallPostId;
import static com.pkletsko.russ.chat.ui.activity.common.helper.WallHelper.notifyWallChanged;

public abstract class BaseWallPostFragment extends Fragment {

    private static final String TAG = BaseWallPostFragment.class.getSimpleName();

    protected Activity activity;

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected UserService userService;

    protected WallPostService wallPostService;

    protected SwipeRefreshLayout swipeContainer;
    protected RecyclerView recyclerView;
    protected LinearLayoutManager mLayoutManager;

    protected ArrayList<WallPost> wallPosts = new ArrayList<>();
    protected WallPostAdapter wallPostAdapter;

    /**
     * Initialization of common wall data.
     * Next list of cases will be covered here:
     * 1 Case : when a user log in (first time/N time) and there is no wall posts stored yet in MDB, so cash is empty.
     *          - in this case request with default parameter 0L will be send to server to get first (N) wall posts if exist.
     * 2 Case : when a user log in (second time/N time) and there is already wall posts stored in MDB, so cash is not empty.
     *          - in this case just load wall posts fro cash to UI
     * 3 Case : in case number of cashed wall posts less than LIMIT ,
     *          a new request with last loaded wall post id will be send to server to get additional wall posts.
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();

        globalVariable = (GlobalClass) activity.getApplicationContext();

        utils = new Utils(activity.getApplicationContext());

        client = globalVariable.getClient();

        userService = new UserServiceImpl(globalVariable, utils);

        wallPostService = new WallPostServiceImpl(globalVariable, userService, activity.getApplicationContext());

        intentBroadcastService = new Intent(activity, BroadcastService.class);

        wallPostAdapter = new WallPostAdapter(getActivity(), wallPosts, globalVariable);

        final Set<WallPost> cashedWallPosts = getCash();

        if (cashedWallPosts.isEmpty()) {
            // 1 Case
            loadFromServer(INIT_VALUE);
        } else {
            // 2 Case
            appendWallPosts(wallPosts, cashedWallPosts);
            notifyWallChanged(getActivity(), wallPostAdapter);

            // 3 Case
            if (cashedWallPosts.size() < WALL_POSTS_SIZE_LIMIT) {
                loadFromServer(getWallPostId(wallPosts.size() - 1, wallPosts));
            }
        }
    }

    protected abstract Set<WallPost> getCash();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        swipeContainer = (SwipeRefreshLayout) inflater.inflate(R.layout.wall_post_history, container, false);

        recyclerView = (RecyclerView) swipeContainer.findViewById(R.id.recyclerView);

        mLayoutManager = new LinearLayoutManager(activity);

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(wallPostAdapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(
                mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadMoreDown();
            }
        });

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                loadMoreUp();
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        serverCheck();

        fragmentInitializationBody(inflater, container, savedInstanceState);

        return swipeContainer;
    }

    /**
     * Re Initialization of common wall data. Happens each time we come back from other Activity
     * Next list of cases will be covered here:
     * 5 Case : when a user scroll up and there is no wall posts loaded yet.
     *          - in this case request with default parameter 0L will be send to server to get first (N) wall posts if exist.
     * 6 Case : when a user scroll up and there is already some wall posts loaded.
     *          a new request with last loaded wall post id will be send to server to get additional wall posts.
     * */
    protected void loadMoreUp() {
        if (wallPosts.isEmpty()) {
            // 5 Case
            loadFromServer(INIT_VALUE);
        } else {
            // 6 Case
            loadFromServer(getWallPostId(FIRST_ITEM, wallPosts), UP);
        }
    }

    /**
     * Re Initialization of common wall data. Happens each time we come back from other Activity
     * Next list of cases will be covered here:
     * 4 Case : when a user scroll down.
     *          - in this case request with parameter (last wall post id from UI list) will be send to server to get additional (N) wall posts if exist.
     * */
    protected void loadMoreDown() {
        if (wallPosts.size() > 0) {
            Log.d(TAG, "LoadMore wallPosts size = " + wallPosts.size() + ", totalItemsCount = " + wallPosts.size() + " , wallPostId = " + wallPosts.get(wallPosts.size() - 1).getWallMessageId());
            // 4 Case
            loadFromServer(getWallPostId(wallPosts.size() - 1, wallPosts));
        }
    }

    protected void fragmentInitializationBody(LayoutInflater inflater, ViewGroup container,
                                              Bundle savedInstanceState) {
        //nothing by default
    }

    @Override
    public void onResume() {
        super.onResume();

        //TODO when come back from other places all visible items on wall/my wall should be updated
        //number of likes , number of comments

        getActivity().startService(intentBroadcastService);
        getActivity().registerReceiver(broadcastWallPostReceiver, new IntentFilter(EventHub.BROADCAST_ACTIVE_WALL_UPDATE));
        getActivity().registerReceiver(broadcastMyWallPostReceiver, new IntentFilter(EventHub.BROADCAST_ACTIVE_MY_WALL_UPDATE));
        getActivity().registerReceiver(broadcastNotifyLikeChanged, new IntentFilter(EventHub.BROADCAST_NOTIFY_LIKE_CHANGED));
        getActivity().registerReceiver(broadcastNotifyCommentChanged, new IntentFilter(EventHub.BROADCAST_NOTIFY_COMMENT_CHANGED));
        getActivity().registerReceiver(broadcastNotifyUploadProgressBarChanged, new IntentFilter(EventHub.BROADCAST_NOTIFY_UPLOAD_PROGRESS_BAR_CHANGED));
        getActivity().registerReceiver(broadcastNotifyUploadCompleted, new IntentFilter(EventHub.BROADCAST_NOTIFY_UPLOAD_COMPLETED));
    }

    @Override
    public void onPause() {
        //getActivity().unregisterReceiver(broadcastWallPostReceiver);
        //getActivity().unregisterReceiver(broadcastMyWallPostReceiver);
        //getActivity().unregisterReceiver(broadcastNotifyLikeChanged);
        //getActivity().unregisterReceiver(broadcastNotifyCommentChanged);
        super.onPause();
    }

    protected BroadcastReceiver broadcastNotifyUploadProgressBarChanged = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long wallPostId = intent.getLongExtra("wallPostId", -1L);

            for (int i = 0; i < wallPosts.size(); i++) {
                final int index = i;
                if (wallPosts.get(i).getWallMessageId() == wallPostId) {

                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                                //wallPostAdapter.getViewHolder(index).progressBar.setVisibility(View.GONE);
                                //wallPostAdapter.getViewHolder(index).progressStatusTextView.setVisibility(View.GONE);
                            if (wallPostAdapter.getViewHolder(index) != null) {
                                wallPostAdapter.getViewHolder(index).progressStatus = wallPostAdapter.getViewHolder(index).progressStatus++;
                            }
                               // wallPostAdapter.getViewHolder(index).progressBar.incrementProgressBy(1);
                                //wallPostAdapter.getViewHolder(index).progressStatusTextView.setText(progressStatus + "/" + wallPostAdapter.getViewHolder(index).progressBar.getMax());
                                //wallPostAdapter.getViewHolder(index).progressBar.setVisibility(View.VISIBLE);
                                //wallPostAdapter.getViewHolder(index).progressStatusTextView.setVisibility(View.VISIBLE);
                        }
                    });

                }
            }
        }
    };

    protected BroadcastReceiver broadcastNotifyUploadCompleted = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long wallPostId = intent.getLongExtra("wallPostId", -1L);

            for (int i = 0; i < wallPosts.size(); i++) {
                if (wallPosts.get(i).getWallMessageId() == wallPostId) {
                    // TODO find what item have to be updated
                    globalVariable.getWallPostLocalImages().remove(wallPostId);
                    WallHelper.notifyWallItemChanged(getActivity(), wallPostAdapter, i);
                    //wallPostAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    protected BroadcastReceiver broadcastWallPostReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String broadcastedMessage = intent.getStringExtra("message");

            Log.d(TAG, "BroadcastReceiver of Wall: broadcastedMessage = " + broadcastedMessage);

            broadcastWallPostReceiverHandler(broadcastedMessage);
            notifyWallChanged(getActivity(), wallPostAdapter);
            globalVariable.notifycation("RussChat", "New Wall Post Received");
        }
    };

    protected BroadcastReceiver broadcastMyWallPostReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String broadcastedMessage = intent.getStringExtra("message");

            Log.d(TAG, "BroadcastReceiver of My Wall: broadcastedMessage = " + broadcastedMessage);

            broadcastMyWallPostReceiverHandler(broadcastedMessage);
            notifyWallChanged(getActivity(), wallPostAdapter);
        }
    };

    protected void broadcastMyWallPostReceiverHandler(String broadcastedMessage) {}
    protected void broadcastWallPostReceiverHandler(String broadcastedMessage) {}

    protected BroadcastReceiver broadcastNotifyLikeChanged = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long wallPostId = intent.getLongExtra("wallPostId", -1L);
            final int likesNumber = intent.getIntExtra("likesNumber", -1);

            for (int i = 0; i < wallPosts.size(); i++) {
                final int index = i;
                if (wallPosts.get(i).getWallMessageId() == wallPostId && wallPostAdapter.getViewHolder(i) != null) {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            wallPostAdapter.getViewHolder(index).likeNumberTextView.setVisibility(View.GONE);
                            wallPostAdapter.getViewHolder(index).likeNumberTextView.setText(String.valueOf(likesNumber));
                            wallPostAdapter.getViewHolder(index).likeNumberTextView.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
            globalVariable.notifycation("RussChat", "New Like Received");
        }
    };

    protected BroadcastReceiver broadcastNotifyCommentChanged = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long wallPostId = intent.getLongExtra("wallPostId", -1L);
            int commentsNumber = intent.getIntExtra("commentsNumber", -1);

            for (int i = 0; i < wallPosts.size(); i++) {
                if (wallPosts.get(i).getWallMessageId() == wallPostId && wallPostAdapter.getViewHolder(i) != null) {
                    wallPostAdapter.getViewHolder(i).commentNumberTextView.setVisibility(View.GONE);
                    wallPostAdapter.getViewHolder(i).commentNumberTextView.setText(String.valueOf(commentsNumber));
                    wallPostAdapter.getViewHolder(i).commentNumberTextView.setVisibility(View.VISIBLE);
                }
            }
            globalVariable.notifycation("RussChat", "New Comment Received");
        }
    };

    protected void serverCheck() {
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    //basic functionality is empty, to add you code this method should be override
                } else {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(activity.getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            activity.stopService(intentBroadcastService);
                        }
                    });
                }
            }
        }).isServerAlive(globalVariable);
    }

    /**
     * Method to send message to web socket server
     */
    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }

    protected abstract Map<String, String> getRequestParameters() ;

    /**
     * load received wall posts from server
     * */
    protected void loadFromServer(final Long lastLoadedWallPostId) {
        loadFromServer(lastLoadedWallPostId, DOWN);
    }

    /**
     * load received wall posts from server with direction
     * */
    protected void loadFromServer(final Long lastLoadedWallPostId, final int direction) {
        if (globalVariable.isAppRunning()) {
            final Map<String, String> requestParameters = getRequestParameters();
            requestParameters.put("token", globalVariable.getServerToken());
            requestParameters.put("userId", globalVariable.getCurrentUser().getUserId().toString());
            requestParameters.put("postId", String.valueOf(lastLoadedWallPostId));
            requestParameters.put("direction", String.valueOf(direction));

            (new RequestUtil() {
                @Override
                public void onPostRequestResponse(String result) {
                    if (result != null && !result.isEmpty()) {
                        WallPosts wallPostsFromResponse = utils.getUserWallPostsResponse(result);
                        if (wallPostsFromResponse == null) {
                            showToast("Something went wrong. Bad response from server.");
                        } else {
                            final Set<WallPost> receivedWallPosts = wallPostsFromResponse.getWallPosts();
                            final int currentWallSize = wallPosts.size();
                            final int totalSize = currentWallSize + receivedWallPosts.size();

                            for (final WallPost wallPost : receivedWallPosts) {
                                // in case information about the user is not loaded yet
                                addTempUser(wallPost.getFromUserId(), globalVariable);

                                // save wall post in cash and mdb
                                saveWallPost(wallPost);
                            }

                            if (totalSize > WALL_POSTS_SIZE_LIMIT) {
                                //remove items according to limit

                                final int removeItemsNumber = totalSize - WALL_POSTS_SIZE_LIMIT;
                                final ArrayList<WallPost> clearedWallPosts = (ArrayList<WallPost>) wallPosts.clone();

                                wallPosts.clear();

                                if (UP == direction) {
                                    final int removeLimit = currentWallSize - removeItemsNumber;
                                    final int removeFrom = currentWallSize - 1;

                                    for (int i = removeFrom; i >= removeLimit; i--) {
                                        removeWallPost(clearedWallPosts, i);
                                    }

                                    appendWallPosts(wallPosts, receivedWallPosts, clearedWallPosts);
                                } else {
                                    //DOWN
                                    for (int i = 0; i < removeItemsNumber; i++) {
                                        removeWallPost(clearedWallPosts);
                                    }
                                    appendWallPosts(wallPosts, clearedWallPosts, receivedWallPosts);
                                }
                            } else {
                                appendWallPosts(wallPosts, receivedWallPosts);
                            }
                            notifyWallChanged(activity, wallPostAdapter);
                        }
                    }
                    swipeContainer.setRefreshing(false);
                }
            }).request(requestParameters, globalVariable);
        } else {
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    protected void removeWallPostFromCash(final Long wallPostId) {
        globalVariable.getWallPostCash().remove(wallPostId);
    }

    private void removeWallPost(ArrayList<WallPost> clearedWallPosts){
        removeWallPost(clearedWallPosts, 0);
    }

    protected abstract void removeWallPost(ArrayList<WallPost> clearedWallPosts, final int index);

    protected abstract  void saveWallPost(final WallPost wallPost);

    public void showToast(String error) {
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
    }
}

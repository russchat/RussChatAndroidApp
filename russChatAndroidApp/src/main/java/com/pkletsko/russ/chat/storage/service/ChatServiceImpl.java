package com.pkletsko.russ.chat.storage.service;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ChatMessage;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.UnReceivedChatMessages;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.Set;

/**
 * Created by pkletsko on 25.05.2015.
 */
public class ChatServiceImpl implements ChatService {

    private static final String TAG = ChatServiceImpl.class.getSimpleName();

    private GlobalClass globalVariable;

    public ChatServiceImpl(GlobalClass globalVariable) {
        this.globalVariable = globalVariable;
    }

    public void saveMessageToMDB(final ChatMessage message) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                globalVariable.getMessageMDAO().createMessage(message, globalVariable.getCurrentUser().getUserId());
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void saveConversationToMDB(final Chat newConversation) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                if (globalVariable.getConversationMDAO().getConversationById(newConversation.getConversationId(), globalVariable.getCurrentUser().getUserId()) == null) {
                    globalVariable.getConversationMDAO().createConversation(newConversation, globalVariable.getCurrentUser().getUserId());
                }
                return "OK";
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    public void storeUnReceivedChatMessagesToMDB(final UnReceivedChatMessages unReceivedChatMessages) {
        globalVariable.getMessageMDAO().createChatMessages(unReceivedChatMessages.getChatMessages(), globalVariable.getCurrentUser().getUserId());

        for (Chat chat : unReceivedChatMessages.getChats()) {
            if (globalVariable.getConversationMDAO().getConversationById(chat.getConversationId(), globalVariable.getCurrentUser().getUserId()) == null) {
                if (chat.getUsers().size() == 2) {
                    for (UserProfile userProfile : chat.getUsers()) {
                        if (userProfile.getUserId() != globalVariable.getCurrentUser().getUserId()) {
                            chat.setConversationName(userProfile.getFullName());
                            chat.setConversationIcon(userProfile.getProfileIcon());
                        }
                    }
                } else {
                    chat.setConversationIcon("https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg");
                    //TODO for group chat
                }

                globalVariable.getConversationMDAO().createConversation(chat, globalVariable.getCurrentUser().getUserId());
            }
        }
    }

    public void initializationOfApplicationData(final UnReceivedChatMessages unReceivedChatMessages) {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String result = null;
                storeUnReceivedChatMessagesToMDB(unReceivedChatMessages);

                for (Chat conversationUI : unReceivedChatMessages.getChats()) {
                    globalVariable.getUnreadMessagesCash().put(conversationUI.getConversationId(), conversationUI.getNewChatMessagesNumber());
                }

                //set to global variable
                Set<Chat> conversations = globalVariable.getConversationMDAO().getAllConversationsByOwnerId(globalVariable.getCurrentUser().getUserId());
                if (conversations != null) {
                    for (Chat conversation : conversations) {
                        globalVariable.getChatCash().put(conversation.getConversationId(), conversation);
                        globalVariable.getConversationCash().add(conversation);
                    }
                }

                for (UserProfile userProfile : globalVariable.getUserMDAO().getAllUsersByAppOwnerId(globalVariable.getCurrentUser().getUserId())) {
                    globalVariable.addUserProfileToCash(userProfile);
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                Log.i(TAG, "result:" + result);
            }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }
}

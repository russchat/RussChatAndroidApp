package com.pkletsko.russ.chat.ui.activity.common.activity.base;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.codebutler.android_websockets.WebSocketClient;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.BroadcastService;
import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.ServerCheckUtil;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.storage.service.UserService;
import com.pkletsko.russ.chat.storage.service.UserServiceImpl;
import com.pkletsko.russ.chat.ui.activity.login.SplashActivity;

/**
 * Created by pkletsko on 29.05.2015.
 */
public abstract class BaseActivity extends SlidingFragmentActivity {
    // LogCat tag
    private static final String TAG = BaseActivity.class.getSimpleName();

    protected GlobalClass globalVariable;
    protected Utils utils;
    protected WebSocketClient client;

    protected Intent intentBroadcastService;

    protected ActionBar actionBar;

    protected UserService userService;

    protected Long currentUserId;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        beforeSetContentView();
        setContentView(getLayoutResourceId());

        Log.d(TAG, "Initialization of Base Activity");

        this.actionBar = getActionBar();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.statusbarred));
        }

        this.actionBar.setDisplayHomeAsUpEnabled(true); // Enabling Back navigation on Action Bar icon

        this.globalVariable = (GlobalClass) getApplicationContext();
        restartAppAfterSleepMode();
        if (globalVariable.getCurrentUser() != null) {
            this.utils = new Utils(getApplicationContext());

            this.client = globalVariable.getClient();

            this.userService = new UserServiceImpl(globalVariable, utils);

        /*Set base vars*/
            this.currentUserId = globalVariable.getCurrentUser().getUserId();

            Log.d(TAG, "Before intentBroadcastService");
            this.intentBroadcastService = new Intent(this, BroadcastService.class);
            startService(intentBroadcastService);
            Log.d(TAG, "After intentBroadcastService");

            // set the Behind View
            setBehindContentView(R.layout.profile_menu_frame);

            // customize the SlidingMenu
            SlidingMenu sm = getSlidingMenu();
            sm.setShadowWidthRes(R.dimen.shadow_width);
            sm.setShadowDrawable(R.drawable.shadow);
            sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
            sm.setFadeDegree(0.35f);
            sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);


            serverCheck();
        }
    }

    public void restartAppAfterSleepMode() {
        //this is special method to prevent app from crash after it was in a sleep mode
        //whole app should be re-init
        if (globalVariable.getCurrentUser() == null) {
            Intent intentRedirect = new Intent(this, SplashActivity.class);
            intentRedirect.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentRedirect);
            finish();
        }
    }

    protected abstract int getLayoutResourceId();

    protected void beforeSetContentView(){
        setTheme(R.style.ChatAppRedTheme); // Set here
    }

    protected void serverCheck() {
        Log.d(TAG, "serverCheck");
        (new ServerCheckUtil() {
            @Override
            public void onPostServerAlive(String result) {
                if (result != null && result.trim().equals("OK")) {
                    Log.d(TAG, "onPostServerAlive: OK");
                    //basic functionality is empty, to add you code this method should be override
                    //startService(intentBroadcastService);
                } else {
                    Log.d(TAG, "onPostServerAlive: No internet connection or server is down.");
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No internet connection or server is down.", Toast.LENGTH_SHORT).show();
                            // offline mode

                            globalVariable.setIsAppRunning(false);
                            stopService(intentBroadcastService);
                        }
                    });
                }
            }
        }).isServerAlive(globalVariable);
    }

    /**
     * Method to send message to web socket server
     */
    protected void sendMessageToServer(String message) {
        if (client != null && client.isConnected()) {
            client.send(message);
        }
    }
}

package com.pkletsko.russ.chat.websocket.message.json.model.wall;


import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;

public class Attachment extends BasicTransportJSONObject implements JSONResponse, Comparable {

    private Long attachmentMDBId;

    private Long wallMessageId;

    private String attachmnetName;

    public Long getAttachmentMDBId() {
        return attachmentMDBId;
    }

    public void setAttachmentMDBId(Long attachmentMDBId) {
        this.attachmentMDBId = attachmentMDBId;
    }

    public Long getWallMessageId() {
        return wallMessageId;
    }

    public void setWallMessageId(Long wallMessageId) {
        this.wallMessageId = wallMessageId;
    }

    public String getAttachmnetName() {
        return attachmnetName;
    }

    public void setAttachmnetName(String attachmnetName) {
        this.attachmnetName = attachmnetName;
    }

    @Override
    public int compareTo(Object o) {
        Attachment comment = (Attachment) o;
        Long result = this.attachmentMDBId - comment.getAttachmentMDBId();
        return result.intValue();
    }
}

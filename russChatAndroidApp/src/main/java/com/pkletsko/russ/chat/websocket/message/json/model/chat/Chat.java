package com.pkletsko.russ.chat.websocket.message.json.model.chat;

import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 15:07
 * To change this template use File | Settings | File Templates.
 */
public class Chat {

    public Long conversationMDBId;

    public Long conversationId;

    public String conversationName;

    public Integer newChatMessagesNumber;

    public Set<UserProfile> users;

    public String conversationIcon;

    public String getConversationIcon() {
        return conversationIcon;
    }

    public void setConversationIcon(String conversationIcon) {
        this.conversationIcon = conversationIcon;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    public Integer getNewChatMessagesNumber() {
        return newChatMessagesNumber;
    }

    public void setNewChatMessagesNumber(Integer newChatMessagesNumber) {
        this.newChatMessagesNumber = newChatMessagesNumber;
    }

    public Long getConversationMDBId() {
        return conversationMDBId;
    }

    public void setConversationMDBId(Long conversationMDBId) {
        this.conversationMDBId = conversationMDBId;
    }

    public Set<UserProfile> getUsers() {
        return users;
    }

    public void setUsers(Set<UserProfile> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        String str = getConversationName();
        if (getNewChatMessagesNumber() != null) {
            str+=" " + getNewChatMessagesNumber();
        }

        return str.trim();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Chat) {
            Chat pp = (Chat) o;
            return (pp.conversationName.equals(this.conversationName) && pp.conversationId == this.conversationId);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = conversationId != null ? (int)(conversationId.longValue()^(conversationId.longValue()>>>32))  : 0;
        result = 31 * result + (conversationName != null ? conversationName.hashCode() : 0);
        return result;
    }

}

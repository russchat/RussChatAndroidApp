package com.pkletsko.russ.chat.ui.activity.common.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.pkletsko.russ.chat.R;
import com.pkletsko.russ.chat.common.LoginSourceType;
import com.pkletsko.russ.chat.common.event.handler.EventHub;
import com.pkletsko.russ.chat.ui.activity.common.activity.base.BaseMainActivity;
import com.pkletsko.russ.chat.ui.activity.common.tab.TabEnum;
import com.pkletsko.russ.chat.ui.activity.conversation.create.CreateNewConversationMultiActivity;
import com.pkletsko.russ.chat.ui.activity.conversation.fragment.ConversationFragment;
import com.pkletsko.russ.chat.ui.activity.login.LoginActivity;
import com.pkletsko.russ.chat.ui.activity.profile.activity.UserProfileActivity;
import com.pkletsko.russ.chat.ui.activity.profile.edit.UserProfileEditActivity;
import com.pkletsko.russ.chat.ui.activity.settings.ChangeIPActivity;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.MyWallFragment;
import com.pkletsko.russ.chat.ui.activity.wall.fragment.WallFragment;
import com.pkletsko.russ.chat.ui.activity.wall.post.WallPostEditActivity;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseMainActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ImageButton closeProfileButton;
    private ImageButton editMyProfileButton;

    private ImageView profileIconSmall;
    private ImageView profileIconBig;
    private TextView loggedUserName;
    private TextView myMsgNumber;
    private TextView myMsgNumberLable;
    private TextView myFollowingNumber;
    private TextView myFollowersNumber;
    private TextView settingsLable;
    private TextView logoutLable;

    private TextView myFollowingNumberLable;
    private TextView myFollowersNumberLable;

    private ImageButton openMenuButton;
    private TextView useNameTextView;

    private RelativeLayout topLevelLayout;

    private UserProfile currentUserProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeBlue);
        super.onCreate(savedInstanceState);

        Log.d(TAG, "Initialization of Main Activity");

        currentUserProfile = globalVariable.getCurrentUser();

        //this two checks should be together
        restartAppAfterSleepMode();
        if (currentUserProfile != null) {
            customActionBarInit();
            tabsInit();
            personalUserMenuInit();

            //conversation tab should be opened after back from chat activity
            int tabToOpen = getIntent().getIntExtra("FirstTab", -1);
            if (tabToOpen != -1) {
                TabLayout tabLayout = (TabLayout) findViewById(R.id.mainTabLayout);
                if (tabLayout != null) {
                    TabLayout.Tab tab = tabLayout.getTabAt(2);
                    if (tab != null) {
                        tab.select();
                    }
                }
            }
            topLevelLayout = (RelativeLayout) findViewById(R.id.top_layout);
            if (isFirstTime()) {
                topLevelLayout.setVisibility(View.INVISIBLE);
            }
        }
    }

//    public void restartAppAfterSleepMode() {
//        //this is special method to prevent app from crash after it was in a sleep mode
//        //whole app should be re-init
//        if (globalVariable.getCurrentUser() == null) {
//            Intent intentRedirect = new Intent(MainActivity.this, SplashActivity.class);
//            intentRedirect.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intentRedirect);
//            finish();
//        }
//    }


    private boolean isFirstTime() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        //boolean ranBefore = false;
        if (!ranBefore) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
            topLevelLayout.setVisibility(View.VISIBLE);
            topLevelLayout.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    topLevelLayout.setVisibility(View.INVISIBLE);
                    return false;
                }
            });
        }
        return ranBefore;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
            finish(); // finish activity
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(intentBroadcastService);
        registerReceiver(broadcastReceiverGetUserProfile, new IntentFilter(EventHub.BROADCAST_GET_USER_PROFILE));
    }

    @Override
    public void onPause() {
        unregisterReceiver(broadcastReceiverGetUserProfile);
        super.onPause();
    }

    private void customActionBarInit() {
        // Always cast your custom Toolbar here, and set it as the ActionBar.
        final Toolbar mCustomView = (Toolbar) findViewById(R.id.main_custom_action_toolbar);
        setSupportActionBar(mCustomView);

        useNameTextView = (TextView) mCustomView.findViewById(R.id.userNameCustomActionBar);
        useNameTextView.setText(currentUserProfile.getFullName());
        useNameTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        final ImageButton addActionButton = (ImageButton) mCustomView.findViewById(R.id.addActionCustomActionBar);
        addActionButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                addActionButton();
            }
        });

        openMenuButton = (ImageButton) mCustomView.findViewById(R.id.openMenuActionCustomActionBar);
        openMenuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) mCustomView.findViewById(R.id.searchViewCustomActionBar);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                openMenuButton.setVisibility(View.VISIBLE);
                useNameTextView.setVisibility(View.VISIBLE);
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenuButton.setVisibility(View.GONE);
                useNameTextView.setVisibility(View.GONE);
            }
        });

        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        View searchPlate = searchView.findViewById(searchPlateId);
        if (searchPlate != null) {
            int searchTextId = searchPlate.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
            TextView searchText = (TextView) searchPlate.findViewById(searchTextId);
            if (searchText != null) {
                searchText.setTextColor(Color.WHITE);
                searchText.setHintTextColor(Color.WHITE);
            }
        }

        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ImageView searchBtn = (ImageView) searchField.get(searchView);
            searchBtn.setImageResource(R.drawable.ic_action_search);
        } catch (NoSuchFieldException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (IllegalAccessException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private void tabsInit() {
        final CustomViewPager viewPager = (CustomViewPager) findViewById(R.id.pager);

        final PagerAdapter pagerAdapter = new PagerAdapter(getFragmentManager(), getApplicationContext());
        pagerAdapter.addFragment(new WallFragment(), R.drawable.ic_action_group);
        pagerAdapter.addFragment(new MyWallFragment(), R.drawable.ic_action_person);
        pagerAdapter.addFragment(new ConversationFragment(), R.drawable.ic_action_chat);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                switch (position) {
                    case 0:
                        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
                        break;
                    default:
                        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
                        break;
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }

        });

        viewPager.setAdapter(pagerAdapter);

        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.mainTabLayout);
        tabLayout.setupWithViewPager(viewPager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }
    }

    private void personalUserMenuInit() {
        editMyProfileButton = (ImageButton) findViewById(R.id.editMyProfileButton);
        closeProfileButton = (ImageButton) findViewById(R.id.closeProfileButton);
        profileIconSmall = (ImageView) findViewById(R.id.test_my_icon_small);
        profileIconBig = (ImageView) findViewById(R.id.test_my_icon);

        loggedUserName = (TextView) findViewById(R.id.loggedUserName);
        loggedUserName.setText(currentUserProfile.getFullName());
        loggedUserName.setMovementMethod(LinkMovementMethod.getInstance());
        loggedUserName.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    globalVariable.setOpenProfile(currentUserProfile.getUserId());
                    globalVariable.setOpenProfileCurrentTab(0);
                    Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                    startActivity(intentRedirect);
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myMsgNumber = (TextView) findViewById(R.id.myMsgNumber);
        myMsgNumber.setMovementMethod(LinkMovementMethod.getInstance());
        myMsgNumber.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    if (currentUserProfile.getMyPosts() > 0) {
                        globalVariable.setOpenProfile(currentUserProfile.getUserId());
                        globalVariable.setOpenProfileCurrentTab(0);
                        Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                        startActivity(intentRedirect);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myMsgNumberLable = (TextView) findViewById(R.id.myMsgNumberLable);
        myMsgNumberLable.setMovementMethod(LinkMovementMethod.getInstance());
        myMsgNumberLable.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    if (currentUserProfile.getMyPosts() > 0) {
                        globalVariable.setOpenProfile(currentUserProfile.getUserId());
                        globalVariable.setOpenProfileCurrentTab(0);
                        Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                        startActivity(intentRedirect);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myFollowingNumber = (TextView) findViewById(R.id.myFollowingNumber);
        myFollowingNumber.setMovementMethod(LinkMovementMethod.getInstance());
        myFollowingNumber.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    if (currentUserProfile.getFollowingWalls() > 0) {
                        globalVariable.setOpenProfile(currentUserProfile.getUserId());
                        globalVariable.setOpenProfileCurrentTab(1);
                        Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                        startActivity(intentRedirect);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myFollowingNumberLable = (TextView) findViewById(R.id.myFollowingNumberLable);
        myFollowingNumberLable.setMovementMethod(LinkMovementMethod.getInstance());
        myFollowingNumberLable.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    if (currentUserProfile.getFollowingWalls() > 0) {
                        globalVariable.setOpenProfile(currentUserProfile.getUserId());
                        globalVariable.setOpenProfileCurrentTab(1);
                        Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                        startActivity(intentRedirect);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myFollowersNumber = (TextView) findViewById(R.id.myFollowersNumber);
        myFollowersNumber.setMovementMethod(LinkMovementMethod.getInstance());
        myFollowersNumber.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    if (currentUserProfile.getFollowers() > 0) {
                        globalVariable.setOpenProfile(currentUserProfile.getUserId());
                        globalVariable.setOpenProfileCurrentTab(2);
                        Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                        startActivity(intentRedirect);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myFollowersNumberLable = (TextView) findViewById(R.id.myFollowersNumberLable);
        myFollowersNumberLable.setMovementMethod(LinkMovementMethod.getInstance());
        myFollowersNumberLable.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.isAppRunning()) {
                    if (currentUserProfile.getFollowers() > 0) {
                        globalVariable.setOpenProfile(currentUserProfile.getUserId());
                        globalVariable.setOpenProfileCurrentTab(2);
                        Intent intentRedirect = new Intent(MainActivity.this, UserProfileActivity.class);
                        startActivity(intentRedirect);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        settingsLable = (TextView) findViewById(R.id.settingsLable);
        settingsLable.setMovementMethod(LinkMovementMethod.getInstance());
        settingsLable.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                Intent iUnFollow = new Intent(MainActivity.this, ChangeIPActivity.class);
                startActivity(iUnFollow);
            }
        });

        //TODO this is temporary because we don't need settings for now
        settingsLable.setVisibility(View.GONE);

        logoutLable = (TextView) findViewById(R.id.logoutLable);
        logoutLable.setMovementMethod(LinkMovementMethod.getInstance());
        logoutLable.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (globalVariable.getLoginSourceType().equals(LoginSourceType.FACEBOOK)) {
                    // check for updates action
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    LoginManager.getInstance().logOut();
                    Log.i(TAG, "FACEBOOK Logout !!!");
                }

                if (globalVariable.getLoginSourceType().equals(LoginSourceType.GOOGLE)) {
                    globalVariable.setGoogleLogOut(true);
                    Log.i(TAG, "GOOGLE Logout !!!");
                }

                globalVariable.getUserMDAO().removeCurrentUser(currentUserProfile);
                globalVariable.setIsAppRunning(false);
                stopService(intentBroadcastService);

                Intent iUnFollow = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(iUnFollow);
            }
        });

        closeProfileButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                toggle();
            }

        });

        editMyProfileButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (globalVariable.isAppRunning()) {
                    Intent intentRedirect = new Intent(MainActivity.this, UserProfileEditActivity.class);
                    startActivity(intentRedirect);
                } else {
                    Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myMsgNumber.setText(String.valueOf(currentUserProfile.getMyPosts()));
        myFollowingNumber.setText(String.valueOf(currentUserProfile.getFollowingWalls()));
        myFollowersNumber.setText(String.valueOf(currentUserProfile.getFollowers()));

        profileIconBig.setAlpha(20);
        Picasso.with(getApplicationContext())
                .load(currentUserProfile.getProfileIcon())
                .into(profileIconBig);

        //TODO 150 !! round ???
        globalVariable.getPicassoHelper().loadImageWithFit(currentUserProfile.getProfileIcon(), profileIconSmall, globalVariable.getPicassoHelper().getRoundedTransformation());
    }

    /**
     * add action button reaction
     */
    private void addActionButton() {
        if (globalVariable.isAppRunning()) {
            if (globalVariable.getActiveTab() != null && globalVariable.getActiveTab().equals(TabEnum.TAB_CONVERSATION)) {
                Intent i = new Intent(MainActivity.this, CreateNewConversationMultiActivity.class);
                startActivity(i);
            }

            if (globalVariable.getActiveTab() != null && globalVariable.getActiveTab().equals(TabEnum.TAB_MY_WALL)) {
                Intent i = new Intent(MainActivity.this, WallPostEditActivity.class);
                startActivity(i);
            }

            if (globalVariable.getActiveTab() != null && globalVariable.getActiveTab().equals(TabEnum.TAB_WALL)) {
                Intent i = new Intent(MainActivity.this, WallPostEditActivity.class);
                startActivity(i);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Is not available in offline mode.", Toast.LENGTH_SHORT).show();
        }
    }

    private BroadcastReceiver broadcastReceiverGetUserProfile = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    currentUserProfile = globalVariable.getCurrentUser();
                    myMsgNumber.setText(String.valueOf(currentUserProfile.getMyPosts()));
                    myFollowingNumber.setText(String.valueOf(currentUserProfile.getFollowingWalls()));
                    myFollowersNumber.setText(String.valueOf(currentUserProfile.getFollowers()));

                    profileIconBig.setAlpha(20);
                    Picasso.with(getApplicationContext())
                            .load(currentUserProfile.getProfileIcon())
                            .into(profileIconBig);

                    //TODO 150 !! round ???
                    globalVariable.getPicassoHelper().loadImageWithFit(currentUserProfile.getProfileIcon(), profileIconSmall, globalVariable.getPicassoHelper().getRoundedTransformation());
                }
            });
        }
    };

    static class PagerAdapter extends FragmentPagerAdapter {
        private final Context appContext;
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<Integer> tabIcons = new ArrayList<>();

        public PagerAdapter(final FragmentManager fragmentManager, final Context appContext) {
            super(fragmentManager);
            this.appContext = appContext;
        }

        public void addFragment(final Fragment fragment, final Integer tabIcon) {
            fragmentList.add(fragment);
            tabIcons.add(tabIcon);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            final View v = LayoutInflater.from(appContext).inflate(R.layout.main_activity_tab_header, null);

            final ImageView tabIcon = (ImageView) v.findViewById(R.id.tab_image_header);
            tabIcon.setImageResource(tabIcons.get(position));
            return v;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}

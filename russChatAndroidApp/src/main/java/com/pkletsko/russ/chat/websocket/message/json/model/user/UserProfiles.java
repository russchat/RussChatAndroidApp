package com.pkletsko.russ.chat.websocket.message.json.model.user;

import java.util.Set;

import com.pkletsko.russ.chat.websocket.message.json.model.common.BasicTransportJSONObject;
import com.pkletsko.russ.chat.websocket.message.json.model.common.JSONResponse;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

/**
 * Created with IntelliJ IDEA.
 * User: pkletsko
 * Date: 06.02.15
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */
public class UserProfiles extends BasicTransportJSONObject implements JSONResponse {

    private Set<UserProfile> userProfiles;

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }
}

package com.pkletsko.russ.chat.storage.service;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.pkletsko.russ.chat.common.GlobalClass;
import com.pkletsko.russ.chat.other.Utils;
import com.pkletsko.russ.chat.websocket.message.json.model.user.UserProfile;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pkletsko on 25.05.2015.
 */
public class UserServiceImpl implements UserService {

    private GlobalClass globalVariable;
    private Utils utils;

    private static Set<UserProfile> usersStoreQueue = new HashSet<>();

    private static Set<Long> usersCheckQueue = new HashSet<>();

    public UserServiceImpl(GlobalClass globalVariable, Utils utils) {
        this.globalVariable = globalVariable;
        this.utils = utils;
    }

    private static final String TAG = UserServiceImpl.class.getSimpleName();

    public void storeUsersToMDB(final Set<UserProfile> userProfiles) {
        Log.d(TAG, "storeUsersToMDB: size = " + userProfiles.size());

        for (final UserProfile userProfile : userProfiles) {
            storeUserToMDB(userProfile);
        }
    }

    public void storeUserToMDB(final UserProfile userProfile) {
        Log.d(TAG, "1) storeUserToMDB: userProfile = " + userProfile);

        if (!usersStoreQueue.contains(userProfile)) {
            globalVariable.addUserProfileToCash(userProfile);

            Log.d(TAG, "2) storeUserToMDB: before usersStoreQueue.size:  = " + usersStoreQueue.size());
            usersStoreQueue.add(userProfile);

            saveOrUpdateUserMDB(userProfile);
        }
    }

    public void userChecking(final Long userId) {

        Log.d(TAG, "1) userChecking: userId = " + userId);

        if (!usersCheckQueue.contains(userId)) {

            Log.d(TAG, "2) userChecking: before usersCheckQueue.size:  = " + usersCheckQueue.size());
            usersCheckQueue.add(userId);

            boolean userExist = isUserExist(userId);
            Log.d(TAG, "3) userChecking: userExist  = " + userExist);
        }
    }

    public void saveOrUpdateUserMDB(final UserProfile userProfile) {
        Log.d(TAG, "1) saveOrUpdateUserMDB: userProfile = " + userProfile);
        AsyncTask<Void, Void, UserProfile> task = new AsyncTask<Void, Void, UserProfile>() {
            @Override
            protected UserProfile doInBackground(Void... params) {
                final Long currentUserId = globalVariable.getCurrentUser().getUserId();
                return globalVariable.getUserMDAO().saveOrUpdateUser(userProfile, currentUserId);
            }

            @Override
            protected void onPostExecute(UserProfile result) {
                Log.d(TAG, "4) saveOrUpdateUserMDB->onPostExecute result: " + result.getFullName()
                        + " , id = [" + result.getUserId() + "][" + result.getUserMDBId()
                        + "], version = " + result.getVersion());
                globalVariable.addUserProfileToCash(result);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    private boolean isUserExist(final Long userId) {
        final UserProfile existedUser = globalVariable.getUsersCash().get(userId);
        if (existedUser == null) {
            globalVariable.sendMessageToServer(utils.getSendUserProfileRequestJSON(userId));
            return false;
        } else if (existedUser.getFullName().equals("UnKnown")) {
            globalVariable.sendMessageToServer(utils.getSendUserProfileRequestJSON(userId));
            return false;
        } else {
            return true;
        }
    }
}

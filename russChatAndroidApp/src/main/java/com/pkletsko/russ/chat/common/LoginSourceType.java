package com.pkletsko.russ.chat.common;

public enum LoginSourceType {
    FACEBOOK(1),
    GOOGLE(2);

    private final int code;

    LoginSourceType(final int value) {
        this.code = value;
    }

    public int getCode() {
        return code;
    }

    public static LoginSourceType findByCode(int code){
        for(LoginSourceType v : values()){
            if( v.getCode() == code){
                return v;
            }
        }
        return null;
    }
}

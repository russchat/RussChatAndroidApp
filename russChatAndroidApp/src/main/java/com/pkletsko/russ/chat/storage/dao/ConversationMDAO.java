package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.Chat;
import com.pkletsko.russ.chat.websocket.message.json.model.chat.ConversationComp;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class ConversationMDAO {
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = { RussSQLiteHelper.COLUMN_CONVERSATION_MDB_ID,
            RussSQLiteHelper.COLUMN_CONVERSATION_ID, RussSQLiteHelper.COLUMN_CONVERSATION_NAME, RussSQLiteHelper.COLUMN_CONVERSATION_ICON, RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public ConversationMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Chat createConversation(final Chat chat, final Long appOwnerId) {
        ContentValues values = new ContentValues();
        values.put(RussSQLiteHelper.COLUMN_CONVERSATION_ID, chat.getConversationId());
        values.put(RussSQLiteHelper.COLUMN_CONVERSATION_NAME, chat.getConversationName());
        values.put(RussSQLiteHelper.COLUMN_CONVERSATION_ICON, chat.getConversationIcon());
        values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
        long insertId = database.insert(RussSQLiteHelper.TABLE_CONVERSATION, null, values);
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_CONVERSATION, allColumns, RussSQLiteHelper.COLUMN_CONVERSATION_MDB_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Chat newChat = cursorToConversationUI(cursor);
        cursor.close();
        return newChat;
    }

    public void deleteAll() {
        database.execSQL("delete from " + RussSQLiteHelper.TABLE_CONVERSATION);
    }

    public void deleteConversation(Chat conversationMDTO) {
        long id = conversationMDTO.getConversationMDBId();
        database.delete(RussSQLiteHelper.TABLE_CONVERSATION, RussSQLiteHelper.COLUMN_CONVERSATION_MDB_ID + " = " + id, null);
    }

    public Set<Chat> getAllConversations() {
        Set<Chat> conversations = new HashSet<Chat>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_CONVERSATION,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Chat conversation = cursorToConversationUI(cursor);
            conversations.add(conversation);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return conversations;
    }

    public Set<Chat> getAllConversationsByOwnerId(final Long appOwnerId) {
        Set<Chat> conversations = new TreeSet<>(new ConversationComp());

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_CONVERSATION,
                allColumns, RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Chat conversation = cursorToConversationUI(cursor);
            conversations.add(conversation);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return conversations;
    }

    public Chat getConversationById(final long serverConversationId, final Long appOwnerId) {
        Chat conversationMDTO = null;
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_CONVERSATION, allColumns, RussSQLiteHelper.COLUMN_CONVERSATION_ID + " = " + serverConversationId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            conversationMDTO = cursorToConversationUI(cursor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return conversationMDTO;
    }

    private Chat cursorToConversationUI(Cursor cursor) {
        Chat conversation = new Chat();
        conversation.setConversationMDBId(cursor.getLong(0));
        conversation.setConversationId(cursor.getLong(1));
        conversation.setConversationName(cursor.getString(2));
        conversation.setConversationIcon(cursor.getString(3));
        return conversation;
    }
}

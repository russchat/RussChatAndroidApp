package com.pkletsko.russ.chat.storage.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.pkletsko.russ.chat.storage.RussSQLiteHelper;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.Comment;
import com.pkletsko.russ.chat.websocket.message.json.model.wall.CommentComp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pkletsko on 10.02.2015.
 */
public class CommentMDAO {
    // Database fields
    private SQLiteDatabase database;
    private RussSQLiteHelper dbHelper;
    private String[] allColumns = {RussSQLiteHelper.COLUMN_COMMENT_MDB_ID,
            RussSQLiteHelper.COLUMN_COMMENT_TEXT,
            RussSQLiteHelper.COLUMN_COMMENT_ID,
            RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID,
            RussSQLiteHelper.COLUMN_COMMENT_FROM_USER_ID,
            RussSQLiteHelper.COLUMN_APP_OWNER_ID};

    public CommentMDAO(Context context) {
        dbHelper = new RussSQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Comment createComment(final Comment comment, final Long appOwnerId) {
        Comment existedComment = getComment(comment.getCommentId(), appOwnerId);
        if (existedComment == null) {
            ContentValues values = new ContentValues();
            values.put(RussSQLiteHelper.COLUMN_COMMENT_TEXT, comment.getCommentText());
            values.put(RussSQLiteHelper.COLUMN_COMMENT_ID, comment.getCommentId());
            values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, comment.getWallMessageId());
            values.put(RussSQLiteHelper.COLUMN_COMMENT_FROM_USER_ID, comment.getFromUserId());
            values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
            long insertId = database.insert(RussSQLiteHelper.TABLE_COMMENT, null, values);
            Cursor cursor = database.query(RussSQLiteHelper.TABLE_COMMENT, allColumns, RussSQLiteHelper.COLUMN_COMMENT_MDB_ID + " = " + insertId, null, null, null, null);
            cursor.moveToFirst();
            existedComment = cursorToComment(cursor);
            cursor.close();
        }
        return existedComment;
    }

    public void createComments(final Set<Comment> comments, final Long appOwnerId) {
        for (Comment comment : comments) {
            Comment existedComment = getComment(comment.getCommentId(), appOwnerId);
            if (existedComment == null) {
                ContentValues values = new ContentValues();
                values.put(RussSQLiteHelper.COLUMN_COMMENT_TEXT, comment.getCommentText());
                values.put(RussSQLiteHelper.COLUMN_COMMENT_ID, comment.getCommentId());
                values.put(RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID, comment.getWallMessageId());
                values.put(RussSQLiteHelper.COLUMN_COMMENT_FROM_USER_ID, comment.getFromUserId());
                values.put(RussSQLiteHelper.COLUMN_APP_OWNER_ID, appOwnerId);
                database.insert(RussSQLiteHelper.TABLE_COMMENT, null, values);
            }
        }
    }

    public void deleteComment(Comment comment) {
        long id = comment.getCommentMDBId();
        database.delete(RussSQLiteHelper.TABLE_COMMENT, RussSQLiteHelper.COLUMN_COMMENT_MDB_ID + " = " + id, null);
    }

    public List<Comment> getAllComments() {
        List<Comment> commentList = new ArrayList<Comment>();

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_COMMENT, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Comment comment = cursorToComment(cursor);
            commentList.add(comment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return commentList;
    }

    public Set<Comment> getCommentsByWallMessageId(final Long wallMessageId, final Long appOwnerId) {
        Set<Comment> commentList = new TreeSet<Comment>(new CommentComp());

        Cursor cursor = database.query(RussSQLiteHelper.TABLE_COMMENT, allColumns, RussSQLiteHelper.COLUMN_WALL_MESSAGE_ID + " = " + wallMessageId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Comment comment = cursorToComment(cursor);
            commentList.add(comment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return commentList;
    }

    public Comment getComment(final Long commentId, final Long appOwnerId) {
        Cursor cursor = database.query(RussSQLiteHelper.TABLE_COMMENT, allColumns, RussSQLiteHelper.COLUMN_COMMENT_ID + " = " + commentId + " AND " + RussSQLiteHelper.COLUMN_APP_OWNER_ID + " = " + appOwnerId, null, null, null, null);
        Comment comment = null;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            comment = cursorToComment(cursor);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return comment;
    }

    private Comment cursorToComment(Cursor cursor) {
        Comment comment = new Comment();
        comment.setCommentMDBId(cursor.getLong(0));
        comment.setCommentText(cursor.getString(1));
        comment.setCommentId(cursor.getLong(2));
        comment.setWallMessageId(cursor.getLong(3));
        comment.setFromUserId(cursor.getLong(4));
        return comment;
    }
}
